<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
                        
class jadwal_model extends CI_Model 
{
	public function getJadwal()
	{
		$this->db->select('jadwals.*, karyawans.*, jadwals.id AS jadwal_id, jadwals.status AS jadwal_status, jadwals.created_at AS jadwal_ca');
		$this->db->join('karyawans', 'jadwals.karyawan_id = karyawans.id');
		$this->db->order_by('jadwals.created_at', 'desc');
		$result = $this->db->get('jadwals');
		return $result;
	}

	public function getJadwalUser($id)
	{
		$this->db->select('jadwals.*, karyawans.*, jadwals.id AS jadwal_id, jadwals.status AS jadwal_status, jadwals.created_at AS jadwal_ca');
		$this->db->join('karyawans', 'jadwals.karyawan_id = karyawans.id');
		$this->db->order_by('jadwals.created_at', 'desc');
		$this->db->where('jadwals.karyawan_id', $id);
		$result = $this->db->get('jadwals');
		return $result;
	}

	public function getJadwalUserBelum($id)
	{
		$this->db->select('jadwals.*, karyawans.*, jadwals.id AS jadwal_id, jadwals.status AS jadwal_status, jadwals.created_at AS jadwal_ca');
		$this->db->join('karyawans', 'jadwals.karyawan_id = karyawans.id');
		$this->db->order_by('jadwals.created_at', 'desc');
		$this->db->where('karyawan_id', $id);
		$this->db->where('jadwals.status', 'belum');
		$result = $this->db->get('jadwals');
		return $result;
	}

	public function insertJadwal()
	{
		// $surat_sakit = $_FILES['surat_sakit']['name'];
		// date_default_timezone_set('Asia/Makassar');
		// $date_now = date('dmYHis');
		// if ($surat_sakit != "") {
		// 	$file_name = str_replace('.', '', 'suratsakit_' . $this->input->post('karyawan_id') . '_' . $date_now);
		// 	$config['upload_path']          = FCPATH . 'assets/suratsakit';
		// 	$config['allowed_types']        = 'pdf|jpg|jpeg|png';
		// 	$config['file_name']            = $file_name;
		// 	$config['overwrite']            = true;
		// 	$config['max_size']             = 5120; // 5MB

		// 	$this->load->library('upload', $config);

		// 	if (!$this->upload->do_upload('surat_sakit')) {
		// 		$data['error'] = $this->upload->display_errors();
		// 		$this->session->set_flashdata('error', $this->upload->display_errors());
		// 		redirect('jadwal/tambah');
		// 	} else {
		// 		$uploaded_data = $this->upload->data();

		// 		$insert = array(
		// 			'karyawan_id' => $this->input->post('karyawan_id'),
		// 			'tanggal' => $this->input->post('tanggal'),
		// 			'status' => $this->input->post('status'),
		// 			'surat_sakit' => $uploaded_data['file_name'],
		// 			'jadwal_masuk' => $this->input->post('jadwal_masuk'),
		// 			'jadwal_keluar' => $this->input->post('jadwal_keluar'),

		// 		);
		// 		$this->db->set('created_at', 'NOW()', FALSE);
		// 		$this->db->set('updated_at', 'NOW()', FALSE);
		// 		$result = $this->db->insert('jadwals', $insert);
		// 	}
		// } else {
			$karyawan_id = $this->input->post('karyawan_id');
			foreach($karyawan_id as $karyawan_id){
				$insert = array(
					'karyawan_id' => $karyawan_id,
					'tanggal' => $this->input->post('tanggal'),
					'status' => $this->input->post('status'),
					'jadwal_masuk' => $this->input->post('jadwal_masuk'),
					'jadwal_keluar' => $this->input->post('jadwal_keluar'),

				);
				$this->db->set('created_at', 'NOW()', FALSE);
				$this->db->set('updated_at', 'NOW()', FALSE);
				$result = $this->db->insert('jadwals', $insert);
			}
			
			
		// }

		return $result;
	}

	public function deleteJadwal($id)
	{
		$query = $this->db->get_where('jadwals', array('id' => $id))->row();

		if($query->surat_sakit != NULL){
			unlink(FCPATH . "/assets/suratsakit/" . $query->surat_sakit);
		}
		$this->db->where('id', $id);
		$result = $this->db->delete('jadwals');
		return $result;
	}

	public function getDetailJadwal($id)
	{
		$this->db->select('jadwals.*, karyawans.*, jadwals.id AS jadwal_id, jadwals.status AS jadwal_status, jadwals.created_at AS jadwal_ca');
		$this->db->join('karyawans', 'jadwals.karyawan_id = karyawans.id');
		$this->db->where('jadwals.id', $id);
		$result = $this->db->get('jadwals')->result_array();
		return $result[0];
	}

	public function editJadwal($id)
	{
		// $surat_sakit = $_FILES['surat_sakit']['name'];

		// if ($surat_sakit == "") {
			$edit = array(
				'karyawan_id' => $this->input->post('karyawan_id'),
				'tanggal' => $this->input->post('tanggal'),
				'status' => $this->input->post('status'),
				'jadwal_masuk' => $this->input->post('jadwal_masuk'),
				'jadwal_keluar' => $this->input->post('jadwal_keluar'),
			);
			$this->db->set('updated_at', 'NOW()', FALSE);
			$this->db->where('id', $id);
			$result = $this->db->update('jadwals', $edit);
		// } elseif ($surat_sakit != "") {
		// 	$query = $this->db->get_where('jadwals', array('id' => $id))->row();

		// 	if ($query->surat_sakit != NULL) {
		// 		unlink(FCPATH . "/assets/suratsakit/" . $query->surat_sakit);
		// 	}
			
		// 	date_default_timezone_set('Asia/Makassar');
		// 	$date_now = date('dmYHis');
		// 	$file_name = str_replace('.', '', 'suratsakit_' . $this->input->post('karyawan_id') . '_' . $date_now);
		// 	$config['upload_path']          = FCPATH . '/assets/suratsakit';
		// 	$config['allowed_types']        = 'pdf|jpg|jpeg|png';
		// 	$config['file_name']            = $file_name;
		// 	$config['overwrite']            = true;
		// 	$config['max_size']             = 5120; // 5MB

		// 	$this->load->library('upload', $config);

		// 	if (!$this->upload->do_upload('file')) {
		// 		$data['error'] = $this->upload->display_errors();
		// 		$this->session->set_flashdata('error', $this->upload->display_errors());
		// 		redirect('jadwal/edit/' . $id);
		// 	} else {
		// 		$uploaded_data = $this->upload->data();

		// 		$edit = array(
		// 			'karyawan_id' => $this->input->post('karyawan_id'),
		// 			'tanggal' => $this->input->post('tanggal'),
		// 			'status' => $this->input->post('status'),
		// 			'surat_sakit' => $uploaded_data['file_name'],
		// 			'jadwal_masuk' => $this->input->post('jadwal_masuk'),
		// 			'jadwal_keluar' => $this->input->post('jadwal_keluar'),
		// 		);
		// 		$this->db->set('updated_at', 'NOW()', FALSE);
		// 		$this->db->where('id', $id);
		// 		$result = $this->db->update('jadwals', $edit);
		// 	}
		// }
		return $result;
	}

	public function getJadwalKaryawanNow($id)
	{
		$this->db->select('jadwals.*, karyawans.*, jadwals.id AS jadwal_id, jadwals.status AS jadwal_status, jadwals.created_at AS jadwal_ca');
		$this->db->join('karyawans', 'jadwals.karyawan_id = karyawans.id');
		$this->db->where('karyawan_id', $id);
		$this->db->where('tanggal', date('Y-m-d'));
		$result = $this->db->get('jadwals');
		if($result->num_rows() > 0){
			return $result->result_array()[0];
		}
		else{
			return '-';
		}
		
	}

	public function uploadSuratSakit($id,$karyawan_id){
			$query = $this->db->get_where('jadwals', array('id' => $id))->row();
		
			if ($query->surat_sakit != NULL) {
				unlink(FCPATH . "/assets/suratsakit/" . $query->surat_sakit);
			}
			
			date_default_timezone_set('Asia/Makassar');
			$date_now = date('dmYHis');
			$file_name = str_replace('.', '', 'suratsakit_' . $karyawan_id . '_' . $date_now);
			$config['upload_path']          = FCPATH . '/assets/suratsakit';
			$config['allowed_types']        = 'pdf|jpg|jpeg|png';
			$config['file_name']            = $file_name;
			$config['overwrite']            = true;
			$config['max_size']             = 5120; // 5MB

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('surat_sakit')) {
				$data['error'] = $this->upload->display_errors();
				$this->session->set_flashdata('error', $this->upload->display_errors());
				redirect('dashboard/sakit/' . $id);
			} else {
				$uploaded_data = $this->upload->data();
			
				$edit = array(
					'status' => 'sakit',
					'surat_sakit' => $uploaded_data['file_name'],
				);
				$this->db->set('updated_at', 'NOW()', FALSE);
				$this->db->where('id', $id);
				$result = $this->db->update('jadwals', $edit);
			}
		return $result;
	}

	public function getJadwalNow()
	{
		date_default_timezone_set('Asia/Makassar');
		$this->db->select('jadwals.*, karyawans.*, jadwals.id AS jadwal_id, jadwals.status AS jadwal_status, jadwals.created_at AS jadwal_ca');
		$this->db->join('karyawans', 'jadwals.karyawan_id = karyawans.id');
		$this->db->where('jadwals.tanggal', date('Y-m-d'));
		$this->db->where('jadwals.status <>', 'hadir');
		$this->db->order_by('jadwals.created_at', 'desc');
		$result = $this->db->get('jadwals');
		return $result;
	}
                        
}


/* End of file Jadwal_model.php and path \application\models\Jadwal_model.php */
