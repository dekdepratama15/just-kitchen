<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
                        
class pengumuman_model extends CI_Model 
{
	public function getPengumuman()
	{
		$this->db->select('pengumumans.*, users.*, pengumumans.id AS pengumuman_id, pengumumans.created_at AS pengumuman_ca');
		$this->db->join('users', 'pengumumans.user_id = users.id');
		$this->db->order_by('pengumumans.created_at', 'desc');
		$result = $this->db->get('pengumumans');
		return $result;
	}

	public function insertPengumuman()
	{
		$file = $_FILES['file']['name'];
		date_default_timezone_set('Asia/Makassar');
		$date_now = date('dmYHis');
		if ($file != "") {
			$file_name = str_replace('.', '', 'pengumuman_' . $this->session->userdata('user_id').'_'.$date_now);
			$config['upload_path']          = FCPATH . 'assets/pengumuman';
			$config['allowed_types']        = 'pdf';
			$config['file_name']            = $file_name;
			$config['overwrite']            = true;
			$config['max_size']             = 5120; // 5MB

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('file')) {
				$data['error'] = $this->upload->display_errors();
				$this->session->set_flashdata('error', $this->upload->display_errors());
				redirect('pengumuman/tambah');
			} else {
				$uploaded_data = $this->upload->data();

				$insert = array(
					'user_id' => $this->session->userdata('user_id'),
					'judul' => $this->input->post('judul'),
					'file' => $uploaded_data['file_name'],
					'keterangan' => $this->input->post('keterangan'),

				);
				$this->db->set('created_at', 'NOW()', FALSE);
				$this->db->set('updated_at', 'NOW()', FALSE);
				$result = $this->db->insert('pengumumans', $insert);
			}
		} else {
			$this->session->set_flashdata('error', 'File PDF tidak boleh kosong');
			redirect('pengumuman/tambah');
		}

		return $result;
	}

	public function deletePengumuman($id)
	{
		$query = $this->db->get_where('pengumumans', array('id' => $id))->row();
		unlink(FCPATH . "/assets/pengumuman/" . $query->file);
		$this->db->where('id', $id);
		$result = $this->db->delete('pengumumans');
		return $result;
	}

	public function getDetailPengumuman($id)
	{
		$this->db->select('pengumumans.*, users.*, pengumumans.id AS pengumuman_id, pengumumans.created_at AS pengumuman_ca');
		$this->db->join('users', 'pengumumans.user_id = users.id');
		$this->db->where('pengumumans.id', $id);
		$result = $this->db->get('pengumumans')->result_array();
		return $result[0];
	}

	public function editPengumuman($id)
	{
		$file = $_FILES['file']['name'];

		if ($file == "") {
			$edit = array(
				'user_id' => $this->session->userdata('user_id'),
				'judul' => $this->input->post('judul'),
				'keterangan' => $this->input->post('keterangan'),
			);
			$this->db->set('updated_at', 'NOW()', FALSE);
			$this->db->where('id', $id);
			$result = $this->db->update('pengumumans', $edit);
		} elseif ($file != "") {
			$query = $this->db->get_where('pengumumans', array('id' => $id))->row();
			unlink(FCPATH . "/assets/pengumuman/" . $query->file);
			date_default_timezone_set('Asia/Makassar');
			$date_now = date('dmYHis');
			$file_name = str_replace('.', '', 'pengumuman_' . $this->session->userdata('user_id') . '_' . $date_now);
			$config['upload_path']          = FCPATH . '/assets/pengumuman';
			$config['allowed_types']        = 'pdf';
			$config['file_name']            = $file_name;
			$config['overwrite']            = true;
			$config['max_size']             = 5120; // 5MB

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('file')) {
				$data['error'] = $this->upload->display_errors();
				$this->session->set_flashdata('error', $this->upload->display_errors());
				redirect('pengumuman/edit/'.$id);
			} else {
				$uploaded_data = $this->upload->data();

				$edit = array(
					'user_id' => $this->session->userdata('user_id'),
					'judul' => $this->input->post('judul'),
					'file' => $uploaded_data['file_name'],
					'keterangan' => $this->input->post('keterangan'),
				);
				$this->db->set('updated_at', 'NOW()', FALSE);
				$this->db->where('id', $id);
				$result = $this->db->update('pengumumans', $edit);
			}
		}
		return $result;
	}

	public function getCountPengumuman()
	{
		$this->db->select('pengumumans.*, users.*, pengumumans.id AS pengumuman_id, pengumumans.created_at AS pengumuman_ca');
		$this->db->join('users', 'pengumumans.user_id = users.id');
		$this->db->order_by('pengumumans.created_at', 'desc');
		$result = $this->db->get('pengumumans')->num_rows();
		return $result;
	}
}


/* End of file Pengumuman_model.php and path \application\models\Pengumuman_model.php */
