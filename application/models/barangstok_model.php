<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
                        
class barangstok_model extends CI_Model 
{
	public function getBarangStok()
	{
		$this->db->order_by('created_at', 'desc');
		$result = $this->db->get('barang_stocks');
		return $result;
	}

	public function getBarangStokLog()
	{
		$this->db->select('barang_stocks.*, barang_stock_logs.*, barang_stocks.id AS barang_stock_id, barang_stock_logs.id AS barang_stock_logs_id, barang_stock_logs.created_at AS ca_bstocklog, barang_stock_logs.updated_at AS ua_bstocklog, barang_stock_logs.keterangan AS bmlogketerangan');
		$this->db->join('barang_stocks', 'barang_stock_logs.barang_stock_id = barang_stocks.id');
		$this->db->order_by('barang_stock_logs.created_at', 'desc');
		$result = $this->db->get('barang_stock_logs');
		return $result;
	}

	public function deleteBarangStokLog($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->delete('barang_stock_logs');
		return $result;
	}

	public function getDetailBarangStok($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->get('barang_stocks')->result_array();
		return $result[0];
	}

	public function insertBarangStokKeluar($stock)
	{
		$hasil = $stock - $this->input->post('jumlah');
		$updateBarangStock = array(
			'stock' => $hasil,
		);
		$this->db->where('id', $this->input->post('barang_stock_id'));
		$result = $this->db->update('barang_stocks', $updateBarangStock);

		$insertBarangStockLog = array(
			'barang_stock_id' => $this->input->post('barang_stock_id'),
			'type' => 'keluar',
			'jumlah' => $this->input->post('jumlah'),
			'keterangan' => $this->input->post('keterangan'),
		);
		$this->db->set('created_at', 'NOW()', FALSE);
		$this->db->set('updated_at', 'NOW()', FALSE);
		$result = $this->db->insert('barang_stock_logs', $insertBarangStockLog);

		return $result;
	}

	public function deleteBarangStok($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->delete('barang_stocks');
		$this->db->where('barang_stock_id', $id);
		$result = $this->db->delete('barang_stock_logs');
		$this->db->where('barang_stock_id', $id);
		$result = $this->db->delete('barang_stock_uses');
		return $result;
	}

	public function getCountBarangStok()
	{
		$this->db->order_by('created_at', 'desc');
		$result = $this->db->get('barang_stocks')->num_rows();
		return $result;
	}
                        
}


/* End of file Barangstok_model.php and path \application\models\Barangstok_model.php */
