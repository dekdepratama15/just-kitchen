<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
                        
class pengajuancuti_model extends CI_Model 
{
	public function getPengajuanCuti()
	{
		$this->db->select('jadwals.*, cuti_jadwals.*,  jadwals.tanggal AS tanggal_jadwal, cuti_jadwals.status AS status_cuti, cuti_jadwals.created_at AS cuti_ca, cuti_jadwals.id AS cuti_jadwal_id');
		$this->db->join('jadwals', 'jadwals.id = cuti_jadwals.jadwal_id');
		$this->db->order_by('cuti_jadwals.created_at', 'desc');
		$this->db->where('cuti_jadwals.user_id', $this->session->userdata('user_id'));
		$result = $this->db->get('cuti_jadwals');
		return $result;
	}

	public function getPengajuanCutiAll()
	{
		$this->db->select('jadwals.*, cuti_jadwals.*,karyawans.*, jadwals.tanggal AS tanggal_jadwal, cuti_jadwals.status AS status_cuti, cuti_jadwals.created_at AS cuti_ca, cuti_jadwals.id AS cuti_jadwal_id');
		$this->db->join('jadwals', 'jadwals.id = cuti_jadwals.jadwal_id');
		$this->db->join('karyawans', 'karyawans.user_id = cuti_jadwals.user_id');
		$this->db->order_by('cuti_jadwals.created_at', 'desc');
		$result = $this->db->get('cuti_jadwals');
		return $result;
	}

	public function insertPengajuan()
	{
		$user_id = $this->session->userdata('user_id');
		$jadwal_id = $this->input->post('jadwal_id');
		$surat_sakit = $_FILES['surat_sakit']['name'];
		foreach ($jadwal_id as $jadwal_id) {
			if($surat_sakit == ''){
				$this->db->where('jadwal_id', $jadwal_id);
				$result = $this->db->delete('cuti_jadwals');
				$insert = array(
					'jadwal_id' => $jadwal_id,
					'user_id' => $user_id,
					'tanggal' => $this->input->post('tanggal'),
					'status' => 'pending',
					'jenis' => $this->input->post('jenis'),
					'alasan' => $this->input->post('alasan'),

				);
				$this->db->set('created_at', 'NOW()', FALSE);
				$this->db->set('updated_at', 'NOW()', FALSE);
				$result = $this->db->insert('cuti_jadwals', $insert);
			}else{
				$this->db->where('jadwal_id', $jadwal_id);
				$result = $this->db->delete('cuti_jadwals');
				$insert = array(
					'jadwal_id' => $jadwal_id,
					'user_id' => $user_id,
					'tanggal' => $this->input->post('tanggal'),
					'status' => 'pending',
					'jenis' => $this->input->post('jenis'),
					'alasan' => $this->input->post('alasan'),

				);
				$this->db->set('created_at', 'NOW()', FALSE);
				$this->db->set('updated_at', 'NOW()', FALSE);
				$result = $this->db->insert('cuti_jadwals', $insert);
				
				$query = $this->db->get_where('jadwals', array('id' => $jadwal_id))->row();

				if ($query->surat_sakit != NULL) {
					unlink(FCPATH . "/assets/suratsakit/" . $query->surat_sakit);
				}

				date_default_timezone_set('Asia/Makassar');
				$date_now = date('dmYHis');
				$file_name = str_replace('.', '', 'suratsakit_' . $user_id . '_' . $date_now);
				$config['upload_path']          = FCPATH . '/assets/suratsakit';
				$config['allowed_types']        = 'pdf|jpg|jpeg|png';
				$config['file_name']            = $file_name;
				$config['overwrite']            = true;
				$config['max_size']             = 5120; // 5MB

				$this->load->library('upload', $config);
				if (!$this->upload->do_upload('surat_sakit')) {
					$data['error'] = $this->upload->display_errors();
					$this->session->set_flashdata('error', $this->upload->display_errors());
					redirect('pengajuancuti/tambah');
				} else {
					$uploaded_data = $this->upload->data();

					$edit = array(
						'status' => 'sakit',
						'surat_sakit' => $uploaded_data['file_name'],
					);
					$this->db->set('updated_at', 'NOW()', FALSE);
					$this->db->where('id', $jadwal_id);
					$result = $this->db->update('jadwals', $edit);
				}
			}
		}

		return $result;
	}

	public function getDetailPengajuanCuti($id)
	{
		$this->db->select('jadwals.*, cuti_jadwals.*,  jadwals.tanggal AS tanggal_jadwal, cuti_jadwals.status AS status_cuti, cuti_jadwals.created_at AS cuti_ca, cuti_jadwals.id AS cuti_jadwal_id');
		$this->db->join('jadwals', 'jadwals.id = cuti_jadwals.jadwal_id');
		$this->db->order_by('cuti_jadwals.created_at', 'desc');
		$this->db->where('cuti_jadwals.id', $id);
		$result = $this->db->get('cuti_jadwals')->result_array();
		return $result[0];
	}

	public function editPengajuan($id)
	{
		$edit = array(
			'tanggal' => $this->input->post('tanggal'),
			'jenis' => $this->input->post('jenis'),
			'alasan' => $this->input->post('alasan'),
		);
		$this->db->set('updated_at', 'NOW()', FALSE);
		$this->db->where('id', $id);
		$result = $this->db->update('cuti_jadwals', $edit);

		return $result;
	}

	public function deletePengajuan($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->delete('cuti_jadwals');
		return $result;
	}
                        
}


/* End of file Pengajuancuti_model.php and path \application\models\Pengajuancuti_model.php */
