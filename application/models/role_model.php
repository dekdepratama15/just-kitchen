<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
                        
class role_model extends CI_Model 
{
	public function cek_admin()
	{
		$allowedRoles = array('admin');

		$userRole = $this->session->userdata('role');
		if (!in_array($userRole, $allowedRoles)) {
			redirect('dashboard');
		}
	}

	public function cek_user()
	{
		$allowedRoles = array('user');

		$userRole = $this->session->userdata('role');
		if (!in_array($userRole, $allowedRoles)) {
			redirect('dashboard');
		}
	}
                        
}


/* End of file Checkrole_model.php and path \application\models\Checkrole_model.php */
