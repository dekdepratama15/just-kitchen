<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
                        
class pindahjadwal_model extends CI_Model 
{
    public function getPindahJadwal()
    {
		$this->db->select('jadwals.*, pindah_jadwals.*,  jadwals.tanggal AS tanggal_lama, jadwals.jadwal_masuk AS jadwal_masuk_lama, jadwals.jadwal_keluar AS jadwal_keluar_lama, pindah_jadwals.status AS status_pindah, pindah_jadwals.created_at AS pindah_ca, pindah_jadwals.id AS pindah_jadwal_id');
		$this->db->join('jadwals', 'jadwals.id = pindah_jadwals.jadwal_id');
		$this->db->order_by('pindah_jadwals.created_at', 'desc');
		$this->db->where('pindah_jadwals.user_id', $this->session->userdata('user_id'));
		$result = $this->db->get('pindah_jadwals');
		return $result;
    }

	public function getPindahJadwalAll()
	{
		$this->db->select('jadwals.*, pindah_jadwals.*,karyawans.*, jadwals.tanggal AS tanggal_lama, jadwals.jadwal_masuk AS jadwal_masuk_lama, jadwals.jadwal_keluar AS jadwal_keluar_lama, pindah_jadwals.status AS status_pindah, pindah_jadwals.created_at AS pindah_ca, pindah_jadwals.id AS pindah_jadwal_id');
		$this->db->join('jadwals', 'jadwals.id = pindah_jadwals.jadwal_id');
		$this->db->join('karyawans', 'karyawans.user_id = pindah_jadwals.user_id');
		$this->db->order_by('pindah_jadwals.created_at', 'desc');
		$result = $this->db->get('pindah_jadwals');
		return $result;
	}

	public function getDetailPindahJadwal($id)
	{
		$this->db->select('jadwals.*, pindah_jadwals.*,karyawans.*, jadwals.tanggal AS tanggal_lama, jadwals.jadwal_masuk AS jadwal_masuk_lama, jadwals.jadwal_keluar AS jadwal_keluar_lama, pindah_jadwals.status AS status_pindah, pindah_jadwals.created_at AS pindah_ca, pindah_jadwals.id AS pindah_jadwal_id');
		$this->db->join('jadwals', 'jadwals.id = pindah_jadwals.jadwal_id');
		$this->db->join('karyawans', 'karyawans.user_id = pindah_jadwals.user_id');
		$this->db->where('pindah_jadwals.id', $id);
		$result = $this->db->get('pindah_jadwals')->result_array();
		return $result[0];
	}

	public function pindahJadwal($id)
	{
		$this->db->where('jadwal_id', $id);
		$result = $this->db->delete('pindah_jadwals');

			$user_id = $this->session->userdata('user_id');
			$insert = array(
				'jadwal_id' => $id,
				'user_id' => $user_id,
				'tanggal' => $this->input->post('tanggal_baru'),
				'alasan' => $this->input->post('alasan'),
				'jadwal_masuk' => $this->input->post('jadwal_masuk_baru'),
				'jadwal_keluar' => $this->input->post('jadwal_keluar_baru'),
				'status' => 'pending'

			);
			$this->db->set('created_at', 'NOW()', FALSE);
			$this->db->set('updated_at', 'NOW()', FALSE);
			$result = $this->db->insert('pindah_jadwals', $insert);
		
		return $result;
	}

	public function editPindahJadwal($id)
	{
		$edit = array(
			'tanggal' => $this->input->post('tanggal_baru'),
			'alasan' => $this->input->post('alasan'),
			'jadwal_masuk' => $this->input->post('jadwal_masuk_baru'),
			'jadwal_keluar' => $this->input->post('jadwal_keluar_baru'),
		);
		$this->db->set('updated_at', 'NOW()', FALSE);
		$this->db->where('id', $id);
		$result = $this->db->update('pindah_jadwals', $edit);

		return $result;
	}

	public function deletePindahJadwal($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->delete('pindah_jadwals');
		return $result;
	}
                        
}


/* End of file Pindah_jadwal_model.php and path \application\models\Pindah_jadwal_model.php */
