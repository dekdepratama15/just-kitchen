<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
                        
class karyawan_model extends CI_Model 
{
    public function getKaryawan()
	{
		$this->db->select('users.*, karyawans.*, karyawans.id AS karyawan_id');
		$this->db->join('users', 'karyawans.user_id = users.id');
		$this->db->order_by('karyawans.tgl_bergabung','desc');
		$result = $this->db->get('karyawans');
		return $result;
	}

	public function insertKaryawan()
	{
		if($this->input->post('password') == $this->input->post('repassword')){
			$insertUser = array(
				'username' => $this->input->post('username'),
				'email' => $this->input->post('email'),
				'password' => md5($this->input->post('password')),
				'role' => 'user',
			);
			$this->db->set('created_at', 'NOW()',FALSE);
			$this->db->set('updated_at', 'NOW()',FALSE);
			$result = $this->db->insert('users', $insertUser);
			$query = $this->db->get_where('users', array('username' => $this->input->post('username')))->row();
			$user_id = $query->id;

			$insertKaryawan = array(
				'user_id' => $user_id,
				'jabatan' => $this->input->post('jabatan'),
				'nama_lengkap' => $this->input->post('nama_lengkap'),
				'nama_panggilan' => $this->input->post('nama_panggilan'),
				'status' => $this->input->post('status'),
				'jenis_kelamin' => $this->input->post('jenis_kelamin'),
				'golongan_darah' => $this->input->post('golongan_darah'),
				'agama' => $this->input->post('agama'),
				'status_perkawinan' => $this->input->post('status_perkawinan'),
				'status_karyawan' => $this->input->post('status_karyawan'),
				'jenis_identitas' => $this->input->post('jenis_identitas'),
				'no_identitas' => $this->input->post('no_identitas'),
				'no_telp' => $this->input->post('no_telp'),
				'tgl_lahir' => $this->input->post('tgl_lahir'),
				'tempat_lahir' => $this->input->post('tempat_lahir'),
				'alamat' => $this->input->post('alamat'),
				'tgl_bergabung' => $this->input->post('tgl_bergabung'),
				'tgl_selesai' => NULL,
			);
			$this->db->set('created_at', 'NOW()',FALSE);
			$this->db->set('updated_at', 'NOW()',FALSE);
			$result = $this->db->insert('karyawans', $insertKaryawan);
		}
		else{
			$this->session->set_flashdata('error', 'Password dan Repassword tidak sesuai !');
			redirect('karyawan/tambah');
		}

		return $result;
	}

	public function deleteKaryawan($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->delete('users');
		$this->db->where('user_id', $id);
		$result = $this->db->delete('karyawans');
		return $result;
	}

	public function getDetailKaryawan($id)
	{
		$this->db->select('users.*, karyawans.*, karyawans.id AS karyawan_id');
		$this->db->join('users', 'karyawans.user_id = users.id');
		$this->db->where('karyawans.user_id', $id);
		$result = $this->db->get('karyawans')->result_array();
		return $result[0];
	}

	public function editKaryawan($id)
	{
		if($this->input->post('password') != ''){
			if($this->input->post('password') == $this->input->post('repassword')){
				if(strlen($this->input->post('password')) >= 5){
					$editUser = array(
						'username' => $this->input->post('username'),
						'email' => $this->input->post('email'),
						'password' => md5($this->input->post('password')),
					);
					$this->db->set('updated_at', 'NOW()', FALSE);
					$this->db->where('id', $id);
					$result = $this->db->update('users', $editUser);

					if($this->input->post('tgl_selesai') != ''){
						$editKaryawan = array(
							'jabatan' => $this->input->post('jabatan'),
							'nama_lengkap' => $this->input->post('nama_lengkap'),
							'nama_panggilan' => $this->input->post('nama_panggilan'),
							'status' => $this->input->post('status'),
							'jenis_kelamin' => $this->input->post('jenis_kelamin'),
							'golongan_darah' => $this->input->post('golongan_darah'),
							'agama' => $this->input->post('agama'),
							'status_perkawinan' => $this->input->post('status_perkawinan'),
							'status_karyawan' => $this->input->post('status_karyawan'),
							'jenis_identitas' => $this->input->post('jenis_identitas'),
							'no_identitas' => $this->input->post('no_identitas'),
							'no_telp' => $this->input->post('no_telp'),
							'tgl_lahir' => $this->input->post('tgl_lahir'),
							'tempat_lahir' => $this->input->post('tempat_lahir'),
							'alamat' => $this->input->post('alamat'),
							'tgl_bergabung' => $this->input->post('tgl_bergabung'),
							'tgl_selesai' => $this->input->post('tgl_selesai'),
						);
					}
					else{
						$editKaryawan = array(
							'jabatan' => $this->input->post('jabatan'),
							'nama_lengkap' => $this->input->post('nama_lengkap'),
							'nama_panggilan' => $this->input->post('nama_panggilan'),
							'status' => $this->input->post('status'),
							'jenis_kelamin' => $this->input->post('jenis_kelamin'),
							'golongan_darah' => $this->input->post('golongan_darah'),
							'agama' => $this->input->post('agama'),
							'status_perkawinan' => $this->input->post('status_perkawinan'),
							'status_karyawan' => $this->input->post('status_karyawan'),
							'jenis_identitas' => $this->input->post('jenis_identitas'),
							'no_identitas' => $this->input->post('no_identitas'),
							'no_telp' => $this->input->post('no_telp'),
							'tgl_lahir' => $this->input->post('tgl_lahir'),
							'tempat_lahir' => $this->input->post('tempat_lahir'),
							'alamat' => $this->input->post('alamat'),
							'tgl_bergabung' => $this->input->post('tgl_bergabung'),
							'tgl_selesai' => NULL,
						);
					}
					
					$this->db->set('updated_at', 'NOW()', FALSE);
					$this->db->where('user_id', $id);
					$result = $this->db->update('karyawans', $editKaryawan);
				}
				else{
					$this->session->set_flashdata('error', 'Password setidaknya harus 5 karakter atau lebih !');
					redirect('karyawan/edit/' . $id);
				}
			}
			else{
				$this->session->set_flashdata('error', 'Password dan Repassword tidak sesuai !');
				redirect('karyawan/edit/' . $id);
			}
		}
		else{
			$editUser = array(
				'username' => $this->input->post('username'),
				'email' => $this->input->post('email'),
			);
			$this->db->set('updated_at', 'NOW()', FALSE);
			$this->db->where('id', $id);
			$result = $this->db->update('users', $editUser);

			if ($this->input->post('tgl_selesai') != '') {
				$editKaryawan = array(
					'jabatan' => $this->input->post('jabatan'),
					'nama_lengkap' => $this->input->post('nama_lengkap'),
					'nama_panggilan' => $this->input->post('nama_panggilan'),
					'status' => $this->input->post('status'),
					'jenis_kelamin' => $this->input->post('jenis_kelamin'),
					'golongan_darah' => $this->input->post('golongan_darah'),
					'agama' => $this->input->post('agama'),
					'status_perkawinan' => $this->input->post('status_perkawinan'),
					'status_karyawan' => $this->input->post('status_karyawan'),
					'jenis_identitas' => $this->input->post('jenis_identitas'),
					'no_identitas' => $this->input->post('no_identitas'),
					'no_telp' => $this->input->post('no_telp'),
					'tgl_lahir' => $this->input->post('tgl_lahir'),
					'tempat_lahir' => $this->input->post('tempat_lahir'),
					'alamat' => $this->input->post('alamat'),
					'tgl_bergabung' => $this->input->post('tgl_bergabung'),
					'tgl_selesai' => $this->input->post('tgl_selesai'),
				);
			} else {
				$editKaryawan = array(
					'jabatan' => $this->input->post('jabatan'),
					'nama_lengkap' => $this->input->post('nama_lengkap'),
					'nama_panggilan' => $this->input->post('nama_panggilan'),
					'status' => $this->input->post('status'),
					'jenis_kelamin' => $this->input->post('jenis_kelamin'),
					'golongan_darah' => $this->input->post('golongan_darah'),
					'agama' => $this->input->post('agama'),
					'status_perkawinan' => $this->input->post('status_perkawinan'),
					'status_karyawan' => $this->input->post('status_karyawan'),
					'jenis_identitas' => $this->input->post('jenis_identitas'),
					'no_identitas' => $this->input->post('no_identitas'),
					'no_telp' => $this->input->post('no_telp'),
					'tgl_lahir' => $this->input->post('tgl_lahir'),
					'tempat_lahir' => $this->input->post('tempat_lahir'),
					'alamat' => $this->input->post('alamat'),
					'tgl_bergabung' => $this->input->post('tgl_bergabung'),
					'tgl_selesai' => NULL,
				);
			}
			$this->db->set('updated_at', 'NOW()', FALSE);
			$this->db->where('user_id', $id);
			$result = $this->db->update('karyawans', $editKaryawan);
		}
		

		return $result;
	}

	public function getCountKaryawan()
	{
		$this->db->select('users.*, karyawans.*, karyawans.id AS karyawan_id');
		$this->db->join('users', 'karyawans.user_id = users.id');
		$this->db->order_by('karyawans.tgl_bergabung', 'desc');
		$result = $this->db->get('karyawans')->num_rows();
		return $result;
	}
                        
}


/* End of file Karyawan_model.php and path \application\models\Karyawan_model.php */
