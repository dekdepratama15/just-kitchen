<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
                        
class barangmentah_model extends CI_Model 
{
	public function getBarangMentah()
	{
		$this->db->order_by('created_at', 'desc');
		$result = $this->db->get('barang_mentahs');
		return $result;
	}

	public function getBarangMentah2()
	{
		$this->db->order_by('created_at', 'desc');
		$result = $this->db->get('barang_mentahs')->result_array();
		return $result;
	}

	public function getBarangMentahLog()
	{
		$this->db->select('barang_mentahs.*, barang_mentah_logs.*, barang_mentahs.id AS barang_mentah_id, barang_mentah_logs.id AS barang_mentah_logs_id, barang_mentah_logs.created_at AS ca_bmentahlog, barang_mentah_logs.updated_at AS ua_bmentahlog, barang_mentah_logs.keterangan AS bmlogketerangan');
		$this->db->join('barang_mentahs', 'barang_mentah_logs.barang_mentah_id = barang_mentahs.id');
		$this->db->order_by('barang_mentah_logs.created_at', 'desc');
		$result = $this->db->get('barang_mentah_logs');
		return $result;
	}

	public function insertBarangMentah()
	{
			$insertBarangMentah = array(
				'nama_barang' => $this->input->post('nama_barang'),
				'harga' => $this->input->post('harga'),
				'keterangan' => $this->input->post('keterangan'),
				'stock' => $this->input->post('stock'),
			);
			$this->db->set('created_at', 'NOW()', FALSE);
			$this->db->set('updated_at', 'NOW()', FALSE);
			$result = $this->db->insert('barang_mentahs', $insertBarangMentah);
			$barang_mentah_id = $this->db->insert_id();

			$insertBarangMentahLog = array(
				'barang_mentah_id' => $barang_mentah_id,
				'type' => 'masuk',
				'jumlah' => $this->input->post('stock'),
				'keterangan' => $this->input->post('keterangan'),
			);
			$this->db->set('created_at', 'NOW()', FALSE);
			$this->db->set('updated_at', 'NOW()', FALSE);
			$result = $this->db->insert('barang_mentah_logs', $insertBarangMentahLog);

		return $result;
	}

	public function deleteBarangMentah($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->delete('barang_mentahs');
		$this->db->where('barang_mentah_id', $id);
		$result = $this->db->delete('barang_mentah_logs');
		return $result;
	}

	public function deleteBarangMentahLog($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->delete('barang_mentah_logs');
		return $result;
	}

	public function getDetailBarangMentah($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->get('barang_mentahs')->result_array();
		return $result[0];
	}

	public function editBarangMentah($id)
	{
		$updateBarangMentah = array(
			'nama_barang' => $this->input->post('nama_barang'),
			'harga' => $this->input->post('harga'),
			'keterangan' => $this->input->post('keterangan'),
			'stock' => $this->input->post('stock'),
		);
		$this->db->set('updated_at', 'NOW()', FALSE);
		$this->db->where('id', $id);
		$result = $this->db->update('barang_mentahs', $updateBarangMentah);

		if($this->input->post('stock')>=1){
			$insertBarangMentahLog = array(
				'barang_mentah_id' => $id,
				'type' => 'masuk',
				'jumlah' => $this->input->post('stock'),
				'keterangan' => $this->input->post('keterangan'),
			);
			$this->db->set('created_at', 'NOW()', FALSE);
			$this->db->set('updated_at', 'NOW()', FALSE);
			$result = $this->db->insert('barang_mentah_logs', $insertBarangMentahLog);
		}

		return $result;
	}

	public function insertBarangMentahKeluar($stock)
	{
		$hasil = $stock - $this->input->post('jumlah');
		$updateBarangMentah = array(
			'stock' => $hasil,
		);
		$this->db->where('id', $this->input->post('barang_mentah_id'));
		$result = $this->db->update('barang_mentahs', $updateBarangMentah);

		$insertBarangMentahLog = array(
			'barang_mentah_id' => $this->input->post('barang_mentah_id'),
			'type' => 'keluar',
			'jumlah' => $this->input->post('jumlah'),
			'keterangan' => $this->input->post('keterangan'),
		);
		$this->db->set('created_at', 'NOW()', FALSE);
		$this->db->set('updated_at', 'NOW()', FALSE);
		$result = $this->db->insert('barang_mentah_logs', $insertBarangMentahLog);

		return $result;
	}

	public function getCountBarangMentah()
	{
		$this->db->order_by('created_at', 'desc');
		$result = $this->db->get('barang_mentahs')->num_rows();
		return $result;
	}
                        
}


/* End of file Barangmentah_model.php and path \application\models\Barangmentah_model.php */
