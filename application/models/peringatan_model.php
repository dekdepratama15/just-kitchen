<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
                        
class peringatan_model extends CI_Model 
{
	public function getPeringatan()
	{
		$this->db->select('peringatans.*, users.*, karyawans.*, peringatans.id AS peringatan_id, peringatans.created_at AS peringatan_ca');
		$this->db->join('users', 'peringatans.user_id = users.id');
		$this->db->join('karyawans', 'peringatans.karyawan_id = karyawans.id');
		$this->db->order_by('peringatans.created_at', 'desc');
		$result = $this->db->get('peringatans');
		return $result;
	}

	public function insertPeringatan()
	{
		$file = $_FILES['file']['name'];
		date_default_timezone_set('Asia/Makassar');
		$date_now = date('dmYHis');
		if ($file != "") {
			$file_name = str_replace('.', '', 'peringatan_' . $this->session->userdata('user_id') .'_'. $date_now);
			$config['upload_path']          = FCPATH . 'assets/peringatan';
			$config['allowed_types']        = 'pdf';
			$config['file_name']            = $file_name;
			$config['overwrite']            = true;
			$config['max_size']             = 5120; // 5MB

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('file')) {
				$data['error'] = $this->upload->display_errors();
				$this->session->set_flashdata('error', $this->upload->display_errors());
				redirect('peringatan/tambah');
			} else {
				$uploaded_data = $this->upload->data();

				$insert = array(
					'karyawan_id' => $this->input->post('karyawan_id'),
					'user_id' => $this->session->userdata('user_id'),
					'nomor' => $this->input->post('nomor'),
					'jenis' => $this->input->post('jenis'),
					'alasan' => $this->input->post('alasan'),
					'file' => $uploaded_data['file_name'],
				);
				$this->db->set('created_at', 'NOW()', FALSE);
				$this->db->set('updated_at', 'NOW()', FALSE);
				$result = $this->db->insert('peringatans', $insert);
			}
		} else {
			$this->session->set_flashdata('error', 'File PDF tidak boleh kosong');
			redirect('peringatan/tambah');
		}

		return $result;
	}

	public function deletePeringatan($id)
	{
		$query = $this->db->get_where('peringatans', array('id' => $id))->row();
		unlink(FCPATH . "/assets/peringatan/" . $query->file);
		$this->db->where('id', $id);
		$result = $this->db->delete('peringatans');
		return $result;
	}

	public function getDetailPeringatan($id)
	{
		$this->db->select('peringatans.*, users.*, karyawans.*, peringatans.id AS peringatan_id, peringatans.created_at AS peringatan_ca');
		$this->db->join('users', 'peringatans.user_id = users.id');
		$this->db->join('karyawans', 'peringatans.karyawan_id = karyawans.id');
		$this->db->where('peringatans.id', $id);
		$result = $this->db->get('peringatans')->result_array();
		return $result[0];
	}

	public function editPeringatan($id)
	{
		$file = $_FILES['file']['name'];

		if ($file == "") {
			$edit = array(
				'karyawan_id' => $this->input->post('karyawan_id'),
				'user_id' => $this->session->userdata('user_id'),
				'nomor' => $this->input->post('nomor'),
				'jenis' => $this->input->post('jenis'),
				'alasan' => $this->input->post('alasan'),
			);
			$this->db->set('updated_at', 'NOW()', FALSE);
			$this->db->where('id', $id);
			$result = $this->db->update('peringatans', $edit);
		} elseif ($file != "") {
			$query = $this->db->get_where('peringatans', array('id' => $id))->row();
			unlink(FCPATH . "/assets/peringatan/" . $query->file);
			date_default_timezone_set('Asia/Makassar');
			$date_now = date('dmYHis');
			$file_name = str_replace('.', '', 'peringatan_' . $this->session->userdata('user_id') . '_' . $date_now);
			$config['upload_path']          = FCPATH . '/assets/peringatan';
			$config['allowed_types']        = 'pdf';
			$config['file_name']            = $file_name;
			$config['overwrite']            = true;
			$config['max_size']             = 5120; // 5MB

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('file')) {
				$data['error'] = $this->upload->display_errors();
				$this->session->set_flashdata('error', $this->upload->display_errors());
				redirect('peringatan/edit/' . $id);
			} else {
				$uploaded_data = $this->upload->data();

				$edit = array(
					'karyawan_id' => $this->input->post('karyawan_id'),
					'user_id' => $this->session->userdata('user_id'),
					'nomor' => $this->input->post('nomor'),
					'jenis' => $this->input->post('jenis'),
					'alasan' => $this->input->post('alasan'),
					'file' => $uploaded_data['file_name'],
				);
				$this->db->set('updated_at', 'NOW()', FALSE);
				$this->db->where('id', $id);
				$result = $this->db->update('peringatans', $edit);
			}
		}
		return $result;
	}

	public function getPeringatanKaryawan($id)
	{
		$this->db->where('karyawan_id', $id);
		$query = $this->db->get('peringatans');
		if($query->num_rows() > 0){
			$this->db->select('peringatans.*, users.*, karyawans.*, peringatans.id AS peringatan_id, peringatans.created_at AS peringatan_ca');
			$this->db->join('users', 'peringatans.user_id = users.id');
			$this->db->join('karyawans', 'peringatans.karyawan_id = karyawans.id');
			$this->db->where('peringatans.karyawan_id', $id);
			$this->db->order_by('peringatans.created_at', 'desc');
			$result = $this->db->get('peringatans');
			return $result;
		}else{
			return '';
		}
	}
                        
}


/* End of file Peringatan_model.php and path \application\models\Peringatan_model.php */
