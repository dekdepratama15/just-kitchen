<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class auth extends CI_Controller {

	public $session,$input,$db;

    public function __construct()
    {
        parent::__construct();
		
		$this->load->model('auth_model');
		
    }

    public function index()
    {
		$this->load->view('auth/login');
    }

	public function process()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$query = $this->db->get_where('users',array('username'=>$username));
        $user = $query->row();
        $role = $user->role;
		$user_id = $user->id;
            if(md5($password) == $user->password) {
				$this->session->set_userdata('user_id',$user_id);
                $this->session->set_userdata('username',$username);
				$this->session->set_userdata('role',$role);
				$this->session->set_userdata('email',$user->email);
				$this->session->set_userdata('is_login',TRUE);

                redirect('dashboard');

            } else {
                $this->session->set_flashdata('error','Username / Password tidak sesuai');
				redirect('auth');
            }
	}

	public function logout()
	{                    
		session_start();        
		session_destroy();
		redirect('auth');
	}
}

/* End of file Auth.php and path \application\controllers\Auth.php */
