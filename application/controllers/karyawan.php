<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class karyawan extends CI_Controller {

	public $karyawan_model,$form_validation,$session,$input, $auth_model, $role_model;

    public function __construct()
    {
        parent::__construct();
		$this->load->model('karyawan_model');
		$this->load->model('auth_model');
		$this->load->model('role_model');
		$this->auth_model->cek_login();
		$this->role_model->cek_admin();
    }

    public function index(){
			$data['karyawans'] = $this->karyawan_model->getKaryawan();
			$this->load->view('layouts/header');
			$this->load->view('layouts/menu');
			$this->load->view('karyawan/index',$data);
			$this->load->view('layouts/footer');
    }

	public function tambah()
	{
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('karyawan/tambah');
		$this->load->view('layouts/footer');
	}

	public function add()
	{
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[1]|max_length[255]|is_unique[users.username]');
		$this->form_validation->set_rules('email', 'Email', 'required|min_length[1]|max_length[255]|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
		$this->form_validation->set_rules('repassword', 'Re-Password', 'required|min_length[5]');
		$this->form_validation->set_rules('jabatan', 'Jabatan', 'required|min_length[1]');
		$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required|min_length[1]');
		$this->form_validation->set_rules('nama_panggilan', 'Nama Panggilan', 'required|min_length[1]');
		$this->form_validation->set_rules('status', 'Status', 'required|min_length[1]');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required|min_length[1]');
		$this->form_validation->set_rules('golongan_darah', 'Golongan Darah', 'required|min_length[1]');
		$this->form_validation->set_rules('agama', 'Agama', 'required|min_length[1]');
		$this->form_validation->set_rules('status_perkawinan', 'Status Perkawinan', 'required|min_length[1]');
		$this->form_validation->set_rules('status_karyawan', 'Status Karyawan', 'required|min_length[1]');
		$this->form_validation->set_rules('jenis_identitas', 'Jenis Identitas', 'required|min_length[1]');
		$this->form_validation->set_rules('no_identitas', 'No Identitas', 'required|min_length[1]');
		$this->form_validation->set_rules('no_telp', 'No Telp', 'required|min_length[1]');
		$this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required');
		$this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required|min_length[1]');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required|min_length[1]');
		$this->form_validation->set_rules('tgl_bergabung', 'Tanggal Bergabung', 'required');

		if ($this->form_validation->run() == true) {
			$this->karyawan_model->insertKaryawan();
			$this->session->set_flashdata('success', 'Berhasil menambah karyawan !');
			redirect('karyawan');
		} else {
			$this->session->set_flashdata('error', validation_errors());
			redirect('karyawan/tambah');
		}
	}

	public function hapus($id)
	{
		$this->karyawan_model->deleteKaryawan($id);
		$this->session->set_flashdata('success', 'Berhasil menghapus karyawan!');
		redirect('karyawan');
	}

	public function edit($id)
	{
		$data['karyawan'] = $this->karyawan_model->getDetailKaryawan($id);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('karyawan/edit', $data);
		$this->load->view('layouts/footer');
	}

	public function editProcess($id)
	{
		$data = $this->karyawan_model->getDetailKaryawan($id);
		if ($data['username'] == $this->input->post('username') && $data['email'] == $this->input->post('email')) {
			$this->form_validation->set_rules('username', 'Username', 'required|min_length[1]|max_length[255]');
			$this->form_validation->set_rules('email', 'Email', 'required|min_length[1]|max_length[255]');
			$this->form_validation->set_rules('jabatan', 'Jabatan', 'required|min_length[1]');
			$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required|min_length[1]');
			$this->form_validation->set_rules('nama_panggilan', 'Nama Panggilan', 'required|min_length[1]');
			$this->form_validation->set_rules('status', 'Status', 'required|min_length[1]');
			$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required|min_length[1]');
			$this->form_validation->set_rules('golongan_darah', 'Golongan Darah', 'required|min_length[1]');
			$this->form_validation->set_rules('agama', 'Agama', 'required|min_length[1]');
			$this->form_validation->set_rules('status_perkawinan', 'Status Perkawinan', 'required|min_length[1]');
			$this->form_validation->set_rules('status_karyawan', 'Status Karyawan', 'required|min_length[1]');
			$this->form_validation->set_rules('jenis_identitas', 'Jenis Identitas', 'required|min_length[1]');
			$this->form_validation->set_rules('no_identitas', 'No Identitas', 'required|min_length[1]');
			$this->form_validation->set_rules('no_telp', 'No Telp', 'required|min_length[1]');
			$this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required');
			$this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required|min_length[1]');
			$this->form_validation->set_rules('alamat', 'Alamat', 'required|min_length[1]');
			$this->form_validation->set_rules('tgl_bergabung', 'Tanggal Bergabung', 'required');
		}
		elseif($data['username'] != $this->input->post('username') && $data['email'] == $this->input->post('email')){
			$this->form_validation->set_rules('username', 'Username', 'required|min_length[1]|max_length[255]|is_unique[users.username]');
			$this->form_validation->set_rules('email', 'Email', 'required|min_length[1]|max_length[255]');
			$this->form_validation->set_rules('jabatan', 'Jabatan', 'required|min_length[1]');
			$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required|min_length[1]');
			$this->form_validation->set_rules('nama_panggilan', 'Nama Panggilan', 'required|min_length[1]');
			$this->form_validation->set_rules('status', 'Status', 'required|min_length[1]');
			$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required|min_length[1]');
			$this->form_validation->set_rules('golongan_darah', 'Golongan Darah', 'required|min_length[1]');
			$this->form_validation->set_rules('agama', 'Agama', 'required|min_length[1]');
			$this->form_validation->set_rules('status_perkawinan', 'Status Perkawinan', 'required|min_length[1]');
			$this->form_validation->set_rules('status_karyawan', 'Status Karyawan', 'required|min_length[1]');
			$this->form_validation->set_rules('jenis_identitas', 'Jenis Identitas', 'required|min_length[1]');
			$this->form_validation->set_rules('no_identitas', 'No Identitas', 'required|min_length[1]');
			$this->form_validation->set_rules('no_telp', 'No Telp', 'required|min_length[1]');
			$this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required');
			$this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required|min_length[1]');
			$this->form_validation->set_rules('alamat', 'Alamat', 'required|min_length[1]');
			$this->form_validation->set_rules('tgl_bergabung', 'Tanggal Bergabung', 'required');
		} 
		elseif ($data['username'] == $this->input->post('username') && $data['email'] != $this->input->post('email')) {
			$this->form_validation->set_rules('username', 'Username', 'required|min_length[1]|max_length[255]');
			$this->form_validation->set_rules('email', 'Email', 'required|min_length[1]|max_length[255]|is_unique[users.email]');
			$this->form_validation->set_rules('jabatan', 'Jabatan', 'required|min_length[1]');
			$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required|min_length[1]');
			$this->form_validation->set_rules('nama_panggilan', 'Nama Panggilan', 'required|min_length[1]');
			$this->form_validation->set_rules('status', 'Status', 'required|min_length[1]');
			$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required|min_length[1]');
			$this->form_validation->set_rules('golongan_darah', 'Golongan Darah', 'required|min_length[1]');
			$this->form_validation->set_rules('agama', 'Agama', 'required|min_length[1]');
			$this->form_validation->set_rules('status_perkawinan', 'Status Perkawinan', 'required|min_length[1]');
			$this->form_validation->set_rules('status_karyawan', 'Status Karyawan', 'required|min_length[1]');
			$this->form_validation->set_rules('jenis_identitas', 'Jenis Identitas', 'required|min_length[1]');
			$this->form_validation->set_rules('no_identitas', 'No Identitas', 'required|min_length[1]');
			$this->form_validation->set_rules('no_telp', 'No Telp', 'required|min_length[1]');
			$this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required');
			$this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required|min_length[1]');
			$this->form_validation->set_rules('alamat', 'Alamat', 'required|min_length[1]');
			$this->form_validation->set_rules('tgl_bergabung', 'Tanggal Bergabung', 'required');
		}
		else{
			$this->form_validation->set_rules('username', 'Username', 'required|min_length[1]|max_length[255]|is_unique[users.username]');
			$this->form_validation->set_rules('email', 'Email', 'required|min_length[1]|max_length[255]|is_unique[users.email]');
			$this->form_validation->set_rules('jabatan', 'Jabatan', 'required|min_length[1]');
			$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required|min_length[1]');
			$this->form_validation->set_rules('nama_panggilan', 'Nama Panggilan', 'required|min_length[1]');
			$this->form_validation->set_rules('status', 'Status', 'required|min_length[1]');
			$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required|min_length[1]');
			$this->form_validation->set_rules('golongan_darah', 'Golongan Darah', 'required|min_length[1]');
			$this->form_validation->set_rules('agama', 'Agama', 'required|min_length[1]');
			$this->form_validation->set_rules('status_perkawinan', 'Status Perkawinan', 'required|min_length[1]');
			$this->form_validation->set_rules('status_karyawan', 'Status Karyawan', 'required|min_length[1]');
			$this->form_validation->set_rules('jenis_identitas', 'Jenis Identitas', 'required|min_length[1]');
			$this->form_validation->set_rules('no_identitas', 'No Identitas', 'required|min_length[1]');
			$this->form_validation->set_rules('no_telp', 'No Telp', 'required|min_length[1]');
			$this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required');
			$this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required|min_length[1]');
			$this->form_validation->set_rules('alamat', 'Alamat', 'required|min_length[1]');
			$this->form_validation->set_rules('tgl_bergabung', 'Tanggal Bergabung', 'required');
		}

		if ($this->form_validation->run() == true) {
			$this->karyawan_model->editKaryawan($id);
			$this->session->set_flashdata('success', 'Data karyawan berhasil diedit!');
			redirect('karyawan');
		} else {
			$this->session->set_flashdata('error', validation_errors());
			redirect('karyawan/edit/' . $id);
		}
	}

	public function detail($id)
	{
		$data['karyawan'] = $this->karyawan_model->getDetailKaryawan($id);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('karyawan/detail', $data);
		$this->load->view('layouts/footer');
	}
}

/* End of file Karyawan.php and path \application\controllers\Karyawan.php */
