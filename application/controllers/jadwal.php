<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class jadwal extends CI_Controller {

	public $jadwal_model, $form_validation, $session, $input, $db, $auth_model,$karyawan_model,$role_model;

    public function __construct()
    {
        parent::__construct();
		$this->load->model('jadwal_model');
		$this->load->model('karyawan_model');
		$this->load->model('auth_model');
		$this->auth_model->cek_login();
		$this->load->model('role_model');
		$this->role_model->cek_admin();
    }

	public function index()
	{
		$data['jadwals'] = $this->jadwal_model->getJadwal();
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('jadwal/index', $data);
		$this->load->view('layouts/footer');
	}

	public function tambah()
	{
		$data['karyawans'] = $this->karyawan_model->getKaryawan();
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('jadwal/tambah',$data);
		$this->load->view('layouts/footer');
	}

	public function add()
	{
		$this->form_validation->set_rules('karyawan_id[]', 'Karyawan', 'required');
		$this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('jadwal_masuk', 'Jadwal Masuk', 'required');
		$this->form_validation->set_rules('jadwal_keluar', 'Jadwal Keluar', 'required');

		if ($this->form_validation->run() == true) {
			$this->jadwal_model->insertJadwal();
			$this->session->set_flashdata('success', 'Berhasil menambah jadwal!');
			redirect('jadwal');
		} else {
			$this->session->set_flashdata('error', validation_errors());
			redirect('jadwal/tambah');
		}
	}

	public function hapus($id)
	{
		$this->jadwal_model->deleteJadwal($id);
		$this->session->set_flashdata('success', 'Berhasil menghapus jadwal!');
		redirect('jadwal');
	}

	public function edit($id)
	{
		$data['jadwal'] = $this->jadwal_model->getDetailJadwal($id);
		$data['karyawans'] = $this->karyawan_model->getKaryawan();
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('jadwal/edit', $data);
		$this->load->view('layouts/footer');
	}

	public function editProcess($id)
	{
		$this->form_validation->set_rules('karyawan_id', 'Karyawan', 'required|min_length[1]');
		$this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('jadwal_masuk', 'Jadwal Masuk', 'required');
		$this->form_validation->set_rules('jadwal_keluar', 'Jadwal Keluar', 'required');

		if ($this->form_validation->run() == true) {
			$this->jadwal_model->editJadwal($id);
			$this->session->set_flashdata('success', 'Jadwal berhasil diedit!');
			redirect('jadwal');
		} else {
			$this->session->set_flashdata('error', validation_errors());
			redirect('jadwal/edit/' . $id);
		}
	}
}

/* End of file Jadwal.php and path \application\controllers\Jadwal.php */
