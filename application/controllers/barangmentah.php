<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class barangmentah extends CI_Controller {

	public $barangmentah_model, $form_validation, $session, $pdfgenerator, $input, $auth_model, $role_model;

    public function __construct()
    {
        parent::__construct();
		$this->load->model('barangmentah_model');
		$this->load->model('auth_model');
		$this->load->model('role_model');
		$this->auth_model->cek_login();
		$this->role_model->cek_admin();
    }

    public function index()
    {
		$data['bmentah'] = $this->barangmentah_model->getBarangMentah();
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('barangmentah/index', $data);
		$this->load->view('layouts/footer');
    }

	public function tambah()
	{
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('barangmentah/tambah');
		$this->load->view('layouts/footer');
	}

	public function add()
	{
		$this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required|is_unique[barang_mentahs.nama_barang]');
		$this->form_validation->set_rules('harga', 'Harga', 'required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
		$this->form_validation->set_rules('stock', 'Stock', 'required');

		if ($this->form_validation->run() == true) {
			$this->barangmentah_model->insertBarangMentah();
			$this->session->set_flashdata('success', 'Berhasil menambah barang mentah!');
			redirect('barangmentah');
		} else {
			$this->session->set_flashdata('error', validation_errors());
			redirect('barangmentah/tambah');
		}
	}

	public function hapus($id)
	{
		$this->barangmentah_model->deleteBarangMentah($id);
		$this->session->set_flashdata('success', 'Berhasil menghapus barang mentah!');
		redirect('barangmentah');
	}

	public function edit($id)
	{
		$data['bmentah'] = $this->barangmentah_model->getDetailBarangMentah($id);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('barangmentah/edit', $data);
		$this->load->view('layouts/footer');
	}

	public function editProcess($id)
	{
		$data = $this->barangmentah_model->getDetailBarangMentah($id);
		if ($data['nama_barang'] == $this->input->post('nama_barang')) {
			$this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required');
			$this->form_validation->set_rules('harga', 'Harga', 'required');
			$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
			$this->form_validation->set_rules('stock', 'Stock', 'required');
		}
		else{
			$this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required|is_unique[barang_mentahs.nama_barang]');
			$this->form_validation->set_rules('harga', 'Harga', 'required');
			$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
			$this->form_validation->set_rules('stock', 'Stock', 'required');
		}

		if ($this->form_validation->run() == true) {
			$this->barangmentah_model->editBarangMentah($id);
			$this->session->set_flashdata('success', 'Data barang mentah berhasil diedit!');
			redirect('barangmentah');
		} else {
			$this->session->set_flashdata('error', validation_errors());
			redirect('barangmentah/edit/' . $id);
		}
	}

	public function log()
	{
		$data['bmentahlog'] = $this->barangmentah_model->getBarangMentahLog();
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('barangmentah/log', $data);
		$this->load->view('layouts/footer');
	}

	public function hapusLog($id)
	{
		$this->barangmentah_model->deleteBarangMentahLog($id);
		$this->session->set_flashdata('success', 'Berhasil menghapus log barang mentah!');
		redirect('barangmentah/log');
	}

	public function keluar()
	{
		$data['bmentah'] = $this->barangmentah_model->getBarangMentah();
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('barangmentah/keluar', $data);
		$this->load->view('layouts/footer');
	}

	public function keluarProcess()
	{
		$this->form_validation->set_rules('barang_mentah_id', 'Barang Mentah', 'required');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');

		if ($this->form_validation->run() == true) {
			$data = $this->barangmentah_model->getDetailBarangMentah($this->input->post('barang_mentah_id'));
			if($this->input->post('jumlah') <= $data['stock']){
				$this->barangmentah_model->insertBarangMentahKeluar($data['stock']);
				$this->session->set_flashdata('success', 'Berhasil menambah barang mentah keluar manual!');
				redirect('barangmentah/log');
			}
			else{
				$this->session->set_flashdata('error', 'Stok barang mentah tidak cukup atau tidak ada!');
				redirect('barangmentah/keluar');
			}
			
		} else {
			$this->session->set_flashdata('error', validation_errors());
			redirect('barangmentah/keluar');
		}
	}

	public function exportpdf(){
		$this->load->library('pdfgenerator');
		$data['bmentah'] = $this->barangmentah_model->getBarangMentah();
		$file_pdf = 'laporan_barang_mentah';
		$paper = 'A4';
		$orientation = "landscape";
		$html = $this->load->view('barangmentah/pdfindex', $data, true);
		$this->pdfgenerator->generate($html, $file_pdf, $paper, $orientation);
	}

	public function exportlogpdf()
	{
		$this->load->library('pdfgenerator');
		$data['bmentahlog'] = $this->barangmentah_model->getBarangMentahLog();
		$file_pdf = 'laporan_barang_mentah_log';
		$paper = 'A4';
		$orientation = "landscape";
		$html = $this->load->view('barangmentah/pdflogindex', $data, true);
		$this->pdfgenerator->generate($html, $file_pdf, $paper, $orientation);
	}
}

/* End of file Barangmentah.php and path \application\controllers\Barangmentah.php */
