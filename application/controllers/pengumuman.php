<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class pengumuman extends CI_Controller {

	public $pengumuman_model, $form_validation, $session, $input, $db, $auth_model,$role_model;

    public function __construct()
    {
        parent::__construct();
		$this->load->model('pengumuman_model');
		$this->load->model('auth_model');
		$this->auth_model->cek_login();
		$this->load->model('role_model');
		$this->role_model->cek_admin();
		
    }

    public function index()
    {
		$data['pengumumans'] = $this->pengumuman_model->getPengumuman();
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('pengumuman/index', $data);
		$this->load->view('layouts/footer');
    }

	public function tambah()
	{
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('pengumuman/tambah');
		$this->load->view('layouts/footer');
	}

	public function add()
	{
		$this->form_validation->set_rules('judul', 'Judul', 'required|min_length[2]|max_length[255]|is_unique[pengumumans.judul]');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'required|min_length[1]');

		if ($this->form_validation->run() == true) {
			$this->pengumuman_model->insertPengumuman();
			$this->session->set_flashdata('success', 'Berhasil menambah pengumuman!');
			redirect('pengumuman');
		} else {
			$this->session->set_flashdata('error', validation_errors());
			redirect('pengumuman/tambah');
		}
	}

	public function hapus($id)
	{
		$this->pengumuman_model->deletePengumuman($id);
		$this->session->set_flashdata('success', 'Berhasil menghapus pengumuman!');
		redirect('pengumuman');
	}

	public function edit($id)
	{
		$data['pengumuman'] = $this->pengumuman_model->getDetailPengumuman($id);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('pengumuman/edit', $data);
		$this->load->view('layouts/footer');
	}

	public function editProcess($id)
	{
		$data = $this->pengumuman_model->getDetailPengumuman($id);
		if($data['judul'] == $this->input->post('judul')){
			$this->form_validation->set_rules('judul', 'Judul', 'required|min_length[2]|max_length[255]');
			$this->form_validation->set_rules('keterangan', 'Keterangan', 'required|min_length[1]');
		}
		else{
			$this->form_validation->set_rules('judul', 'Judul', 'required|min_length[2]|max_length[255]|is_unique[pengumumans.judul]');
			$this->form_validation->set_rules('keterangan', 'Keterangan', 'required|min_length[1]');
		}

		if ($this->form_validation->run() == true) {
			$this->pengumuman_model->editPengumuman($id);
			$this->session->set_flashdata('success', 'Pengumuman berhasil diedit!');
			redirect('pengumuman');
		} else {
			$this->session->set_flashdata('error', validation_errors());
			redirect('pengumuman/edit/' . $id);
		}
	}
}

/* End of file Pengumuman.php and path \application\controllers\Pengumuman.php */
