<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class barangstok extends CI_Controller {

	public $barangstok_model, $db, $pdfgenerator, $barangmentah_model,$form_validation, $session, $input, $auth_model, $role_model;

    public function __construct()
    {
        parent::__construct();
		$this->load->model('barangstok_model');
		$this->load->model('barangmentah_model');
		$this->load->model('auth_model');
		$this->load->model('role_model');
		$this->auth_model->cek_login();
		$this->role_model->cek_admin();
    }

	public function index()
	{
		$data['bstok'] = $this->barangstok_model->getBarangStok();
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('barangstok/index', $data);
		$this->load->view('layouts/footer');
	}

	public function tambah()
	{
		$data['bmentah'] = $this->barangmentah_model->getBarangMentah2();
		$data['bmentah_json'] = json_encode($data['bmentah']);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('barangstok/tambah',$data);
		$this->load->view('layouts/footer');
	}

	public function add()
	{
		$this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required|is_unique[barang_mentahs.nama_barang]');
		$this->form_validation->set_rules('harga', 'Harga', 'required');
		$this->form_validation->set_rules('barang_mentah_id[]', 'Barang Mentah', 'required');
		$this->form_validation->set_rules('jumlah[]', 'Jumlah', 'required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
		$this->form_validation->set_rules('stock', 'Stock', 'required');

		if ($this->form_validation->run() == true) {
			$barang_mentah_ids = $this->input->post('barang_mentah_id');
			$jumlahs = $this->input->post('jumlah');

			$insertBarangStok = array(
				'nama_barang' => $this->input->post('nama_barang'),
				'harga' => $this->input->post('harga'),
				'keterangan' => $this->input->post('keterangan'),
				'stock' => $this->input->post('stock'),
			);
			$this->db->set('created_at', 'NOW()', FALSE);
			$this->db->set('updated_at', 'NOW()', FALSE);
			$this->db->insert('barang_stocks', $insertBarangStok);
			$barang_stock_id = $this->db->insert_id();

			$insertBarangStokLog = array(
				'barang_stock_id' => $barang_stock_id,
				'type' => 'masuk',
				'jumlah' => $this->input->post('stock'),
				'keterangan' => $this->input->post('keterangan'),
			);
			$this->db->set('created_at', 'NOW()', FALSE);
			$this->db->set('updated_at', 'NOW()', FALSE);
			$this->db->insert('barang_stock_logs', $insertBarangStokLog);

			$success = true;

			$initialStock = array(); // Array untuk menyimpan nilai stok awal

			// Simpan nilai stok awal sebelum memulai perulangan
			for ($i = 0; $i < count($barang_mentah_ids); $i++) {
				$barang_mentah_id = $barang_mentah_ids[$i];
				$jumlah = $jumlahs[$i];

				$data = $this->barangmentah_model->getDetailBarangMentah($barang_mentah_id);

				$initialStock[$barang_mentah_id] = $data['stock'];
			}

			for ($i = 0; $i < count($barang_mentah_ids); $i++) {
				$barang_mentah_id = $barang_mentah_ids[$i];
				$jumlah = $jumlahs[$i];

				$data = $this->barangmentah_model->getDetailBarangMentah($barang_mentah_id);
				
				if ($jumlah <= $data['stock']) {
					$insertBarangStokUse = array(
						'barang_stock_id' => $barang_stock_id,
						'barang_mentah_id' => $barang_mentah_id,
						'jumlah' => $jumlah,
					);
					$this->db->set('created_at', 'NOW()', FALSE);
					$this->db->set('updated_at', 'NOW()', FALSE);
					$this->db->insert('barang_stock_uses', $insertBarangStokUse);

					$hasil = $data['stock'] - $jumlah;
					$updateBarangMentah = array(
						'stock' => $hasil,
					);
					$this->db->where('id', $barang_mentah_id);
					$this->db->update('barang_mentahs', $updateBarangMentah);

					
				} else {
					$success = false;
					
					$this->session->set_flashdata('error', 'Stok barang mentah tidak cukup atau tidak ada!');
					break;
				}
				
			}

			if ($success) {
				for ($i = 0; $i < count($barang_mentah_ids); $i++) {
					$barang_mentah_id = $barang_mentah_ids[$i];
					$jumlah = $jumlahs[$i];
					$insertBarangMentahLog = array(
						'barang_mentah_id' => $barang_mentah_id,
						'type' => 'keluar',
						'jumlah' => $jumlah,
						'keterangan' => 'Dipakai untuk ' . $this->input->post('nama_barang'),
					);
					$this->db->set('created_at', 'NOW()', FALSE);
					$this->db->set('updated_at', 'NOW()', FALSE);
					$this->db->insert('barang_mentah_logs', $insertBarangMentahLog);
				}
				$this->session->set_flashdata('success', 'Berhasil menambah barang stok!');
				redirect('barangstok');
			} else {
				foreach ($initialStock as $barang_mentah_id => $initialStockValue) {
					$this->db->set('stock', $initialStockValue);
					$this->db->where('id', $barang_mentah_id);
					$this->db->update('barang_mentahs');
				}
				$this->db->where('barang_stock_id', $barang_stock_id);
				$this->db->delete('barang_stock_logs');

				$this->db->where('id', $barang_stock_id);
				$this->db->delete('barang_stocks');
				redirect('barangstok/tambah');
			}
			
		} else {
			$this->session->set_flashdata('error', validation_errors());
			redirect('barangstok/tambah');
		}
	}

	public function log()
	{
		$data['bstoklog'] = $this->barangstok_model->getBarangStokLog();
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('barangstok/log', $data);
		$this->load->view('layouts/footer');
	}

	public function hapusLog($id)
	{
		$this->barangstok_model->deleteBarangStokLog($id);
		$this->session->set_flashdata('success', 'Berhasil menghapus log barang stok!');
		redirect('barangstok/log');
	}

	public function keluar()
	{
		$data['bstok'] = $this->barangstok_model->getBarangStok();
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('barangstok/keluar', $data);
		$this->load->view('layouts/footer');
	}

	public function keluarProcess()
	{
		$this->form_validation->set_rules('barang_stock_id', 'Barang Stok', 'required');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');

		if ($this->form_validation->run() == true) {
			$data = $this->barangstok_model->getDetailBarangStok($this->input->post('barang_stock_id'));
			if ($this->input->post('jumlah') <= $data['stock']) {
				$this->barangstok_model->insertBarangStokKeluar($data['stock']);
				$this->session->set_flashdata('success', 'Berhasil menambah barang stok keluar manual!');
				redirect('barangstok/log');
			} else {
				$this->session->set_flashdata('error', 'Stok barang stok tidak cukup atau tidak ada!');
				redirect('barangstok/keluar');
			}
		} else {
			$this->session->set_flashdata('error', validation_errors());
			redirect('barangstok/keluar');
		}
	}

	public function hapus($id)
	{
		$this->barangstok_model->deleteBarangStok($id);
		$this->session->set_flashdata('success', 'Berhasil menghapus barang stok!');
		redirect('barangstok');
	}

	public function exportpdf()
	{
		$this->load->library('pdfgenerator');
		$data['bstok'] = $this->barangstok_model->getBarangStok();
		$file_pdf = 'laporan_barang_stok';
		$paper = 'A4';
		$orientation = "landscape";
		$html = $this->load->view('barangstok/pdfindex', $data, true);
		$this->pdfgenerator->generate($html, $file_pdf, $paper, $orientation);
	}

	public function exportlogpdf()
	{
		$this->load->library('pdfgenerator');
		$data['bstoklog'] = $this->barangstok_model->getBarangStokLog();
		$file_pdf = 'laporan_barang_stok_log';
		$paper = 'A4';
		$orientation = "landscape";
		$html = $this->load->view('barangstok/pdflogindex', $data, true);
		$this->pdfgenerator->generate($html, $file_pdf, $paper, $orientation);
	}
}

/* End of file Barangstok.php and path \application\controllers\Barangstok.php */
