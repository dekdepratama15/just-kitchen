<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class peringatan extends CI_Controller {

	public $peringatan_model, $form_validation, $session, $input, $db, $auth_model, $karyawan_model,$role_model;

    public function __construct()
    {
        parent::__construct();
		$this->load->model('peringatan_model');
		$this->load->model('auth_model');
		$this->load->model('karyawan_model');
		$this->auth_model->cek_login();
		$this->load->model('role_model');
		$this->role_model->cek_admin();
    }

	public function index()
	{
		$data['peringatans'] = $this->peringatan_model->getPeringatan();
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('peringatan/index', $data);
		$this->load->view('layouts/footer');
	}

	public function tambah()
	{
		$data['karyawans'] = $this->karyawan_model->getKaryawan();
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('peringatan/tambah',$data);
		$this->load->view('layouts/footer');
	}

	public function add()
	{
		$this->form_validation->set_rules('nomor', 'Nomor', 'required|min_length[1]|is_unique[peringatans.nomor]');
		$this->form_validation->set_rules('karyawan_id', 'Karyawan', 'required');
		$this->form_validation->set_rules('jenis', 'Jenis SP', 'required');
		$this->form_validation->set_rules('alasan', 'Alasan', 'required');

		if ($this->form_validation->run() == true) {
			$this->peringatan_model->insertPeringatan();
			$this->session->set_flashdata('success', 'Berhasil menambah peringatan!');
			redirect('peringatan');
		} else {
			$this->session->set_flashdata('error', validation_errors());
			redirect('peringatan/tambah');
		}
	}

	public function hapus($id)
	{
		$this->peringatan_model->deletePeringatan($id);
		$this->session->set_flashdata('success', 'Berhasil menghapus peringatan!');
		redirect('peringatan');
	}

	public function edit($id)
	{
		$data['peringatan'] = $this->peringatan_model->getDetailPeringatan($id);
		$data['karyawans'] = $this->karyawan_model->getKaryawan();
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('peringatan/edit', $data);
		$this->load->view('layouts/footer');
	}

	public function editProcess($id)
	{
		$data = $this->peringatan_model->getDetailPeringatan($id);
		if ($data['nomor'] == $this->input->post('nomor')) {
			$this->form_validation->set_rules('nomor', 'Nomor', 'required|min_length[1]');
			$this->form_validation->set_rules('karyawan_id', 'Karyawan', 'required');
			$this->form_validation->set_rules('jenis', 'Jenis SP', 'required');
			$this->form_validation->set_rules('alasan', 'Alasan', 'required');
		} else {
			$this->form_validation->set_rules('nomor', 'Nomor', 'required|min_length[1]|is_unique[peringatans.nomor]');
			$this->form_validation->set_rules('karyawan_id', 'Karyawan', 'required');
			$this->form_validation->set_rules('jenis', 'Jenis SP', 'required');
			$this->form_validation->set_rules('alasan', 'Alasan', 'required');
		}

		if ($this->form_validation->run() == true) {
			$this->peringatan_model->editPeringatan($id);
			$this->session->set_flashdata('success', 'Peringatan berhasil diedit!');
			redirect('peringatan');
		} else {
			$this->session->set_flashdata('error', validation_errors());
			redirect('peringatan/edit/' . $id);
		}
	}
}

/* End of file Peringatan.php and path \application\controllers\Peringatan.php */
