<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class pindahjadwal extends CI_Controller {

	public $pindahjadwal_model,$jadwal_model, $form_validation, $session, $input, $db, $auth_model, $karyawan_model,$role_model;

    public function __construct()
    {
        parent::__construct();
		$this->load->model('pindahjadwal_model');
		$this->load->model('jadwal_model');
		$this->load->model('karyawan_model');
		$this->load->model('auth_model');
		$this->auth_model->cek_login();
    }

    public function index()
    {
		if($this->session->userdata('role') == 'user'){
			$this->load->model('role_model');
			$this->role_model->cek_user();
			$user_id = $this->session->userdata('user_id');
			$karyawan = $this->karyawan_model->getDetailKaryawan($user_id);
			$karyawan_id = $karyawan['karyawan_id'];
			$data['jadwals'] = $this->jadwal_model->getJadwalUserBelum($karyawan_id);
			$this->load->view('layouts/header');
			$this->load->view('layouts/menu');
			$this->load->view('pindahjadwal/index', $data);
			$this->load->view('layouts/footer');
		}
		else{
			$this->load->model('role_model');
			$this->role_model->cek_admin();
			$data['pjadwals'] = $this->pindahjadwal_model->getPindahJadwalAll();
			$this->load->view('layouts/header');
			$this->load->view('layouts/menu');
			$this->load->view('pindahjadwal/listpindah', $data);
			$this->load->view('layouts/footer');
		}
    }

	public function pindah($id)
	{
		$this->load->model('role_model');
		$this->role_model->cek_user();
		$data['jadwal'] = $this->jadwal_model->getDetailJadwal($id);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('pindahjadwal/formpindah', $data);
		$this->load->view('layouts/footer');
	}

	public function pindahProcess($id)
	{
		$this->load->model('role_model');
		$this->role_model->cek_user();
		$this->form_validation->set_rules('tanggal_baru', 'Tanggal', 'required');
		$this->form_validation->set_rules('jadwal_keluar_baru', 'Jadwal Keluar', 'required');
		$this->form_validation->set_rules('jadwal_masuk_baru', 'Jadwal Masuk', 'required');
		$this->form_validation->set_rules('alasan', 'Alasan', 'required|min_length[1]');

		if ($this->form_validation->run() == true) {
			$this->pindahjadwal_model->pindahJadwal($id);
			$this->session->set_flashdata('success', 'Pindah jadwal berhasil diajukan!');
			redirect('pindahjadwal/listpindah');
		} else {
			$this->session->set_flashdata('error', validation_errors());
			redirect('pindahjadwal/pindah/' . $id);
		}
	}

	public function listpindah()
	{
		$this->load->model('role_model');
		$this->role_model->cek_user();
		$data['pjadwals'] = $this->pindahjadwal_model->getPindahJadwal();
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('pindahjadwal/listpindah', $data);
		$this->load->view('layouts/footer');
	}

	public function approve($id)
	{
		$this->load->model('role_model');
		$this->role_model->cek_admin();
		$data = $this->pindahjadwal_model->getDetailPindahJadwal($id);

		$editpj = array(
			'status' => 'approve',
		);
		$this->db->set('updated_at', 'NOW()', FALSE);
		$this->db->where('id', $id);
		$this->db->update('pindah_jadwals', $editpj);

		$editj = array(
			'tanggal' => $data['tanggal'],
			'jadwal_masuk' => $data['jadwal_masuk'],
			'jadwal_keluar' => $data['jadwal_keluar'],
			'status' => 'pindah'
		);
		$this->db->set('updated_at', 'NOW()', FALSE);
		$this->db->where('id', $data['jadwal_id']);
		$this->db->update('jadwals', $editj);

		$this->session->set_flashdata('success', 'Pindah jadwal berhasil diapprove!');
		redirect('pindahjadwal');
	}

	public function tolak($id)
	{
		$this->load->model('role_model');
		$this->role_model->cek_admin();

		$this->db->where('id', $id);
		$this->db->delete('pindah_jadwals');

		$this->session->set_flashdata('success', 'Pindah jadwal berhasil ditolak!');
		redirect('pindahjadwal');
	}

	public function edit($id)
	{
		$this->load->model('role_model');
		$this->role_model->cek_user();

		$data['jadwal'] = $this->pindahjadwal_model->getDetailPindahJadwal($id);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('pindahjadwal/edit', $data);
		$this->load->view('layouts/footer');
	}

	public function editProcess($id)
	{
		$this->load->model('role_model');
		$this->role_model->cek_user();

		$this->form_validation->set_rules('tanggal_baru', 'Tanggal', 'required');
		$this->form_validation->set_rules('jadwal_keluar_baru', 'Jadwal Keluar', 'required');
		$this->form_validation->set_rules('jadwal_masuk_baru', 'Jadwal Masuk', 'required');
		$this->form_validation->set_rules('alasan', 'Alasan', 'required|min_length[1]');

		if ($this->form_validation->run() == true) {
			$this->pindahjadwal_model->editPindahJadwal($id);
			$this->session->set_flashdata('success', 'Pindah jadwal berhasil diedit!');
			redirect('pindahjadwal/listpindah');
		} else {
			$this->session->set_flashdata('error', validation_errors());
			redirect('pindahjadwal/edit/' . $id);
		}
	}

	public function hapus($id)
	{
		$this->pindahjadwal_model->deletePindahJadwal($id);
		$this->session->set_flashdata('success', 'Berhasil menghapus pindah jadwal!');
		redirect('pindahjadwal/listpindah');
	}

}

/* End of file Pindah_jadwal.php and path \application\controllers\Pindah_jadwal.php */
