<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class pengajuancuti extends CI_Controller {

	public $pengajuancuti_model, $jadwal_model, $form_validation, $session, $input, $db, $auth_model, $karyawan_model, $role_model;

    public function __construct()
    {
        parent::__construct();
		$this->load->model('pengajuancuti_model');
		$this->load->model('jadwal_model');
		$this->load->model('karyawan_model');
		$this->load->model('auth_model');
		$this->auth_model->cek_login();
    }

    public function index()
    {
		if ($this->session->userdata('role') == 'user') {
			$this->load->model('role_model');
			$this->role_model->cek_user();
			$data['pcuti'] = $this->pengajuancuti_model->getPengajuanCuti();
			$this->load->view('layouts/header');
			$this->load->view('layouts/menu');
			$this->load->view('pengajuancuti/index', $data);
			$this->load->view('layouts/footer');
		} else {
			$this->load->model('role_model');
			$this->role_model->cek_admin();
			$data['pcuti'] = $this->pengajuancuti_model->getPengajuanCutiAll();
			$this->load->view('layouts/header');
			$this->load->view('layouts/menu');
			$this->load->view('pengajuancuti/index', $data);
			$this->load->view('layouts/footer');
		}
    }

	public function tambah()
	{
		$this->load->model('role_model');
		$this->role_model->cek_user();

		$user_id = $this->session->userdata('user_id');
		$karyawan = $this->karyawan_model->getDetailKaryawan($user_id);
		$karyawan_id = $karyawan['karyawan_id'];
		$data['jadwals'] = $this->jadwal_model->getJadwalUser($karyawan_id);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('pengajuancuti/tambah', $data);
		$this->load->view('layouts/footer');
	}

	public function add()
	{
		$this->load->model('role_model');
		$this->role_model->cek_user();

		$this->form_validation->set_rules('jadwal_id[]', 'Jadwal', 'required');
		$this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
		$this->form_validation->set_rules('jenis', 'Jenis', 'required');
		$this->form_validation->set_rules('alasan', 'Alasan', 'required');

		if ($this->form_validation->run() == true) {
			$this->pengajuancuti_model->insertPengajuan();
			$this->session->set_flashdata('success', 'Berhasil menambah pengajuan cuti!');
			redirect('pengajuancuti');
		} else {
			$this->session->set_flashdata('error', validation_errors());
			redirect('pengajuancuti/tambah');
		}
	}

	public function edit($id)
	{
		$this->load->model('role_model');
		$this->role_model->cek_user();

		$user_id = $this->session->userdata('user_id');
		$karyawan = $this->karyawan_model->getDetailKaryawan($user_id);
		$karyawan_id = $karyawan['karyawan_id'];
		$data['jadwals'] = $this->jadwal_model->getJadwalUser($karyawan_id);
		$data['pcuti'] = $this->pengajuancuti_model->getDetailPengajuanCuti($id);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('pengajuancuti/edit', $data);
		$this->load->view('layouts/footer');
	}

	public function editProcess($id)
	{
		$this->load->model('role_model');
		$this->role_model->cek_user();

		$this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
		$this->form_validation->set_rules('jenis', 'Jenis', 'required');
		$this->form_validation->set_rules('alasan', 'Alasan', 'required');

		if ($this->form_validation->run() == true) {
			$this->pengajuancuti_model->editPengajuan($id);
			$this->session->set_flashdata('success', 'Berhasil edit pengajuan cuti!');
			redirect('pengajuancuti');
		} else {
			$this->session->set_flashdata('error', validation_errors());
			redirect('pengajuancuti/edit/' . $id);
		}
	}

	public function hapus($id)
	{
		$this->pengajuancuti_model->deletePengajuan($id);
		$this->session->set_flashdata('success', 'Berhasil menghapus pengajuan cuti!');
		redirect('pengajuancuti');
	}

	public function approve($id)
	{
		$this->load->model('role_model');
		$this->role_model->cek_admin();
		$data = $this->pengajuancuti_model->getDetailPengajuanCuti($id);

		$editcj = array(
			'status' => 'approve',
		);
		$this->db->set('updated_at', 'NOW()', FALSE);
		$this->db->where('id', $id);
		$this->db->update('cuti_jadwals', $editcj);

		$editj = array(
			'status' => 'cuti',
		);
		$this->db->set('updated_at', 'NOW()', FALSE);
		$this->db->where('id', $data['jadwal_id']);
		$this->db->update('jadwals', $editj);

		$this->session->set_flashdata('success', 'Pengajuan cuti berhasil diapprove!');
		redirect('pengajuancuti');
	}

	public function tolak($id)
	{
		$this->load->model('role_model');
		$this->role_model->cek_admin();

		$this->db->where('id', $id);
		$this->db->delete('cuti_jadwals');

		$this->session->set_flashdata('success', 'Pengajuan cuti berhasil ditolak!');
		redirect('pengajuancuti');
	}
}

/* End of file Pengajuancuti.php and path \application\controllers\Pengajuancuti.php */
