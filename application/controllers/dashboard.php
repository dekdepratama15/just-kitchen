<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class dashboard extends CI_Controller {
	
	public $auth_model,$pengajuancuti_model,$barangstok_model,$role_model,$session,$karyawan_model,$jadwal_model,$db,$form_validation,$pengumuman_model,$barangmentah_model,$peringatan_model;

    public function __construct()
    {
        parent::__construct();
		$this->load->model('auth_model');
		$this->load->model('pengajuancuti_model');
		$this->load->model('karyawan_model');
		$this->load->model('pengumuman_model');
		$this->load->model('barangmentah_model');
		$this->load->model('barangstok_model');
		$this->load->model('peringatan_model');
		$this->load->model('jadwal_model');
		$this->load->model('role_model');
		$this->auth_model->cek_login();
		
    }

    public function index()
    {
		if ($this->session->userdata('role') == 'user') {
			$this->role_model->cek_user();
			$karyawan = $this->karyawan_model->getDetailKaryawan($this->session->userdata('user_id'));
			$karyawan_id = $karyawan['karyawan_id'];
			$karyawan = $this->karyawan_model->getDetailKaryawan($this->session->userdata('user_id'));
			$karyawan_id = $karyawan['karyawan_id'];
			$data['jadwal'] = $this->jadwal_model->getJadwalKaryawanNow($karyawan_id);
			$data['peringatans'] = $this->peringatan_model->getPeringatanKaryawan($karyawan_id);
			$this->load->view('layouts/header');
			$this->load->view('layouts/menu');
			$this->load->view('dashboard/index',$data);
			$this->load->view('layouts/footer');

		} else {
			$this->role_model->cek_admin();
			$data['pcuti'] = $this->pengajuancuti_model->getPengajuanCutiAll();
			$data['countkaryawan'] = $this->karyawan_model->getCountKaryawan();
			$data['countpengumuman'] = $this->pengumuman_model->getCountPengumuman();
			$data['countbmentah'] = $this->barangmentah_model->getCountBarangMentah();
			$data['countbstok'] = $this->barangstok_model->getCountBarangStok();
			$data['jadwals'] = $this->jadwal_model->getJadwalNow();
			$this->load->view('layouts/header');
			$this->load->view('layouts/menu');
			$this->load->view('dashboard/index', $data);
			$this->load->view('layouts/footer');
		}
		
    }

	public function absenMasuk($jadwal_id){
		$this->role_model->cek_user();
		date_default_timezone_set('Asia/Makassar');
		$timeNow = strtotime(date('H:i:s'));

		$jadwal = $this->jadwal_model->getDetailJadwal($jadwal_id);
		$jadwal_masuk = strtotime($jadwal['jadwal_masuk']);
		$jamjadwal = date('H', $jadwal_masuk);
		$menitjadwal = date('i', $jadwal_masuk);
		$jamNow = date('H', $timeNow);
		$menitNow = date('i', $timeNow);
		$selisihmenit = $menitjadwal - $menitNow;
		$selisihjam = $jamjadwal - $jamNow;
		if($selisihmenit >= 0 && $selisihjam >= 0){
			$edit = array(
				'waktu_masuk' => date('H:i:s'),
				'status' => 'hadir'
			);
			$this->db->set('updated_at','NOW()', FALSE);
			$this->db->where('id', $jadwal_id);
			$this->db->update('jadwals', $edit);
			$this->session->set_flashdata('success', 'Anda berhasil absen masuk dan tidak terlambat !');
			redirect('dashboard');
		}else{
			$edit = array(
				'waktu_masuk' => date('H:i:s'),
				'status' => 'hadir',
				'waktu_telat' => abs($selisihjam).' jam '.abs($selisihmenit).' menit'
			);
			$this->db->set('updated_at','NOW()', FALSE);
			$this->db->where('id', $jadwal_id);
			$this->db->update('jadwals', $edit);
			$this->session->set_flashdata('success', 'Anda berhasil absen masuk dan telat '.abs($selisihjam).' jam '.abs($selisihmenit).' menit');
			redirect('dashboard');
		}
	}

	public function absenKeluar($jadwal_id){
		$this->role_model->cek_user();
		date_default_timezone_set('Asia/Makassar');
		$timeNow = strtotime(date('H:i:s'));

		$jadwal = $this->jadwal_model->getDetailJadwal($jadwal_id);
		$jadwal_keluar = strtotime($jadwal['jadwal_keluar']);
		$menitjadwal = date('i', $jadwal_keluar);
		$menitNow = date('i', $timeNow);
		$selisihmenit = $menitjadwal - $menitNow;

		$jamjadwal = date('H', $jadwal_keluar);
		$jamNow = date('H', $timeNow);
		$selisihjam =  $jamjadwal - $jamNow;
		if ($selisihjam <= 0 && $selisihmenit <=0) {
			$edit = array(
				'waktu_keluar' => date('H:i:s'),
			);
			$this->db->set('updated_at', 'NOW()', FALSE);
			$this->db->where('id', $jadwal_id);
			$this->db->update('jadwals', $edit);
			$this->session->set_flashdata('success', 'Anda berhasil absen keluar!');
			redirect('dashboard');
		} else {
			$this->session->set_flashdata('error', 'Belum waktunya absen keluar!');
			redirect('dashboard');
		}
	}

	public function sakit($jadwal_id){
		$this->role_model->cek_user();
		$data['jadwal'] = $this->jadwal_model->getDetailJadwal($jadwal_id);
		$this->load->view('layouts/header');
		$this->load->view('layouts/menu');
		$this->load->view('dashboard/sakit', $data);
		$this->load->view('layouts/footer');
	}

	public function sakitProcess($jadwal_id){
		$this->role_model->cek_user();
		$karyawan = $this->karyawan_model->getDetailKaryawan($this->session->userdata('user_id'));
		$karyawan_id = $karyawan['karyawan_id'];
		if($_FILES['surat_sakit']['name']){
			$this->jadwal_model->uploadSuratSakit($jadwal_id, $karyawan_id);
			$this->session->set_flashdata('success', 'Berhasil upload surat sakit!');
			redirect('dashboard');
		}else{
			$this->session->set_flashdata('error', 'Gagal upload surat sakit!');
			redirect('dashboard');
		}
		
	}
}

/* End of file Dashboard.php and path \application\controllers\Dashboard.php */
