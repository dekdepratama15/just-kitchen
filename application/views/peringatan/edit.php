<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-xl-12 col-lg-12">
				<?php if ($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger solid">
						<?= $this->session->flashdata('error') ?>
					</div>
				<?php endif ?>
				<div class="card card-bx">
					<div class="card-header">
						<h4 class="title">Form Tambah Peringatan</h4>
					</div>
					<form method="POST" action="<?= base_url('peringatan/editProcess/') . $peringatan['peringatan_id'] ?>" enctype="multipart/form-data">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-12 mb-3">
									<label class="form-label">Karyawan</label>
									<select class="selectpicker nice-select default-select form-control wide mh-auto" name="karyawan_id" required>
										<option selected disabled>- Pilih Karyawan -</option>
										<?php foreach ($karyawans->result_array() as $karyawan) :
											$selected = ($karyawan['karyawan_id'] == $peringatan['karyawan_id']) ? 'selected' : '';
										?>
											<option value="<?= $karyawan['karyawan_id'] ?>" <?= $selected ?>><?= $karyawan['nama_panggilan'] ?> - <?= $karyawan['jabatan'] ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Nomor</label>
									<input type="text" class="form-control" value="<?= $peringatan['nomor'] ?>" name="nomor" placeholder="Masukkan Nomor" required>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Jenis SP</label>
									<select class="selectpicker nice-select default-select form-control wide mh-auto" name="jenis" required>
										<option disabled>- Pilih Jenis SP -</option>
										<?php
										$jenisSP = ['SP1', 'SP2', 'SP3'];
										foreach ($jenisSP as $jenis) :
											$selected = ($peringatan['jenis'] == $jenis) ? 'selected' : ''; ?>
											<option <?= $selected ?> value="<?= $jenis ?>"><?= $jenis ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Alasan</label>
									<textarea name="alasan" id="" cols="20" rows="5" class="form-control" required><?= $peringatan['alasan'] ?></textarea>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">File PDF</label>
									<input type="file" class="form-control" name="file">
								</div>
							</div>
						</div>
						<div class="card-footer">
							<button class="btn btn-primary" type="submit">Edit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!--**********************************
            Content body end
        ***********************************-->
