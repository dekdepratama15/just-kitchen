<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-xl-12 col-lg-12">
				<?php if ($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger solid">
						<?= $this->session->flashdata('error') ?>
					</div>
				<?php endif ?>
				<div class="card card-bx">
					<div class="card-header">
						<h4 class="title">Form Tambah Peringatan</h4>
					</div>
					<form method="POST" action="<?= base_url('peringatan/add') ?>" enctype="multipart/form-data">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-12 mb-3">
									<label class="form-label">Karyawan</label>
									<select class="selectpicker nice-select default-select form-control wide mh-auto" name="karyawan_id" required>
										<option selected disabled>- Pilih Karyawan -</option>
										<?php foreach ($karyawans->result_array() as $karyawan) : ?>
											<option value="<?= $karyawan['karyawan_id'] ?>"><?= $karyawan['nama_panggilan'] ?> - <?= $karyawan['jabatan'] ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Nomor</label>
									<input type="text" class="form-control" value="" name="nomor" placeholder="Masukkan Nomor" required>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Jenis SP</label>
									<select class="selectpicker nice-select default-select form-control wide mh-auto" name="jenis" required>
										<option selected disabled>- Pilih Jenis SP -</option>
										<?php
										$jenisSP = ['SP1', 'SP2', 'SP3'];
										foreach ($jenisSP as $jenis) : ?>
											<option value="<?= $jenis ?>"><?= $jenis ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Alasan</label>
									<textarea name="alasan" id="" cols="20" rows="5" class="form-control" required></textarea>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">File PDF</label>
									<input type="file" class="form-control" name="file" required>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<button class="btn btn-primary" type="submit">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!--**********************************
            Content body end
        ***********************************-->
