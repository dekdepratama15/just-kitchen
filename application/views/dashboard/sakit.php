<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-xl-12 col-lg-12">
				<?php if ($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger solid">
						<?= $this->session->flashdata('error') ?>
					</div>
				<?php endif ?>
				<div class="card card-bx">
					<div class="card-header">
						<h4 class="title">Form Surat Sakit</h4>
					</div>
					<form method="POST" action="<?= base_url('dashboard/sakitProcess/') . $jadwal['jadwal_id'] ?>" enctype="multipart/form-data">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-12 mb-3">
									<label class="form-label">Upload Surat Sakit</label>
									<input type="file" class="form-control" name="surat_sakit" required>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<button class="btn btn-primary" type="submit">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!--**********************************
            Content body end
        ***********************************-->
