		<!--**********************************
            Content body start
        ***********************************-->
		<div class="content-body">
			<!-- row -->
			<div class="container-fluid">
				<?php if ($this->session->userdata('role') == 'admin') : ?>
					<div class="row">
						<div class="col-xl-12">
							<div class="row">
								<div class="col-xl-12">
									<div class="card">
										<div class="card-body">
											<div class="row separate-row">
												<div class="col-sm-6">
													<div class="job-icon pb-4 d-flex justify-content-between">
														<div>
															<div class="d-flex align-items-center mb-1">
																<h2 class="mb-0 lh-1"><?= $countkaryawan ?></h2>
															</div>
															<span class="fs-14 d-block mb-2">Jumlah Karyawan</span>
														</div>
													</div>
												</div>
												<div class="col-sm-6">
													<div class="job-icon pb-4 pt-4 pt-sm-0 d-flex justify-content-between">
														<div>
															<div class="d-flex align-items-center mb-1">
																<h2 class="mb-0 lh-1"><?= $countpengumuman ?></h2>
															</div>
															<span class="fs-14 d-block mb-2">Jumlah Pengumuman</span>
														</div>
													</div>
												</div>
												<div class="col-sm-6">
													<div class="job-icon pt-4 pb-sm-0 pb-4 pe-3 d-flex justify-content-between">
														<div>
															<div class="d-flex align-items-center mb-1">
																<h2 class="mb-0 lh-1"><?= $countbmentah ?></h2>
															</div>
															<span class="fs-14 d-block mb-2">Jumlah Barang Mentah</span>
														</div>
													</div>
												</div>
												<div class="col-sm-6">
													<div class="job-icon pt-4  d-flex justify-content-between">
														<div>
															<div class="d-flex align-items-center mb-1">
																<h2 class="mb-0 lh-1"><?= $countbstok ?></h2>
															</div>
															<span class="fs-14 d-block mb-2">Jumlah Barang Stok</span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-12">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title">Daftar Pengajuan Cuti Terbaru</h4>
								</div>
								<div class="card-body">
									<div class="table-responsive">
										<table id="example" class="display" style="min-width: 845px">
											<thead>
												<tr>
													<th>No</th>
													<th>Karyawan</th>
													<th>Jadwal</th>
													<th>Tanggal</th>
													<th>Status</th>
													<th>Jenis</th>
													<th>Alasan</th>
													<th>Tanggal Diajukan</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php
												$no = 1;
												foreach ($pcuti->result_array() as $cuti) : ?>
													<tr>
														<td><?= $no++ ?></td>
														<td><?= ucfirst($cuti['nama_panggilan']) ?></td>
														<td><?= date('d M Y', strtotime($cuti['tanggal_jadwal'])) ?></td>
														<td><?= date('d M Y', strtotime($cuti['tanggal'])) ?></td>
														<?php
														if ($cuti['status_cuti'] == 'approve') : ?>
															<td><span class="badge badge-success"><?= $cuti['status_cuti'] ?></span></td>
														<?php endif ?>

														<?php
														if ($cuti['status_cuti'] == 'pending') : ?>
															<td><span class="badge badge-danger"><?= $cuti['status_cuti'] ?></span></td>
														<?php endif ?>
														<td><?= ucfirst($cuti['jenis']) ?></td>
														<td><?= ucfirst($cuti['alasan']) ?></td>
														<td><?= date('d M Y H:i:s', strtotime($cuti['cuti_ca'])) ?></td>

														<?php
														if ($cuti['status_cuti'] == 'pending') : ?>
															<td>
																<div class="dropdown">
																	<button id="toa" class="btn btn-sm btn-primary" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
																		<span class="bi bi-three-dots-vertical"></span>
																	</button>
																	<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
																		<a href="<?= base_url('pengajuancuti/approve/' . $cuti['cuti_jadwal_id']) ?>" class="dropdown-item" onclick="return confirm('Yakin approve pengajuan cuti?')"><i class="fa fa-check" id="from1"></i> Approve</a>
																		<a href="<?= base_url('pengajuancuti/tolak/' . $cuti['cuti_jadwal_id']) ?>" class="dropdown-item" onclick="return confirm('Yakin tolak pengajuan cuti?')"><i class="fa fa-times" id="from1"></i> Tolak</a>
																	</div>
																</div>
															</td>
														<?php else : ?>
															<td>-</td>
														<?php endif ?>


													</tr>
												<?php endforeach ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-12">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title">Daftar Karyawan Yang Tidak Hadir Hari Ini <?= date('d M Y') ?></h4>
								</div>
								<div class="card-body">
									<div class="table-responsive">
										<table id="examples" class="display" style="min-width: 845px">
											<thead>
												<tr>
													<th>No</th>
													<th>Karyawan</th>
													<th>Jabatan</th>
													<th>Status</th>
													<th>Surat Sakit</th>
												</tr>
											</thead>
											<tbody>
												<?php
												$no = 1;
												foreach ($jadwals->result_array() as $jadwal) : ?>
													<tr>
														<td><?= $no++ ?></td>
														<td><?= $jadwal['nama_panggilan'] ?></td>
														<td><?= $jadwal['jabatan'] ?></td>
														<?php if ($jadwal['jadwal_status'] == 'hadir') : ?>
															<td><span class="badge badge-success"><?= $jadwal['jadwal_status'] ?></span></td>
														<?php else : ?>
															<td><span class="badge badge-danger"><?= $jadwal['jadwal_status'] ?></span></td>
														<?php endif ?>

														<?php if ($jadwal['surat_sakit'] != NULL) : ?>
															<td><a href="<?= base_url('assets/suratsakit/' . $jadwal['surat_sakit']) ?>" target="_BLANK"><button class="btn btn-sm btn-primary">Lihat Surat Sakit</button></a></td>
														<?php else : ?>
															<td>-</td>
														<?php endif ?>
													</tr>
												<?php endforeach ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endif ?>

				<?php if ($this->session->userdata('role') == 'user') : ?>
					<div class="row">
						<div class="col-lg-12">
							<?php if ($this->session->flashdata('success')) : ?>
								<div class="alert alert-success solid">
									<?= $this->session->flashdata('success') ?>
								</div>
							<?php endif ?>

							<?php if ($this->session->flashdata('error')) : ?>
								<div class="alert alert-danger solid">
									<?= $this->session->flashdata('error') ?>
								</div>
							<?php endif ?>
						</div>
						<?php if ($peringatans != '') : ?>
							<?php foreach ($peringatans->result_array() as $peringatan) : ?>
								<div class="col-lg-12">
									<div class="alert alert-danger solid text-white">
										PERINGATAN ! <br>
										Nomor : <?= $peringatan['nomor'] ?><br>
										Jenis : <?= $peringatan['jenis'] ?><br><br>
										<?= $peringatan['alasan'] ?> <br><br>
										<a href="<?= base_url('assets/peringatan/' . $peringatan['file']) ?>" target="_BLANK"><button class="btn btn-xs btn-secondary">Download File</button></a>
										<small class="d-block mt-1"><?= $peringatan['username'] ?> - <?= date('d M Y H:i:s', strtotime($peringatan['peringatan_ca'])) ?></small>
									</div>
								</div>
							<?php endforeach ?>

						<?php endif ?>
						<div class="col-xl-12">
							<div class="row">
								<div class="col-xl-12">
									<div class="card">
										<div class="card-body my-4">
											<?php if ($jadwal == '-') : ?>
												<h3>Jadwal Hari Ini : <?= $jadwal ?></h3>
											<?php else : ?>
												<h3 class="mb-5">Jadwal Hari Ini, <?= date('d M Y', strtotime($jadwal['tanggal'])) ?></h3>
												<h5>Jadwal Masuk : <?= $jadwal['jadwal_masuk'] ?> WITA</h5>
												<h5>Jadwal Keluar : <?= $jadwal['jadwal_keluar'] ?> WITA</h5>

												<?php if ($jadwal['jadwal_status'] == 'hadir') : ?>
													<?php if ($jadwal['waktu_telat'] != NULL) : ?>
														<h5>Terlambat : <span class="badge badge-danger"><?= $jadwal['waktu_telat'] ?></span></h5>
													<?php else : ?>
														<h5>Terlambat : -</h5>
													<?php endif ?>
													<h5>Status : <span class="badge badge-success"><?= $jadwal['jadwal_status'] ?></span></h5>
													<h5 class="mb-5">Absen Keluar :
														<?php if ($jadwal['waktu_keluar'] == NULL) : ?>
															<span class="badge badge-danger">Belum</span>
														<?php else : ?>
															<span class="badge badge-success">Sudah</span>
														<?php endif ?>
													</h5>
												<?php else : ?>
													<h5 class="mb-5">Status : <span class="badge badge-danger"><?= $jadwal['jadwal_status'] ?></span></h5>
												<?php endif ?>

												<?php if ($jadwal['jadwal_status'] == 'belum' || $jadwal['jadwal_status'] == 'pindah') : ?>
													<a href="<?= base_url('dashboard/absenMasuk/' . $jadwal['jadwal_id']) ?>" onclick="return confirm('Yakin Absen Masuk Sekarang?')"><button class="btn btn-danger">Absen Masuk</button></a>
													<a href="<?= base_url('dashboard/sakit/' . $jadwal['jadwal_id']) ?>"><button class="btn btn-secondary">Sakit</button></a>

												<?php elseif ($jadwal['jadwal_status'] == 'hadir' && $jadwal['waktu_keluar'] == NULL) : ?>
													<a href="<?= base_url('dashboard/absenKeluar/' . $jadwal['jadwal_id']) ?>" onclick="return confirm('Yakin Absen Keluar Sekarang?')"><button class="btn btn-success">Absen Keluar</button></a>

												<?php elseif ($jadwal['jadwal_status'] == 'sakit') : ?>
													<a href="<?= base_url('assets/suratsakit/' . $jadwal['surat_sakit']) ?>" target="_BLANK"><button class="btn btn-primary">Lihat Surat Sakit</button></a>
												<?php endif ?>

											<?php endif ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endif ?>

			</div>
		</div>
		<!--**********************************
            Content body end
        ***********************************-->
