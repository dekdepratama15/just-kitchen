<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->


		<div class="row">
			<div class="col-lg-12">
				<?php if ($this->session->flashdata('success')) : ?>
					<div class="alert alert-success solid">
						<?= $this->session->flashdata('success') ?>
					</div>
				<?php endif ?>

				<?php if ($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger solid">
						<?= $this->session->flashdata('error') ?>
					</div>
				<?php endif ?>
			</div>
			<div class="col-12">
				<a href="<?= base_url('barangstok/tambah') ?>"><button class="btn btn-primary my-3">Tambah Barang Stok</button></a>
				<a href="<?= base_url('barangstok/exportpdf') ?>" target="_BLANK"><button class="btn btn-secondary my-3">Export PDF</button></a>
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Daftar Barang Stok</h4>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="example" class="display" style="min-width: 845px">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama Barang</th>
										<th>Harga</th>
										<th>Keterangan</th>
										<th>Stok</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$no = 1;
									foreach ($bstok->result_array() as $bstok) : ?>
										<tr>
											<td><?= $no++ ?></td>
											<td><?= ucfirst($bstok['nama_barang']) ?></td>
											<td>Rp. <?= number_format($bstok['harga'], 0, ",", ".") ?></td>
											<td><?= ucfirst(substr($bstok['keterangan'], 0, 30)) ?>...</td>
											<td><?= $bstok['stock'] ?></td>
											<td>
												<div class="dropdown">
													<button id="toa" class="btn btn-sm btn-primary" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
														<span class="bi bi-three-dots-vertical"></span>
													</button>
													<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
														<a href="<?= base_url('barangstok/hapus/' . $bstok['id']) ?>" class="dropdown-item" onclick="return confirm('Yakin hapus barang stok ?')"><i class="fa fa-trash"></i> Hapus</a>
													</div>
												</div>
											</td>
										</tr>
									<?php endforeach ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--**********************************
            Content body end
        ***********************************-->
