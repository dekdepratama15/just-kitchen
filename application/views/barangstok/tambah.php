<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-xl-12 col-lg-12">
				<?php if ($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger solid">
						<?= $this->session->flashdata('error') ?>
					</div>
				<?php endif ?>
				<div class="card card-bx">
					<div class="card-header">
						<h4 class="title">Form Tambah Barang Stok</h4>
					</div>
					<form method="POST" action="<?= base_url('barangstok/add') ?>">
						<div class="card-body" id="dynamicForm">
							<div class="row">
								<div class="col-sm-12 mb-3">
									<label class="form-label">Nama Barang</label>
									<input type="text" class="form-control" value="" name="nama_barang" placeholder="Masukkan Nama Barang" required>
								</div>
								<div class="col-sm-12 mb-3 d-flex justify-content-end">
									<button type="button" onclick="addRow()" class="btn btn-sm btn-primary">Tambah Barang Mentah</button>
								</div>
								<div class="col-sm-8 mb-3">
									<label class="form-label">Barang Mentah</label>
									<select class="form-control" name="barang_mentah_id[]" required>
										<option selected disabled>- Pilih Barang Mentah -</option>
										<?php foreach ($bmentah as $bmentah) : ?>
											<option value="<?= $bmentah['id'] ?>"><?= $bmentah['nama_barang'] ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-4 mb-3">
									<label class="form-label">Jumlah</label>
									<input type="number" class="form-control" name="jumlah[]" placeholder="Masukkan Jumlah" required>
								</div>

								<div id="newRow"></div>

								<div class="col-sm-12 mb-3">
									<label class="form-label">Harga</label>
									<input type="number" class="form-control" name="harga" placeholder="Masukkan Harga" required>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Keterangan</label>
									<textarea name="keterangan" id="" cols="20" rows="5" class="form-control" required></textarea>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Stok</label>
									<input type="number" class="form-control" name="stock" placeholder="Masukkan Stok" required>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<button class="btn btn-primary" type="submit">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!--**********************************
            Content body end
        ***********************************-->

<script>
	var dataBarangMentah = <?= $bmentah_json ?>;

	function addRow() {
		var newRowContainer = document.getElementById("newRow");

		var newRowHtml = `
        <div class="row mb-3">
            <div class="col-sm-8">
                <select class="form-control" name="barang_mentah_id[]" required>
                    <option selected disabled>- Pilih Barang Mentah -</option>
                    ${dataBarangMentah.map(item => `<option value="${item.id}">${item.nama_barang}</option>`).join('')}
                </select>
            </div>
            <div class="col-sm-3">
                <input type="number" class="form-control" name="jumlah[]" placeholder="Masukkan Jumlah" required>
            </div>
            <div class="col-sm-1">
                <button type="button" class="btn btn-sm mt-1 btn-danger" onclick="removeRow(this)">Hapus</button>
            </div>
        </div>
    `;

		newRowContainer.innerHTML += newRowHtml;
	}

	function removeRow(button) {
		var rowToRemove = button.closest('.row');
		rowToRemove.parentNode.removeChild(rowToRemove);
	}
</script>
