<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-xl-12 col-lg-12">
				<?php if ($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger solid">
						<?= $this->session->flashdata('error') ?>
					</div>
				<?php endif ?>
				<div class="card card-bx">
					<div class="card-header">
						<h4 class="title">Form Tambah Barang Stok Keluar Manual</h4>
					</div>
					<form method="POST" action="<?= base_url('barangstok/keluarProcess') ?>">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-12 mb-3">
									<label class="form-label">Barang Stok</label>
									<select class="selectpicker nice-select default-select form-control wide mh-auto" name="barang_stock_id" required>
										<option selected disabled>- Pilih Barang Mentah -</option>
										<?php foreach ($bstok->result_array() as $bstok) : ?>
											<option value="<?= $bstok['id'] ?>"><?= ucfirst($bstok['nama_barang']) ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Jumlah</label>
									<input type="number" class="form-control" name="jumlah" placeholder="Masukkan Jumlah" required>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Keterangan</label>
									<textarea name="keterangan" id="" cols="20" rows="5" class="form-control" required></textarea>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<button class="btn btn-primary" type="submit">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!--**********************************
            Content body end
        ***********************************-->
