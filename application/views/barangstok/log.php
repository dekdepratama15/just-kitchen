<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->


		<div class="row">
			<div class="col-lg-12">
				<?php if ($this->session->flashdata('success')) : ?>
					<div class="alert alert-success solid">
						<?= $this->session->flashdata('success') ?>
					</div>
				<?php endif ?>

				<?php if ($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger solid">
						<?= $this->session->flashdata('error') ?>
					</div>
				<?php endif ?>
			</div>
			<div class="col-12">
				<a href="<?= base_url('barangstok/exportlogpdf') ?>" target="_BLANK"><button class="btn btn-secondary my-3">Export PDF</button></a>
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Daftar Barang Stok Log</h4>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="example" class="display" style="min-width: 845px">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama Barang</th>
										<th>Type</th>
										<th>Jumlah</th>
										<th>Keterangan</th>
										<th>Ditambahkan</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$no = 1;
									foreach ($bstoklog->result_array() as $bstoklog) : ?>
										<tr>
											<td><?= $no++ ?></td>
											<td><?= ucfirst($bstoklog['nama_barang']) ?></td>
											<?php
											if ($bstoklog['type'] == 'masuk') : ?>
												<td><span class="badge badge-success"><?= $bstoklog['type'] ?></span></td>
											<?php endif ?>

											<?php
											if ($bstoklog['type'] == 'keluar') : ?>
												<td><span class="badge badge-danger"><?= $bstoklog['type'] ?></span></td>
											<?php endif ?>
											<td><?= $bstoklog['jumlah'] ?></td>
											<td><?= ucfirst($bstoklog['bmlogketerangan']) ?></td>
											<td><?= date('d M Y H:i:s', strtotime($bstoklog['ca_bstocklog'])) ?></td>
											<td>
												<div class="dropdown">
													<button id="toa" class="btn btn-sm btn-primary" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
														<span class="bi bi-three-dots-vertical"></span>
													</button>
													<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
														<a href="<?= base_url('barangstok/hapusLog/' . $bstoklog['barang_stock_logs_id']) ?>" class="dropdown-item" onclick="return confirm('Yakin hapus log barang stok ?')"><i class="fa fa-trash"></i> Hapus</a>
													</div>
												</div>
											</td>
										</tr>
									<?php endforeach ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--**********************************
            Content body end
        ***********************************-->
