<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-xl-12 col-lg-12">
				<?php if ($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger solid">
						<?= $this->session->flashdata('error') ?>
					</div>
				<?php endif ?>
				<div class="card card-bx">
					<div class="card-header">
						<h4 class="title">Form Pengajuan Cuti</h4>
					</div>
					<form method="POST" action="<?= base_url('pengajuancuti/editProcess/') . $pcuti['cuti_jadwal_id'] ?>" enctype="multipart/form-data">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-12 mb-3">
									<label class="form-label">Jadwal</label>
									<input type="text" class="form-control" name="jadwal_id" id="" value="<?= date('d M Y', strtotime($pcuti['tanggal_jadwal'])) ?>" readonly>
								</div>

								<div class="col-sm-6 mb-3">
									<label class="form-label">Tanggal</label>
									<input type="date" class="form-control" value="<?= $pcuti['tanggal'] ?>" name="tanggal" required>
								</div>
								<div class="col-sm-6 mb-3">
									<label class="form-label">Jenis</label>
									<input type="text" class="form-control" value="<?= $pcuti['jenis'] ?>" name="jenis" placeholder="Masukkan Jenis" required>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Alasan Pengajuan Cuti</label>
									<textarea name="alasan" id="" cols="30" rows="10" class="form-control" required><?= $pcuti['alasan'] ?></textarea>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<button class="btn btn-primary" type="submit">Edit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!--**********************************
            Content body end
        ***********************************-->
