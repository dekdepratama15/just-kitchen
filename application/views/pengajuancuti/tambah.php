<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-xl-12 col-lg-12">
				<?php if ($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger solid">
						<?= $this->session->flashdata('error') ?>
					</div>
				<?php endif ?>
				<div class="card card-bx">
					<div class="card-header">
						<h4 class="title">Form Pengajuan Cuti</h4>
					</div>
					<form method="POST" action="<?= base_url('pengajuancuti/add') ?>" enctype="multipart/form-data">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-12 mb-3">
									<label class="form-label">Pilih Jadwal</label>
									<select class="selectpicker nice-select default-select form-control wide mh-auto" name="jadwal_id[]" multiple required>
										<?php foreach ($jadwals->result_array() as $jadwal) : ?>
											<option value="<?= $jadwal['jadwal_id'] ?>"><?= date('d M Y', strtotime($jadwal['tanggal'])) ?></option>
										<?php endforeach ?>
									</select>
								</div>

								<div class="col-sm-6 mb-3">
									<label class="form-label">Tanggal</label>
									<input type="date" class="form-control" name="tanggal" required>
								</div>
								<div class="col-sm-6 mb-3">
									<label class="form-label">Jenis</label>
									<input type="text" class="form-control" name="jenis" placeholder="Masukkan Jenis" required>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Alasan Pengajuan Cuti</label>
									<textarea name="alasan" id="" cols="30" rows="10" class="form-control" required></textarea>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Upload Surat Sakit (kosongkan jika tidak perlu)</label>
									<input type="file" class="form-control" name="surat_sakit">
								</div>
							</div>
						</div>
						<div class="card-footer">
							<button class="btn btn-primary" type="submit">Ajukan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!--**********************************
            Content body end
        ***********************************-->
