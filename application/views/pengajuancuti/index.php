<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->


		<div class="row">
			<div class="col-lg-12">
				<?php if ($this->session->flashdata('success')) : ?>
					<div class="alert alert-success solid">
						<?= $this->session->flashdata('success') ?>
					</div>
				<?php endif ?>

				<?php if ($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger solid">
						<?= $this->session->flashdata('error') ?>
					</div>
				<?php endif ?>
			</div>
			<div class="col-12">
				<?php if ($this->session->userdata('role') == 'admin') : ?>
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Daftar Pengajuan Izin dan Cuti</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table id="example" class="display" style="min-width: 845px">
									<thead>
										<tr>
											<th>No</th>
											<th>Karyawan</th>
											<th>Jadwal</th>
											<th>Tanggal</th>
											<th>Status</th>
											<th>Jenis</th>
											<th>Alasan</th>
											<th>Tanggal Diajukan</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$no = 1;
										foreach ($pcuti->result_array() as $cuti) : ?>
											<tr>
												<td><?= $no++ ?></td>
												<td><?= ucfirst($cuti['nama_panggilan']) ?></td>
												<td><?= date('d M Y', strtotime($cuti['tanggal_jadwal'])) ?></td>
												<td><?= date('d M Y', strtotime($cuti['tanggal'])) ?></td>
												<?php
												if ($cuti['status_cuti'] == 'approve') : ?>
													<td><span class="badge badge-success"><?= $cuti['status_cuti'] ?></span></td>
												<?php endif ?>

												<?php
												if ($cuti['status_cuti'] == 'pending') : ?>
													<td><span class="badge badge-danger"><?= $cuti['status_cuti'] ?></span></td>
												<?php endif ?>
												<td><?= ucfirst($cuti['jenis']) ?></td>
												<td><?= ucfirst($cuti['alasan']) ?></td>
												<td><?= date('d M Y H:i:s', strtotime($cuti['cuti_ca'])) ?></td>

												<?php
												if ($cuti['status_cuti'] == 'pending') : ?>
													<td>
														<div class="dropdown">
															<button id="toa" class="btn btn-sm btn-primary" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
																<span class="bi bi-three-dots-vertical"></span>
															</button>
															<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
																<a href="<?= base_url('pengajuancuti/approve/' . $cuti['cuti_jadwal_id']) ?>" class="dropdown-item" onclick="return confirm('Yakin approve pengajuan cuti?')"><i class="fa fa-check" id="from1"></i> Approve</a>
																<a href="<?= base_url('pengajuancuti/tolak/' . $cuti['cuti_jadwal_id']) ?>" class="dropdown-item" onclick="return confirm('Yakin tolak pengajuan cuti?')"><i class="fa fa-times" id="from1"></i> Tolak</a>
															</div>
														</div>
													</td>
												<?php else : ?>
													<td>-</td>
												<?php endif ?>


											</tr>
										<?php endforeach ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				<?php endif ?>

				<?php if ($this->session->userdata('role') == 'user') : ?>
					<a href="<?= base_url('pengajuancuti/tambah') ?>"><button class="btn btn-primary my-3">Tambah Pengajuan Izin dan Cuti</button></a>
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Daftar Pengajuan Izin dan Cuti Anda</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table id="example" class="display" style="min-width: 845px">
									<thead>
										<tr>
											<th>No</th>
											<th>Jadwal</th>
											<th>Tanggal</th>
											<th>Status</th>
											<th>Jenis</th>
											<th>Alasan</th>
											<th>Tanggal Diajukan</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$no = 1;
										foreach ($pcuti->result_array() as $cuti) : ?>
											<tr>
												<td><?= $no++ ?></td>
												<td><?= date('d M Y', strtotime($cuti['tanggal_jadwal'])) ?></td>
												<td><?= date('d M Y', strtotime($cuti['tanggal'])) ?></td>
												<?php
												if ($cuti['status_cuti'] == 'approve') : ?>
													<td><span class="badge badge-success"><?= $cuti['status_cuti'] ?></span></td>
												<?php endif ?>

												<?php
												if ($cuti['status_cuti'] == 'pending') : ?>
													<td><span class="badge badge-danger"><?= $cuti['status_cuti'] ?></span></td>
												<?php endif ?>
												<td><?= ucfirst($cuti['jenis']) ?></td>
												<td><?= ucfirst($cuti['alasan']) ?></td>
												<td><?= date('d M Y H:i:s', strtotime($cuti['cuti_ca'])) ?></td>

												<?php
												if ($cuti['status_cuti'] == 'pending') : ?>
													<td>
														<div class="dropdown">
															<button id="toa" class="btn btn-sm btn-primary" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
																<span class="bi bi-three-dots-vertical"></span>
															</button>
															<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
																<a href="<?= base_url('pengajuancuti/edit/' . $cuti['cuti_jadwal_id']) ?>" class="dropdown-item"><i class="fa fa-edit" id="from1"></i> Edit</a>
																<a href="<?= base_url('pengajuancuti/hapus/' . $cuti['cuti_jadwal_id']) ?>" class="dropdown-item" onclick="return confirm('Yakin hapus pengajuan cuti?')"><i class="fa fa-trash" id="from1"></i> Hapus</a>
															</div>
														</div>
													</td>
												<?php else : ?>
													<td>
														<div class="dropdown">
															<button id="toa" class="btn btn-sm btn-primary" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
																<span class="bi bi-three-dots-vertical"></span>
															</button>
															<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
																<a href="<?= base_url('pengajuancuti/hapus/' . $cuti['cuti_jadwal_id']) ?>" class="dropdown-item" onclick="return confirm('Yakin hapus pengajuan cuti?')"><i class="fa fa-trash" id="from1"></i> Hapus</a>
															</div>
														</div>
													</td>
												<?php endif ?>

											</tr>
										<?php endforeach ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				<?php endif ?>

			</div>
		</div>
	</div>
</div>
<!--**********************************
            Content body end
        ***********************************-->
