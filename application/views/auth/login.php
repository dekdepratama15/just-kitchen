<!DOCTYPE html>
<html lang="en" class="h-100">

<head>
   <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="DexignLab">
	<meta name="robots" content="" >
	<meta name="keywords" content="admin dashboard, admin template, analytics, bootstrap, bootstrap 5, bootstrap 5 admin template, job board admin, job portal admin, modern, responsive admin dashboard, sales dashboard, sass, ui kit, web app, frontend">
	<meta name="description" content="We proudly present Jobick, a Job Admin dashboard HTML Template, If you are hiring a job expert you would like to build a superb website for your Jobick, it's a best choice.">
	<meta property="og:title" content="Jobick : Job Admin Dashboard Bootstrap 5 Template + FrontEnd">
	<meta property="og:description" content="We proudly present Jobick, a Job Admin dashboard HTML Template, If you are hiring a job expert you would like to build a superb website for your Jobick, it's a best choice." >
	<meta property="og:image" content="https://jobick.dexignlab.com/xhtml/social-image.png">
	<meta name="format-detection" content="telephone=no">
	
	<!-- Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- PAGE TITLE HERE -->
	<title>Just Kitchen - Login</title>
	
	<!-- Favicon icon -->
	<link rel="shortcut icon" type="image/png" href="<?= base_url('assets/images/favicon.png') ?>">
<link href="<?= base_url('assets/vendor/bootstrap-select/dist/css/bootstrap-select.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet">

	<style>
		.bg-login{
			background: url('assets/images/login.jpg');
			background-position: center;
			background-repeat: no-repeat;
			background-size: cover;
		}
	</style>

</head>

<body class="vh-100">
    <div class="bg-login h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-md-6">
                    <div class="authincation-content">
                        <div class="row no-gutters">
                            <div class="col-xl-12">
                                <div class="auth-form">
									<div class="text-center mb-3">
										<a href="<?= base_url('auth') ?>" class="brand-logo">
											<h2 class="h3">Just Kitchen</h2>

										</a>
									</div>
                                    <h5 class="text-center mb-4">Sign in your account</h5>
									<?php if ($this->session->flashdata('error')) : ?>
									<div class="alert alert-danger solid">
										<?= $this->session->flashdata('error') ?>
									</div>
									<?php endif ?>

									<?php if ($this->session->flashdata('success')) : ?>
									<div class="alert alert-success solid">
										<?= $this->session->flashdata('success') ?>
									</div>
									<?php endif ?>
                                    <form action="<?= base_url('auth/process') ?>" method="POST">
                                        <div class="mb-3">
                                            <label class="mb-1" for="username"><strong>Username</strong></label>
                                            <input id="username" type="text" class="form-control" name="username" placeholder="Masukkan Username" required>
                                        </div>
                                        <div class="mb-3">
                                            <label class="mb-1" for="password"><strong>Password</strong></label>
                                            <input id="password" type="password" class="form-control" name="password" placeholder="Masukkan Password" required>
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary btn-block">Login</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
	
    <script src="<?= base_url('assets/vendor/global/global.min.js') ?>"></script>
	<script src="<?= base_url('assets/vendor/bootstrap-select/dist/js/bootstrap-select.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/custom.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/dlabnav-init.js') ?>"></script>
	
</body>
</html>
