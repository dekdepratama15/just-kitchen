<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->


		<div class="row">
			<div class="col-lg-12">
				<?php if ($this->session->flashdata('success')) : ?>
					<div class="alert alert-success solid">
						<?= $this->session->flashdata('success') ?>
					</div>
				<?php endif ?>

				<?php if ($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger solid">
						<?= $this->session->flashdata('error') ?>
					</div>
				<?php endif ?>
			</div>
			<div class="col-12">
				<a href="<?= base_url('barangmentah/exportlogpdf') ?>" target="_BLANK"><button class="btn btn-secondary my-3">Export PDF</button></a>
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Daftar Barang Mentah Log</h4>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="example" class="display" style="min-width: 845px">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama Barang</th>
										<th>Type</th>
										<th>Jumlah</th>
										<th>Keterangan</th>
										<th>Ditambahkan</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$no = 1;
									foreach ($bmentahlog->result_array() as $bmentahlog) : ?>
										<tr>
											<td><?= $no++ ?></td>
											<td><?= ucfirst($bmentahlog['nama_barang']) ?></td>
											<?php
											if ($bmentahlog['type'] == 'masuk') : ?>
												<td><span class="badge badge-success"><?= $bmentahlog['type'] ?></span></td>
											<?php endif ?>

											<?php
											if ($bmentahlog['type'] == 'keluar') : ?>
												<td><span class="badge badge-danger"><?= $bmentahlog['type'] ?></span></td>
											<?php endif ?>
											<td><?= $bmentahlog['jumlah'] ?></td>
											<td><?= ucfirst($bmentahlog['bmlogketerangan']) ?></td>
											<td><?= date('d M Y H:i:s', strtotime($bmentahlog['ca_bmentahlog'])) ?></td>
											<td>
												<div class="dropdown">
													<button id="toa" class="btn btn-sm btn-primary" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
														<span class="bi bi-three-dots-vertical"></span>
													</button>
													<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
														<a href="<?= base_url('barangmentah/hapusLog/' . $bmentahlog['barang_mentah_logs_id']) ?>" class="dropdown-item" onclick="return confirm('Yakin hapus log barang mentah ?')"><i class="fa fa-trash"></i> Hapus</a>
													</div>
												</div>
											</td>
										</tr>
									<?php endforeach ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--**********************************
            Content body end
        ***********************************-->
