<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-xl-12 col-lg-12">
				<?php if ($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger solid">
						<?= $this->session->flashdata('error') ?>
					</div>
				<?php endif ?>
				<div class="card card-bx">
					<div class="card-header">
						<h4 class="title">Form Tambah Barang Mentah</h4>
					</div>
					<form method="POST" action="<?= base_url('barangmentah/add') ?>">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-12 mb-3">
									<label class="form-label">Nama Barang</label>
									<input type="text" class="form-control" value="" name="nama_barang" placeholder="Masukkan Nama Barang" required>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Harga</label>
									<input type="number" class="form-control" name="harga" placeholder="Masukkan Harga" required>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Keterangan</label>
									<textarea name="keterangan" id="" cols="20" rows="5" class="form-control" required></textarea>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Stok</label>
									<input type="number" class="form-control" name="stock" placeholder="Masukkan Stok" required>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<button class="btn btn-primary" type="submit">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!--**********************************
            Content body end
        ***********************************-->
