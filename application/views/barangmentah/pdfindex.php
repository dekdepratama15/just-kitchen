<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Laporan Barang Mentah</title>
	<style>
		* {
			font-family: 'Times New Roman', Times, serif;
			margin: 0;
		}

		.container {
			margin: 0px auto;
			width: 100%;
		}

		.header {
			width: 100%;
			margin: 0rem;
		}

		.header img {
			width: 96%;
		}

		.content {
			width: 100%;
			margin: 0.5rem auto;
			text-align: center;
			font-size: 20px;
		}

		.content p {
			font-size: 14px;
			font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
			margin-bottom: 1rem;
		}

		.content table,
		th,
		td {
			border: 1px black solid;
			border-collapse: collapse;
			padding: 0.5rem;
			font-size: 16px;
		}

		.content-mentor {
			margin-bottom: 1rem;
		}

		.content-contestant table {
			margin-top: 1rem;
		}
	</style>
</head>

<body>
	<!-- Start Page -->
	<div class="container">
		<!-- KOP report -->
		<hr>
		<!-- Report Content -->
		<div class="content">
			<!-- Report Title Contestant Identity -->
			<div class="content-contestant">
				<div style="margin: 15px 0 15px 0;"><strong>DAFTAR BARANG MENTAH</strong></div>
				<hr>
				<!-- Table -->
				<table style="width: 100%; margin-top:50px;">
					<tr>
						<th>#</th>
						<th>NAMA BARANG</th>
						<th>HARGA</th>
						<th>KETERANGAN</th>
						<th>STOK</th>
					</tr>
					<?php
					$no = 1;
					foreach ($bmentah->result_array() as $bmentah) : ?>
						<tr>
							<td><?= $no++ ?></td>
							<td><?= ucfirst($bmentah['nama_barang']) ?></td>
							<td>Rp. <?= number_format($bmentah['harga'], 0, ",", ".") ?></td>
							<td><?= ucfirst($bmentah['keterangan']) ?></td>
							<td><?= $bmentah['stock'] ?></td>
						</tr>
					<?php endforeach ?>

				</table>
			</div>
		</div>
	</div>

</body>

</html>
