<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-xl-12 col-lg-12">
				<?php if ($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger solid">
						<?= $this->session->flashdata('error') ?>
					</div>
				<?php endif ?>
				<div class="card card-bx">
					<div class="card-header">
						<h4 class="title">Form Edit Pengumuman</h4>
					</div>
					<form method="POST" action="<?= base_url('pengumuman/editProcess/') . $pengumuman['pengumuman_id'] ?>" enctype="multipart/form-data">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-12 mb-3">
									<label class="form-label">Judul Pengumuman</label>
									<input type="text" class="form-control" value="<?= $pengumuman['judul'] ?>" name="judul" placeholder="Masukkan Judul Pengumuman" required>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Keterangan</label>
									<textarea name="keterangan" id="" cols="20" rows="5" class="form-control" required><?= $pengumuman['keterangan'] ?></textarea>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">File PDF (Abaikan jika tidak ingin merubah file sebelumnya)</label>
									<input type="file" class="form-control" name="file">
								</div>
							</div>
						</div>
						<div class="card-footer">
							<button class="btn btn-primary" type="submit">Edit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!--**********************************
            Content body end
        ***********************************-->
