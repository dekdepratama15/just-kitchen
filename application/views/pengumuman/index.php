<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->


		<div class="row">
			<div class="col-lg-12">
				<?php if ($this->session->flashdata('success')) : ?>
					<div class="alert alert-success solid">
						<?= $this->session->flashdata('success') ?>
					</div>
				<?php endif ?>

				<?php if ($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger solid">
						<?= $this->session->flashdata('error') ?>
					</div>
				<?php endif ?>
			</div>
			<div class="col-12">
				<a href="<?= base_url('pengumuman/tambah') ?>"><button class="btn btn-primary my-3">Tambah Pengumuman</button></a>
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Daftar Pengumuman</h4>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="example" class="display" style="min-width: 845px">
								<thead>
									<tr>
										<th>No</th>
										<th>Pembuat</th>
										<th>Judul</th>
										<th>Keterangan</th>
										<th>Tanggal Dibuat</th>
										<th>File</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$no = 1;
									foreach ($pengumumans->result_array() as $pengumuman) : ?>
										<tr>
											<td><?= $no++ ?></td>
											<td><?= $pengumuman['username'] ?></td>
											<td><?= $pengumuman['judul'] ?></td>
											<td><?= substr($pengumuman['keterangan'], 0, 30) ?>...</td>
											<td><?= date('d M Y H:i:s', strtotime($pengumuman['pengumuman_ca'])) ?></td>
											<td><a href="<?= base_url('assets/pengumuman/' . $pengumuman['file']) ?>" target="_BLANK"><button class="btn btn-sm btn-primary">Lihat File PDF</button></a></td>
											<td>
												<div class="dropdown">
													<button id="toa" class="btn btn-sm btn-primary" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
														<span class="bi bi-three-dots-vertical"></span>
													</button>
													<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
														<a href="<?= base_url('pengumuman/edit/' . $pengumuman['pengumuman_id']) ?>" class="dropdown-item"><i class="fa fa-edit" id="from1"></i> Edit</a>
														<a href="<?= base_url('pengumuman/hapus/' . $pengumuman['pengumuman_id']) ?>" class="dropdown-item" onclick="return confirm('Yakin hapus pengumuman ?')"><i class="fa fa-trash"></i> Hapus</a>
													</div>
												</div>
											</td>
										</tr>
									<?php endforeach ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--**********************************
            Content body end
        ***********************************-->
