		<!--**********************************
            Sidebar start
        ***********************************-->
		<div class="dlabnav">
			<div class="dlabnav-scroll">
				<div class="dropdown header-profile2 ">
					<a class="nav-link " href="javascript:void(0);" role="button" data-bs-toggle="dropdown">
						<div class="header-info2 d-flex align-items-center">
							<img src="<?= base_url('assets/images/user.jpg') ?>" alt="" style="margin-left: 13px;">
							<div class="d-flex align-items-center sidebar-info">
								<div>
									<span class="font-w400 d-block"><?= ucfirst($this->session->userdata('username')) ?></span>
									<small class="text-end font-w400"><?= ucfirst($this->session->userdata('role')) ?></small>
								</div>
								<i class="fas fa-chevron-down"></i>
							</div>

						</div>
					</a>
					<div class="dropdown-menu dropdown-menu-end">
						<!-- <a href="app-profile.html" class="dropdown-item ai-icon ">
							<svg xmlns="http://www.w3.org/2000/svg" class="text-primary" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
								<path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
								<circle cx="12" cy="7" r="4"></circle>
							</svg>
							<span class="ms-2">Profile </span>
						</a> -->
						<a href="<?= base_url('auth/logout') ?>" class="dropdown-item ai-icon">
							<svg xmlns="http://www.w3.org/2000/svg" class="text-danger" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
								<path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path>
								<polyline points="16 17 21 12 16 7"></polyline>
								<line x1="21" y1="12" x2="9" y2="12"></line>
							</svg>
							<span class="ms-2">Logout </span>
						</a>
					</div>
				</div>
				<ul class="metismenu" id="menu">
					<li class="<?= ($this->uri->segment(1) == 'dashboard') ? 'mm-active' : '' ?>"><a href="<?= base_url('dashboard') ?>" class="" aria-expanded="false">
							<i class="flaticon-025-dashboard"></i>
							<span class="nav-text">Dashboard</span>
						</a>
					</li>

					<?php if ($this->session->userdata('role') == 'admin') : ?>
						<li class="<?= ($this->uri->segment(1) == 'jadwal') ? 'mm-active' : '' ?>"><a href="<?= base_url('jadwal') ?>" class="" aria-expanded="false">
								<i class="fas fa-calendar-week"></i>
								<span class="nav-text">Jadwal</span>
							</a>
						</li>
					<?php endif ?>

					<li class="<?= ($this->uri->segment(1) == 'pindahjadwal') ? 'mm-active' : '' ?>"><a href="<?= base_url('pindahjadwal') ?>" class="" aria-expanded="false">
							<i class="fas fa-calendar-day"></i>
							<span class="nav-text">Pindah Jadwal</span>
						</a>
					</li>

					<li class="<?= ($this->uri->segment(1) == 'pengajuancuti') ? 'mm-active' : '' ?>"><a href="<?= base_url('pengajuancuti') ?>" class="" aria-expanded="false">
							<i class="fas fa-calendar-minus"></i>
							<span class="nav-text">Pengajuan Izin dan Cuti</span>
						</a>
					</li>

					<?php if ($this->session->userdata('role') == 'admin') : ?>
						<li class="<?= ($this->uri->segment(1) == 'karyawan') ? 'mm-active' : '' ?>"><a href="<?= base_url('karyawan') ?>" class="" aria-expanded="false">
								<i class="fas fa-users"></i>
								<span class="nav-text">Karyawan</span>
							</a>
						</li>
					<?php endif ?>

					<?php if ($this->session->userdata('role') == 'admin') : ?>
						<li class="<?= ($this->uri->segment(1) == 'pengumuman') ? 'mm-active' : '' ?>"><a href="<?= base_url('pengumuman') ?>" class="" aria-expanded="false">
								<i class="fas fa-bullhorn"></i>
								<span class="nav-text">Pengumuman</span>
							</a>
						</li>
					<?php endif ?>

					<?php if ($this->session->userdata('role') == 'admin') : ?>
						<li class="<?= ($this->uri->segment(1) == 'peringatan') ? 'mm-active' : '' ?>"><a href="<?= base_url('peringatan') ?>" class="" aria-expanded="false">
								<i class="fas fa-exclamation-circle"></i>
								<span class="nav-text">Peringatan</span>
							</a>
						</li>
					<?php endif ?>

					<?php if ($this->session->userdata('role') == 'admin') : ?>
						<li><a class="has-arrow " href="javascript:void()" aria-expanded="false">
								<i class="fas fa-store"></i>
								<span class="nav-text">Barang Mentah</span>
							</a>
							<ul aria-expanded="false">
								<li class="<?= ($this->uri->segment(1) =='barangmentah' && $this->uri->segment(2) == NULL || $this->uri->segment(1) == 'barangmentah' && $this->uri->segment(2) == 'tambah') ? 'mm-active' : '' ?>"><a href="<?= base_url('barangmentah') ?>" class="<?= ($this->uri->segment(1) =='barangmentah' && $this->uri->segment(2) == NULL || $this->uri->segment(1) == 'barangmentah' && $this->uri->segment(2) == 'tambah') ? 'mm-active' : '' ?>">Daftar Barang Mentah</a></li>
								<li class="<?= ($this->uri->segment(1) == 'barangmentah' && $this->uri->segment(2) == 'keluar') ? 'mm-active' : '' ?>"><a href="<?= base_url('barangmentah/keluar') ?>" class="<?= ($this->uri->segment(1) == 'barangmentah' && $this->uri->segment(2) == 'keluar') ? 'mm-active' : '' ?>">Form Barang Mentah Keluar</a></li>
								<li class="<?= ($this->uri->segment(1) == 'barangmentah' && $this->uri->segment(2) == 'log') ? 'mm-active' : '' ?>"><a href="<?= base_url('barangmentah/log') ?>" class="<?= ($this->uri->segment(1) == 'barangmentah' && $this->uri->segment(2) == 'log') ? 'mm-active' : '' ?>">Log Barang Mentah</a></li>
							</ul>
						</li>
					<?php endif ?>

					<?php if ($this->session->userdata('role') == 'admin') : ?>
						<li><a class="has-arrow " href="javascript:void()" aria-expanded="false">
								<i class="fas fa-cubes"></i>
								<span class="nav-text">Barang Stok</span>
							</a>
							<ul aria-expanded="false">
								<li class="<?= ($this->uri->segment(1) =='barangstok' && $this->uri->segment(2) == NULL || $this->uri->segment(1) == 'barangstok' && $this->uri->segment(2) == 'tambah') ? 'mm-active' : '' ?>"><a href="<?= base_url('barangstok') ?>" class="<?= ($this->uri->segment(1) =='barangstok' && $this->uri->segment(2) == NULL || $this->uri->segment(1) == 'barangstok' && $this->uri->segment(2) == 'tambah') ? 'mm-active' : '' ?>">Daftar Barang Stok</a></li>
								<li class="<?= ($this->uri->segment(1) == 'barangstok' && $this->uri->segment(2) == 'keluar') ? 'mm-active' : '' ?>"><a href="<?= base_url('barangstok/keluar') ?>" class="<?= ($this->uri->segment(1) == 'barangstok' && $this->uri->segment(2) == 'keluar') ? 'mm-active' : '' ?>">Form Barang Stok Keluar</a></li>
								<li class="<?= ($this->uri->segment(1) == 'barangstok' && $this->uri->segment(2) == 'log') ? 'mm-active' : '' ?>"><a href="<?= base_url('barangstok/log') ?>" class="<?= ($this->uri->segment(1) == 'barangstok' && $this->uri->segment(2) == 'log') ? 'mm-active' : '' ?>">Log Barang Stok</a>
								</li>
							</ul>
						</li>
					<?php endif ?>
				</ul>
			</div>
		</div>
		<!--**********************************
            Sidebar end
        ***********************************-->
