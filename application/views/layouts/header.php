<?php
$this->db->select('pengumumans.*, users.*, pengumumans.id AS pengumuman_id, pengumumans.created_at AS pengumuman_ca, pengumumans.updated_at AS pengumuman_ua');
$this->db->join('users', 'pengumumans.user_id = users.id');
$this->db->order_by('pengumumans.created_at', 'desc');
$pengumumans = $this->db->get('pengumumans');

$row = $pengumumans->num_rows();
?>
<!DOCTYPE html>
<html lang="en">

<head>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="author" content="DexignLab">
	<meta name="robots" content="">
	<meta name="keywords" content="admin dashboard, admin template, analytics, bootstrap, bootstrap 5, bootstrap 5 admin template, job board admin, job portal admin, modern, responsive admin dashboard, sales dashboard, sass, ui kit, web app, frontend">
	<meta name="description" content="We proudly present Jobick, a Job Admin dashboard HTML Template, If you are hiring a job expert you would like to build a superb website for your Jobick, it's a best choice.">
	<meta property="og:title" content="Jobick : Job Admin Dashboard Bootstrap 5 Template + FrontEnd">
	<meta property="og:description" content="We proudly present Jobick, a Job Admin dashboard HTML Template, If you are hiring a job expert you would like to build a superb website for your Jobick, it's a best choice.">
	<meta property="og:image" content="https://jobick.dexignlab.com/xhtml/social-image.png">
	<meta name="format-detection" content="telephone=no">

	<!-- Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Page Title -->
	<title>Just Kitchen - <?= ucfirst($this->uri->segment(1)) ?></title>

	<!-- Favicon icon -->
	<link rel="shortcut icon" type="image/png" href="<?= base_url('assets/images/favicon.png') ?>">

	<!-- All StyleSheet -->
	<link href="<?= base_url('assets/vendor/bootstrap-select/dist/css/bootstrap-select.min.css') ?>" rel="stylesheet">
	<link href="<?= base_url('assets/vendor/owl-carousel/owl.carousel.css') ?>" rel="stylesheet">

	<!-- Globle CSS -->
	<link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet">

	<!-- Datatable -->
	<link href="<?= base_url('assets/vendor/datatables/css/jquery.dataTables.min.css') ?>" rel="stylesheet">

	<!-- Daterange picker -->
	<link href="<?= base_url('assets/vendor/bootstrap-daterangepicker/daterangepicker.css') ?>" rel="stylesheet">
	<!-- Clockpicker -->
	<link href="<?= base_url('assets/vendor/clockpicker/css/bootstrap-clockpicker.min.css') ?>" rel="stylesheet">
	<!-- asColorpicker -->
	<link href="<?= base_url('assets/vendor/jquery-asColorPicker/css/asColorPicker.min.css') ?>" rel="stylesheet">
	<!-- Material color picker -->
	<link href="<?= base_url('assets/vendor/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') ?>" rel="stylesheet">

	<!-- Pick date -->
	<link rel="stylesheet" href="<?= base_url('assets/vendor/pickadate/themes/default.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/vendor/pickadate/themes/default.date.css') ?>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<script type="text/javascript" src="<?= base_url('assets/js/jquery-3.6.0.min.js') ?>"></script>

</head>

<body>

	<!--*******************
        Preloader start
    ********************-->
	<div id="preloader">
		<div class="lds-ripple">
			<div></div>
			<div></div>
		</div>
	</div>
	<!--*******************
        Preloader end
    ********************-->

	<!--**********************************
        Main wrapper start
    ***********************************-->
	<div id="main-wrapper">

		<!--**********************************
            Nav header start
        ***********************************-->
		<div class="nav-header">
			<a href="<?= base_url('dashboard') ?>" class="brand-logo mx-3 text-primary">
				<h4 class="text-primary" style="font-weight: 800;">Just Kitchen</h4>

			</a>
			<div class="nav-control">
				<div class="hamburger">
					<span class="line"></span><span class="line"></span><span class="line"></span>
				</div>
			</div>
		</div>
		<!--**********************************
            Nav header end
        ***********************************-->

		<!--**********************************
            Header start
        ***********************************-->
		<div class="header">
			<div class="header-content">
				<nav class="navbar navbar-expand">
					<div class="collapse navbar-collapse justify-content-between">
						<div class="header-left">
							<div class="dashboard_bar">
								<?php
								if ($this->uri->segment(1) == 'dashboard') {
									echo 'Dashboard';
								} elseif ($this->uri->segment(1) == 'karyawan') {
									echo 'Karyawan';
								} elseif ($this->uri->segment(1) == 'pengumuman') {
									echo 'Pengumuman';
								} elseif ($this->uri->segment(1) == 'peringatan') {
									echo 'Peringatan';
								} elseif ($this->uri->segment(1) == 'jadwal') {
									echo 'Jadwal';
								} elseif ($this->uri->segment(1) == 'pindahjadwal') {
									echo 'Pindah Jadwal';
								} elseif ($this->uri->segment(1) == 'pengajuancuti') {
									echo 'Pengajuan Izin dan Cuti';
								} elseif ($this->uri->segment(1) == 'barangmentah'&& $this->uri->segment(2) == NULL) {
									echo 'Barang Mentah';
								} elseif ($this->uri->segment(1) == 'barangmentah' && $this->uri->segment(2) == 'tambah') {
									echo 'Tambah Barang Mentah';
								} elseif ($this->uri->segment(1) == 'barangmentah' && $this->uri->segment(2) == 'keluar') {
									echo 'Barang Mentah Keluar';
								} elseif ($this->uri->segment(1) == 'barangmentah' && $this->uri->segment(2) == 'log') {
									echo 'Barang Mentah Log';
								} elseif ($this->uri->segment(1) =='barangstok' && $this->uri->segment(2) == NULL) {
									echo 'Barang Stok';
								} elseif ($this->uri->segment(1) == 'barangstok' && $this->uri->segment(2) == 'tambah') {
									echo 'Tambah Barang Stok';
								} elseif ($this->uri->segment(1) == 'barangstok' && $this->uri->segment(2) == 'keluar') {
									echo 'Barang Stok Keluar';
								} elseif ($this->uri->segment(1) == 'barangstok' && $this->uri->segment(2) == 'log') {
									echo 'Barang Stok Log';
								}
								?>
							</div>
							<!-- <div class="nav-item d-flex align-items-center">
								<div class="input-group search-area">
									<input type="text" class="form-control" placeholder="Search">
									<span class="input-group-text"><a href="javascript:void(0)"><i class="flaticon-381-search-2"></i></a></span>
								</div>
							</div> -->
						</div>
						<ul class="navbar-nav header-right">

							<li class="nav-item dropdown notification_dropdown">
								<a class="nav-link bell dz-theme-mode p-0" href="javascript:void(0);">
									<i id="icon-light" class="fas fa-sun"></i>
									<i id="icon-dark" class="fas fa-moon"></i>

								</a>
							</li>

							<li class="nav-item dropdown notification_dropdown">
								<a class="nav-link" href="javascript:void(0);" role="button" data-bs-toggle="dropdown">
									<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
										<g data-name="Layer 2" transform="translate(-2 -2)">
											<path id="Path_20" data-name="Path 20" d="M22.571,15.8V13.066a8.5,8.5,0,0,0-7.714-8.455V2.857a.857.857,0,0,0-1.714,0V4.611a8.5,8.5,0,0,0-7.714,8.455V15.8A4.293,4.293,0,0,0,2,20a2.574,2.574,0,0,0,2.571,2.571H9.8a4.286,4.286,0,0,0,8.4,0h5.23A2.574,2.574,0,0,0,26,20,4.293,4.293,0,0,0,22.571,15.8ZM7.143,13.066a6.789,6.789,0,0,1,6.78-6.78h.154a6.789,6.789,0,0,1,6.78,6.78v2.649H7.143ZM14,24.286a2.567,2.567,0,0,1-2.413-1.714h4.827A2.567,2.567,0,0,1,14,24.286Zm9.429-3.429H4.571A.858.858,0,0,1,3.714,20a2.574,2.574,0,0,1,2.571-2.571H21.714A2.574,2.574,0,0,1,24.286,20a.858.858,0,0,1-.857.857Z" />
										</g>
									</svg>
									<?php if ($row > 0) : ?>
										<span class="badge light text-white bg-primary rounded-circle"><?= $row ?></span>
									<?php endif ?>

								</a>
								<div class="dropdown-menu dropdown-menu-end">
									<h4 class="text-center mx-3 my-3">Pengumuman</h4>
									<hr>
									<div id="DZ_W_Notification1" class="widget-media dlab-scroll p-3" style="height:380px;">
										<ul class="timeline">
											<?php if ($row > 0) : ?>
												<?php foreach ($pengumumans->result_array() as $pengumuman) : ?>
													<li>
														<div class="timeline-panel">
															<div class="media-body">
																<h6 class="mb-1"><?= $pengumuman['judul'] ?></h6>
																<p class="mb-1"><?= $pengumuman['keterangan'] ?></p>
																<a href="<?= base_url('assets/pengumuman/' . $pengumuman['file']) ?>" target="_BLANK"><button class="btn btn-xs btn-primary">Download</button></a>
																<small class="d-block mt-1"><?= $pengumuman['username'] ?> - <?= date('d M Y H:i:s', strtotime($pengumuman['pengumuman_ca'])) ?></small>
															</div>
														</div>
													</li>
												<?php endforeach ?>
											<?php endif ?>
											<?php if ($row <= 0) : ?>
												<li>
													<div class="timeline-panel">
														<div class="media-body">
															Tidak ada pengumuman !
														</div>
													</div>
												</li>
											<?php endif ?>
										</ul>
									</div>
								</div>
							</li>

							<li class="nav-item dropdown header-profile">
								<a class="nav-link" href="javascript:void(0);" role="button" data-bs-toggle="dropdown">
									<img src="<?= base_url('assets/images/user.jpg') ?>" alt="">
								</a>
								<div class="dropdown-menu dropdown-menu-end">
									<!-- <a href="app-profile.html" class="dropdown-item ai-icon">
										<svg id="icon-user2" xmlns="http://www.w3.org/2000/svg" class="text-primary" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
											<path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
											<circle cx="12" cy="7" r="4"></circle>
										</svg>
										<span class="ms-2">Profile </span>
									</a> -->
									<a href="<?= base_url('auth/logout') ?>" class="dropdown-item ai-icon">
										<svg xmlns="http://www.w3.org/2000/svg" class="text-danger" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
											<path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path>
											<polyline points="16 17 21 12 16 7"></polyline>
											<line x1="21" y1="12" x2="9" y2="12"></line>
										</svg>
										<span class="ms-2">Logout </span>
									</a>
								</div>
							</li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
		<!--**********************************
            Header end ti-comment-alt
        ***********************************-->
