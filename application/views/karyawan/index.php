<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->


		<div class="row">
			<div class="col-lg-12">
				<?php if ($this->session->flashdata('success')) : ?>
					<div class="alert alert-success solid">
						<?= $this->session->flashdata('success') ?>
					</div>
				<?php endif ?>

				<?php if ($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger solid">
						<?= $this->session->flashdata('error') ?>
					</div>
				<?php endif ?>
			</div>
			<div class="col-12">
				<a href="<?= base_url('karyawan/tambah') ?>"><button class="btn btn-primary my-3">Tambah Karyawan</button></a>
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Daftar Karyawan</h4>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="example" class="display" style="min-width: 845px">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama</th>
										<th>Jabatan</th>
										<th>Email</th>
										<th>Status</th>
										<th>Status Karyawan</th>
										<th>Tanggal Gabung</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$no = 1;
									foreach ($karyawans->result_array() as $karyawan) : ?>
										<tr>
											<td><?= $no++ ?></td>
											<td><?= $karyawan['nama_lengkap'] ?></td>
											<td><?= $karyawan['jabatan'] ?></td>
											<td><?= $karyawan['email'] ?></td>
											<?php
											if ($karyawan['status'] == 'Aktif') : ?>
												<td><span class="badge badge-success"><?= $karyawan['status'] ?></span></td>
											<?php endif ?>

											<?php
											if ($karyawan['status'] == 'Resign') : ?>
												<td><span class="badge badge-danger"><?= $karyawan['status'] ?></span></td>
											<?php endif ?>

											<td><?= $karyawan['status_karyawan'] ?></td>
											<td><?= date('d M Y', strtotime($karyawan['tgl_bergabung'])) ?></td>
											<td>
												<div class="dropdown">
													<button id="toa" class="btn btn-sm btn-primary" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
														<span class="bi bi-three-dots-vertical"></span>
													</button>
													<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
														<a href="<?= base_url('karyawan/detail/' . $karyawan['user_id']) ?>" class="dropdown-item"><i class="fas fa-info-circle"></i> Detail </a>
														<a href="<?= base_url('karyawan/edit/' . $karyawan['user_id']) ?>" class="dropdown-item"><i class="fa fa-edit" id="from1"></i> Edit</a>
														<a href="<?= base_url('karyawan/hapus/' . $karyawan['user_id']) ?>" class="dropdown-item" onclick="return confirm('Yakin hapus karyawan ?')"><i class="fa fa-trash"></i> Hapus</a>
													</div>
												</div>
											</td>
										</tr>
									<?php endforeach ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--**********************************
            Content body end
        ***********************************-->
