<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-xl-12 col-lg-12">
				<div class="clearfix">
					<div class="card card-bx author-profile mb-5">
						<div class="card-body">
							<div class="p-5">
								<div class="author-profile">
									<div class="author-info">
										<h6 class="title"><?= ucwords($karyawan['nama_lengkap']) ?></h6>
										<span><?= $karyawan['nama_panggilan'] ?></span>
										<span><?= $karyawan['jabatan'] ?></span>
									</div>
								</div>
							</div>
							<div class="info-list">
								<ul>
									<li>Username<span><?= $karyawan['username'] ?></span></li>
									<li>Email<span><?= $karyawan['email'] ?></span></li>
									<li>Status
										<?php
										if ($karyawan['status'] == 'Aktif') : ?>
											<span class="badge badge-success text-white"><?= $karyawan['status'] ?></span>
										<?php endif ?>

										<?php
										if ($karyawan['status'] == 'Resign') : ?>
											<span class="badge badge-danger text-white"><?= $karyawan['status'] ?></span>
										<?php endif ?>
									</li>
									<li>Jenis Kelamin<span><?= ucwords($karyawan['jenis_kelamin']) ?></span></li>
									<li>Golongan Darah<span><?= $karyawan['golongan_darah'] ?></span></li>
									<li>Agama<span><?= ucwords($karyawan['agama']) ?></span></li>
									<li>Status Kawin<span><?= ucwords($karyawan['status_perkawinan']) ?></span></li>
									<li>Status Karyawan<span><?= ucwords($karyawan['status_karyawan']) ?></span></li>
									<li>Tempat, Tanggal Lahir<span><?= ucwords($karyawan['tempat_lahir']) ?>, <?= date('d M Y', strtotime($karyawan['tgl_lahir'])) ?></span></li>
									<li>Jenis Identitas<span><?= ucwords($karyawan['jenis_identitas']) ?></span></li>
									<li>No Identitas<span><?= ucwords($karyawan['no_identitas']) ?></span></li>
									<li>No Telp<span><?= ucwords($karyawan['no_telp']) ?></span></li>
									<li>Alamat<span><?= ucfirst($karyawan['alamat']) ?></span></li>
								</ul>
							</div>
						</div>
						<div class="card-footer">
							<div class="mb-3">
								Tanggal Bergabung : <?= date('d M Y', strtotime($karyawan['tgl_bergabung'])) ?>
							</div>
							<div class="mb-3">
								<?php
								if ($karyawan['tgl_selesai'] != NULL) : ?>
									Tanggal Selesai : <?= date('d M Y', strtotime($karyawan['tgl_selesai'])) ?>
								<?php endif ?>

								<?php
								if ($karyawan['tgl_selesai'] == NULL) : ?>
									Tanggal Selesai : -
								<?php endif ?>
							</div>
							<a href="<?= base_url('karyawan') ?>"><button class="btn btn-sm btn-primary">Kembali</button></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--**********************************
            Content body end
        ***********************************-->
