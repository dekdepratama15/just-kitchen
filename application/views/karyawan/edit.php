<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-xl-12 col-lg-12">
				<?php if ($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger solid">
						<?= $this->session->flashdata('error') ?>
					</div>
				<?php endif ?>
				<div class="card card-bx">
					<div class="card-header">
						<h4 class="title">Form Edit Karyawan</h4>
					</div>
					<form method="POST" action="<?= base_url('karyawan/editProcess/') . $karyawan['user_id'] ?>">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-6 mb-3">
									<label class="form-label">Username</label>
									<input type="text" class="form-control" value="<?= $karyawan['username'] ?>" name="username" placeholder="Masukkan Username" required>

								</div>
								<div class="col-sm-6 mb-3">
									<label class="form-label">Email</label>
									<input type="email" class="form-control" value="<?= $karyawan['email'] ?>" name="email" placeholder="Masukkan Email" required>
								</div>
								<div class="col-sm-6 mb-3">
									<label class="form-label">Password (Abaikan jika tidak ingin ubah password)</label>
									<input type="password" class="form-control" value="" name="password" placeholder="Masukkan Password">
								</div>
								<div class="col-sm-6 mb-3">
									<label class="form-label">Re-Password (Abaikan jika tidak ingin ubah password)</label>
									<input type="password" class="form-control" value="" name="repassword" placeholder="Masukkan Re-Password">
								</div>
								<div class="col-sm-4 mb-3">
									<label class="form-label">Jabatan</label>
									<select class="selectpicker nice-select default-select form-control wide mh-auto" name="jabatan" required>
										<option disabled>- Pilih Jabatan -</option>
										<?php
										$jabatan = ['Pimpinan', 'Staff'];
										foreach ($jabatan as $jabatan) :
											$selected = ($karyawan['jabatan'] == $jabatan) ? 'selected' : ''; ?>
											<option <?= $selected ?> value="<?= $jabatan ?>"><?= $jabatan ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-4 mb-3">
									<label class="form-label">Nama Lengkap</label>
									<input type="text" class="form-control" value="<?= $karyawan['nama_lengkap'] ?>" placeholder="Masukkan Nama Lengkap" name="nama_lengkap" required>
								</div>
								<div class="col-sm-4 mb-3">
									<label class="form-label">Nama Panggilan</label>
									<input type="text" class="form-control" value="<?= $karyawan['nama_panggilan'] ?>" name="nama_panggilan" placeholder="Masukkan Nama Panggilan" required>
								</div>
								<div class="col-sm-4 mb-3">
									<label class="form-label">Status</label>
									<select class="selectpicker nice-select default-select form-control wide mh-auto" name="status" required>
										<option disabled>- Pilih Status -</option>
										<?php
										$status = ['Aktif', 'Resign'];
										foreach ($status as $status) :
											$selected = ($karyawan['status'] == $status) ? 'selected' : ''; ?>
											<option <?= $selected ?> value="<?= $status ?>"><?= $status ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-4 mb-3">
									<label class="form-label">Jenis Kelamin</label>
									<select class="selectpicker nice-select default-select form-control wide mh-auto" name="jenis_kelamin" required>
										<option disabled>- Pilih Jenis Kelamin -</option>
										<?php
										$jeniskelamin = ['Laki-laki', 'Perempuan'];
										foreach ($jeniskelamin as $jk) :
											$selected = ($karyawan['jenis_kelamin'] == $jk) ? 'selected' : ''; ?>
											<option <?= $selected ?> value="<?= $jk ?>"><?= $jk ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-4 mb-3">
									<label class="form-label">Golongan Darah</label>
									<select class="selectpicker nice-select default-select form-control wide mh-auto" name="golongan_darah" required>
										<option disabled>- Pilih Golongan Darah -</option>
										<?php
										$golongandarah = ['A', 'B', 'AB', 'O', '-'];
										foreach ($golongandarah as $gd) :
											$selected = ($karyawan['golongan_darah'] == $gd) ? 'selected' : ''; ?>
											<option <?= $selected ?> value="<?= $gd ?>"><?= $gd ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-4 mb-3">
									<label class="form-label">Agama</label>
									<select class="selectpicker nice-select default-select form-control wide mh-auto" name="agama" required>
										<option disabled>- Pilih Agama -</option>
										<?php
										$agama = ['Islam', 'Kristen', 'Katolik', 'Hindu', 'Buddha', 'Khonghucu'];
										foreach ($agama as $agama) :
											$selected = ($karyawan['agama'] == $agama) ? 'selected' : ''; ?>
											<option <?= $selected ?> value="<?= $agama ?>"><?= $agama ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-4 mb-3">
									<label class="form-label">Status Perkawinan</label>
									<select class="selectpicker nice-select default-select form-control wide mh-auto" name="status_perkawinan" required>
										<option disabled>- Pilih Status Perkawinan -</option>
										<?php
										$statusperkawinan = ['Belum Kawin', 'Kawin'];
										foreach ($statusperkawinan as $statusperkawinan) :
											$selected = ($karyawan['status_perkawinan'] == $statusperkawinan) ? 'selected' : ''; ?>
											<option <?= $selected ?> value="<?= $statusperkawinan ?>"><?= $statusperkawinan ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-4 mb-3">
									<label class="form-label">Status Karyawan</label>
									<select class="selectpicker nice-select default-select form-control wide mh-auto" name="status_karyawan" required>
										<option disabled>- Pilih Status Karyawan -</option>
										<?php
										$statuskaryawan = ['Daily', 'Training', 'Kontrak', 'Tetap'];
										foreach ($statuskaryawan as $statuskaryawan) :
											$selected = ($karyawan['status_karyawan'] == $statuskaryawan) ? 'selected' : ''; ?>
											<option <?= $selected ?> value="<?= $statuskaryawan ?>"><?= $statuskaryawan ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-4 mb-3">
									<label class="form-label">Jenis Identitas</label>
									<input type="text" class="form-control" value="<?= $karyawan['jenis_identitas'] ?>" name="jenis_identitas" placeholder="Masukkan Jenis Identitas" required>
								</div>
								<div class="col-sm-4 mb-3">
									<label class="form-label">No Identitas</label>
									<input type="number" class="form-control" value="<?= $karyawan['no_identitas'] ?>" name="no_identitas" placeholder="Masukkan No Identitas" required>
								</div>
								<div class="col-sm-4 mb-3">
									<label class="form-label">No telp</label>
									<input type="number" class="form-control" value="<?= $karyawan['no_telp'] ?>" name="no_telp" placeholder="Masukkan No Telp" required>
								</div>
								<div class="col-sm-6 mb-3">
									<label class="form-label">Tanggal Lahir</label>
									<input type="date" class="form-control" value="<?= $karyawan['tgl_lahir'] ?>" name="tgl_lahir" placeholder="Pilih Tanggal Lahir" required>
								</div>
								<div class="col-sm-6 mb-3">
									<label class="form-label">Tempat Lahir</label>
									<input type="a" class="form-control" value="<?= $karyawan['tempat_lahir'] ?>" name="tempat_lahir" placeholder="Masukkan Tempat Lahir" required>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Alamat</label>
									<textarea name="alamat" id="" cols="20" rows="5" class="form-control" required><?= $karyawan['alamat'] ?></textarea>
								</div>
								<div class="col-sm-6 mb-3">
									<label class="form-label">Tanggal Bergabung</label>
									<input type="date" class="form-control" value="<?= $karyawan['tgl_bergabung'] ?>" name="tgl_bergabung" placeholder="Pilih Tanggal Gabung" required>
								</div>
								<div class="col-sm-6 mb-3">
									<label class="form-label">Tanggal Selesai (Abaikan Jika Karyawan Belum Selesai)</label>
									<input type="date" class="form-control" value="<?= $karyawan['tgl_selesai'] ?>" name="tgl_selesai" placeholder="Pilih Tanggal Selesai">
								</div>
							</div>
						</div>
						<div class="card-footer">
							<button class="btn btn-primary" type="submit">Edit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!--**********************************
            Content body end
        ***********************************-->
