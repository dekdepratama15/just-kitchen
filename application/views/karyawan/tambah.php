<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-xl-12 col-lg-12">
				<?php if ($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger solid">
						<?= $this->session->flashdata('error') ?>
					</div>
				<?php endif ?>
				<div class="card card-bx">
					<div class="card-header">
						<h4 class="title">Form Tambah Karyawan</h4>
					</div>
					<form method="POST" action="<?= base_url('karyawan/add') ?>">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-6 mb-3">
									<label class="form-label">Username</label>
									<input type="text" class="form-control" value="" name="username" placeholder="Masukkan Username" required>

								</div>
								<div class="col-sm-6 mb-3">
									<label class="form-label">Email</label>
									<input type="email" class="form-control" name="email" placeholder="Masukkan Email" required>
								</div>
								<div class="col-sm-6 mb-3">
									<label class="form-label">Password</label>
									<input type="password" class="form-control" value="" name="password" placeholder="Masukkan Password" required>
								</div>
								<div class="col-sm-6 mb-3">
									<label class="form-label">Re-Password</label>
									<input type="password" class="form-control" value="" name="repassword" placeholder="Masukkan Re-Password" required>
								</div>
								<div class="col-sm-4 mb-3">
									<label class="form-label">Jabatan</label>
									<select class="selectpicker nice-select default-select form-control wide mh-auto" name="jabatan" required>
										<option selected disabled>- Pilih Jabatan -</option>
										<?php
										$jabatan = ['Pimpinan', 'Staff'];
										foreach ($jabatan as $jabatan) : ?>
											<option value="<?= $jabatan ?>"><?= $jabatan ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-4 mb-3">
									<label class="form-label">Nama Lengkap</label>
									<input type="text" class="form-control" value="" placeholder="Masukkan Nama Lengkap" name="nama_lengkap" required>
								</div>
								<div class="col-sm-4 mb-3">
									<label class="form-label">Nama Panggilan</label>
									<input type="text" class="form-control" value="" name="nama_panggilan" placeholder="Masukkan Nama Panggilan" required>
								</div>
								<div class="col-sm-4 mb-3">
									<label class="form-label">Status</label>
									<select class="selectpicker nice-select default-select form-control wide mh-auto" name="status" required>
										<option selected disabled>- Pilih Status -</option>
										<?php
										$status = ['Aktif', 'Resign'];
										foreach ($status as $status) : ?>
											<option value="<?= $status ?>"><?= $status ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-4 mb-3">
									<label class="form-label">Jenis Kelamin</label>
									<select class="selectpicker nice-select default-select form-control wide mh-auto" name="jenis_kelamin" required>
										<option selected disabled>- Pilih Jenis Kelamin -</option>
										<?php
										$jeniskelamin = ['Laki-laki', 'Perempuan'];
										foreach ($jeniskelamin as $jk) : ?>
											<option value="<?= $jk ?>"><?= $jk ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-4 mb-3">
									<label class="form-label">Golongan Darah</label>
									<select class="selectpicker nice-select default-select form-control wide mh-auto" name="golongan_darah" required>
										<option selected disabled>- Pilih Golongan Darah -</option>
										<?php
										$golongandarah = ['A', 'B', 'AB', 'O', '-'];
										foreach ($golongandarah as $gd) : ?>
											<option value="<?= $gd ?>"><?= $gd ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-4 mb-3">
									<label class="form-label">Agama</label>
									<select class="selectpicker nice-select default-select form-control wide mh-auto" name="agama" required>
										<option selected disabled>- Pilih Agama -</option>
										<?php
										$agama = ['Islam', 'Kristen', 'Katolik', 'Hindu', 'Buddha', 'Khonghucu'];
										foreach ($agama as $agama) : ?>
											<option value="<?= $agama ?>"><?= $agama ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-4 mb-3">
									<label class="form-label">Status Perkawinan</label>
									<select class="selectpicker nice-select default-select form-control wide mh-auto" name="status_perkawinan" required>
										<option selected disabled>- Pilih Status Perkawinan -</option>
										<?php
										$statusperkawinan = ['Belum Kawin', 'Kawin'];
										foreach ($statusperkawinan as $statusperkawinan) : ?>
											<option value="<?= $statusperkawinan ?>"><?= $statusperkawinan ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-4 mb-3">
									<label class="form-label">Status Karyawan</label>
									<select class="selectpicker nice-select default-select form-control wide mh-auto" name="status_karyawan" required>
										<option selected disabled>- Pilih Status Karyawan -</option>
										<?php
										$statuskaryawan = ['Daily', 'Training', 'Kontrak', 'Tetap'];
										foreach ($statuskaryawan as $statuskaryawan) : ?>
											<option value="<?= $statuskaryawan ?>"><?= $statuskaryawan ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-4 mb-3">
									<label class="form-label">Jenis Identitas</label>
									<input type="text" class="form-control" value="" name="jenis_identitas" placeholder="Masukkan Jenis Identitas" required>
								</div>
								<div class="col-sm-4 mb-3">
									<label class="form-label">No Identitas</label>
									<input type="number" class="form-control" value="" name="no_identitas" placeholder="Masukkan No Identitas" required>
								</div>
								<div class="col-sm-4 mb-3">
									<label class="form-label">No telp</label>
									<input type="number" class="form-control" value="" name="no_telp" placeholder="Masukkan No Telp" required>
								</div>
								<div class="col-sm-6 mb-3">
									<label class="form-label">Tanggal Lahir</label>
									<input type="date" class="form-control" value="" name="tgl_lahir" placeholder="Pilih Tanggal Lahir" required>
								</div>
								<div class="col-sm-6 mb-3">
									<label class="form-label">Tempat Lahir</label>
									<input type="a" class="form-control" value="" name="tempat_lahir" placeholder="Masukkan Tempat Lahir" required>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Alamat</label>
									<textarea name="alamat" id="" cols="20" rows="5" class="form-control" required></textarea>
								</div>
								<div class="col-sm-6 mb-3">
									<label class="form-label">Tanggal Bergabung</label>
									<input type="date" class="form-control" value="" name="tgl_bergabung" placeholder="Pilih Tanggal Gabung" required>
								</div>
								<div class="col-sm-6 mb-3">
									<label class="form-label">Tanggal Selesai (Abaikan Jika Karyawan Belum Selesai)</label>
									<input type="date" class="form-control" value="" name="tgl_selesai" placeholder="Pilih Tanggal Selesai">
								</div>
							</div>
						</div>
						<div class="card-footer">
							<button class="btn btn-primary" type="submit">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!--**********************************
            Content body end
        ***********************************-->
