<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-xl-12 col-lg-12">
				<?php if ($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger solid">
						<?= $this->session->flashdata('error') ?>
					</div>
				<?php endif ?>
				<div class="card card-bx">
					<div class="card-header">
						<h4 class="title">Form Edit Jadwal Karyawan</h4>
					</div>
					<form method="POST" action="<?= base_url('jadwal/editProcess/') . $jadwal['jadwal_id'] ?>" enctype="multipart/form-data">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-12 mb-3">
									<label class="form-label">Karyawan</label>
									<select class="selectpicker nice-select default-select form-control wide mh-auto" name="karyawan_id" required>
										<option selected disabled>- Pilih Karyawan -</option>
										<?php foreach ($karyawans->result_array() as $karyawan) :
											$selected = ($karyawan['karyawan_id'] == $jadwal['karyawan_id']) ? 'selected' : '';
										?>
											<option value="<?= $karyawan['karyawan_id'] ?>" <?= $selected ?>><?= $karyawan['nama_panggilan'] ?> - <?= $karyawan['jabatan'] ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Tanggal</label>
									<input type="date" class="form-control" name="tanggal" value="<?= $jadwal['tanggal'] ?>" required>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Status</label>
									<select class="selectpicker nice-select default-select form-control wide mh-auto" name="status" required>
										<option disabled>- Pilih Status -</option>
										<?php
										$status = ['belum', 'hadir', 'sakit', 'cuti', 'libur', 'pindah'];
										foreach ($status as $status) :
											$selected = ($jadwal['jadwal_status'] == $status) ? 'selected' : ''; ?>
											<option <?= $selected ?> value="<?= $status ?>"><?= ucfirst($status) ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Jadwal Masuk</label>
									<input type="time" class="form-control" value="<?= $jadwal['jadwal_masuk'] ?>" name="jadwal_masuk" required>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Jadwal Keluar</label>
									<input type="time" class="form-control" value="<?= $jadwal['jadwal_keluar'] ?>" name="jadwal_keluar" required>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<button class="btn btn-primary" type="submit">Edit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!--**********************************
            Content body end
        ***********************************-->
