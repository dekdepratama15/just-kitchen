<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-xl-12 col-lg-12">
				<?php if ($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger solid">
						<?= $this->session->flashdata('error') ?>
					</div>
				<?php endif ?>
				<div class="card card-bx">
					<div class="card-header">
						<h4 class="title">Form Tambah Jadwal Karyawan</h4>
					</div>
					<form method="POST" action="<?= base_url('jadwal/add') ?>" enctype="multipart/form-data">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-12 mb-3">
									<label class="form-label">Karyawan</label>
									<select class="selectpicker nice-select default-select form-control wide mh-auto" name="karyawan_id[]" multiple required>
										<?php foreach ($karyawans->result_array() as $karyawan) : ?>
											<option value="<?= $karyawan['karyawan_id'] ?>"><?= $karyawan['nama_panggilan'] ?> - <?= $karyawan['jabatan'] ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Tanggal</label>
									<input type="date" class="form-control" value="" name="tanggal" required>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Status</label>
									<select class="selectpicker nice-select default-select form-control wide mh-auto" name="status" required>
										<option selected disabled>- Pilih Status -</option>
										<?php
										$status = ['belum', 'hadir', 'sakit', 'cuti', 'libur', 'pindah'];
										foreach ($status as $status) : ?>
											<option value="<?= $status ?>"><?= ucfirst($status) ?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Jadwal Masuk</label>
									<input type="time" class="form-control" name="jadwal_masuk" required>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Jadwal Keluar</label>
									<input type="time" class="form-control" name="jadwal_keluar" required>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<button class="btn btn-primary" type="submit">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!--**********************************
            Content body end
        ***********************************-->
