<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->


		<div class="row">
			<div class="col-lg-12">
				<?php if ($this->session->flashdata('success')) : ?>
					<div class="alert alert-success solid">
						<?= $this->session->flashdata('success') ?>
					</div>
				<?php endif ?>

				<?php if ($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger solid">
						<?= $this->session->flashdata('error') ?>
					</div>
				<?php endif ?>
			</div>
			<div class="col-12">
				<a href="<?= base_url('jadwal/tambah') ?>"><button class="btn btn-primary my-3">Tambah Jadwal</button></a>
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Daftar Jadwal</h4>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="example" class="display" style="min-width: 845px">
								<thead>
									<tr>
										<th>No</th>
										<th>Karyawan</th>
										<th>Tanggal</th>
										<th>Status</th>
										<th>Jadwal Masuk</th>
										<th>Jadwal Keluar</th>
										<th>Waktu Masuk</th>
										<th>Waktu Keluar</th>
										<th>Terlambat</th>
										<th>Surat Sakit</th>
										<th>Tanggal Dibuat</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$no = 1;
									foreach ($jadwals->result_array() as $jadwal) : ?>
										<tr>
											<td><?= $no++ ?></td>
											<td><?= $jadwal['nama_panggilan'] ?> - <?= $jadwal['jabatan'] ?></td>
											<td><?= date('d M Y', strtotime($jadwal['tanggal'])) ?></td>
											<?php if ($jadwal['jadwal_status'] == 'hadir') : ?>
												<td><span class="badge badge-success"><?= $jadwal['jadwal_status'] ?></span></td>
											<?php else : ?>
												<td><span class="badge badge-danger"><?= $jadwal['jadwal_status'] ?></span></td>
											<?php endif ?>

											<td><?= date('H:i:s', strtotime($jadwal['jadwal_masuk'])) ?></td>
											<td><?= date('H:i:s', strtotime($jadwal['jadwal_keluar'])) ?></td>

											<?php if ($jadwal['waktu_masuk'] != NULL) : ?>
												<td><?= date('H:i:s', strtotime($jadwal['waktu_masuk'])) ?></td>
											<?php else : ?>
												<td>-</td>
											<?php endif ?>

											<?php if ($jadwal['waktu_keluar'] != NULL) : ?>
												<td><?= date('H:i:s', strtotime($jadwal['waktu_keluar'])) ?></td>
											<?php else : ?>
												<td>-</td>
											<?php endif ?>

											<?php if ($jadwal['waktu_telat'] != NULL) : ?>
												<td><?= $jadwal['waktu_telat'] ?> Menit</td>
											<?php else : ?>
												<td>-</td>
											<?php endif ?>

											<?php if ($jadwal['surat_sakit'] != NULL) : ?>
												<td><a href="<?= base_url('assets/suratsakit/' . $jadwal['surat_sakit']) ?>" target="_BLANK"><button class="btn btn-sm btn-primary">Lihat Surat Sakit</button></a></td>
											<?php else : ?>
												<td>-</td>
											<?php endif ?>

											<td><?= date('d M Y H:i:s', strtotime($jadwal['jadwal_ca'])) ?></td>
											<td>
												<div class="dropdown">
													<button id="toa" class="btn btn-sm btn-primary" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
														<span class="bi bi-three-dots-vertical"></span>
													</button>
													<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
														<a href="<?= base_url('jadwal/edit/' . $jadwal['jadwal_id']) ?>" class="dropdown-item"><i class="fa fa-edit" id="from1"></i> Edit</a>
														<a href="<?= base_url('jadwal/hapus/' . $jadwal['jadwal_id']) ?>" class="dropdown-item" onclick="return confirm('Yakin hapus jadwal ?')"><i class="fa fa-trash"></i> Hapus</a>
													</div>
												</div>
											</td>
										</tr>
									<?php endforeach ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--**********************************
            Content body end
        ***********************************-->
