<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->


		<div class="row">
			<div class="col-lg-12">
				<?php if ($this->session->flashdata('success')) : ?>
					<div class="alert alert-success solid">
						<?= $this->session->flashdata('success') ?>
					</div>
				<?php endif ?>

				<?php if ($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger solid">
						<?= $this->session->flashdata('error') ?>
					</div>
				<?php endif ?>
			</div>
			<div class="col-12">
				<?php if ($this->session->userdata('role') == 'user') : ?>
					<a href="<?= base_url('pindahjadwal') ?>"><button class="btn btn-primary my-3">Kembali</button></a>
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Daftar Pengajuan Pindah Jadwal Anda</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table id="example" class="display" style="min-width: 845px">
									<thead>
										<tr>
											<th>No</th>
											<th>Tanggal Lama</th>
											<th>Jadwal Masuk Lama</th>
											<th>Jadwal Keluar Lama</th>
											<th>Tanggal Baru</th>
											<th>Jadwal Masuk Baru</th>
											<th>Jadwal Keluar Baru</th>
											<th>Tanggal Pengajuan</th>
											<th>Status</th>
											<th>Alasan</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$no = 1;
										foreach ($pjadwals->result_array() as $jadwal) : ?>
											<tr>
												<td><?= $no++ ?></td>
												<td><?= date('d M Y', strtotime($jadwal['tanggal_lama'])) ?></td>
												<td><?= date('H:i:s', strtotime($jadwal['jadwal_masuk_lama'])) ?></td>
												<td><?= date('H:i:s', strtotime($jadwal['jadwal_keluar_lama'])) ?></td>
												<td><?= date('d M Y', strtotime($jadwal['tanggal'])) ?></td>
												<td><?= date('H:i:s', strtotime($jadwal['jadwal_masuk'])) ?></td>
												<td><?= date('H:i:s', strtotime($jadwal['jadwal_keluar'])) ?></td>
												<td><?= date('d M Y H:i:s', strtotime($jadwal['pindah_ca'])) ?></td>
												<?php
												if ($jadwal['status_pindah'] == 'approve') : ?>
													<td><span class="badge badge-success"><?= $jadwal['status_pindah'] ?></span></td>
													<td><?= ucfirst($jadwal['alasan']) ?></td>
													<td>
														<div class="dropdown">
															<button id="toa" class="btn btn-sm btn-primary" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
																<span class="bi bi-three-dots-vertical"></span>
															</button>
															<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
																<a href="<?= base_url('pindahjadwal/hapus/' . $jadwal['pindah_jadwal_id']) ?>" class="dropdown-item" onclick="return confirm('Yakin hapus pindah jadwal?')"><i class="fa fa-trash" id="from1"></i> Hapus</a>
															</div>
														</div>
													</td>
												<?php endif ?>

												<?php
												if ($jadwal['status_pindah'] == 'pending') : ?>
													<td><span class="badge badge-danger"><?= $jadwal['status_pindah'] ?></span></td>
													<td><?= ucfirst($jadwal['alasan']) ?></td>
													<td>
														<div class="dropdown">
															<button id="toa" class="btn btn-sm btn-primary" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
																<span class="bi bi-three-dots-vertical"></span>
															</button>
															<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
																<a href="<?= base_url('pindahjadwal/edit/' . $jadwal['pindah_jadwal_id']) ?>" class="dropdown-item"><i class="fa fa-edit" id="from1"></i> Edit</a>
																<a href="<?= base_url('pindahjadwal/hapus/' . $jadwal['pindah_jadwal_id']) ?>" class="dropdown-item" onclick="return confirm('Yakin hapus pindah jadwal?')"><i class="fa fa-trash" id="from1"></i> Hapus</a>
															</div>
														</div>
													</td>
												<?php endif ?>
											</tr>
										<?php endforeach ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				<?php endif ?>

				<?php if ($this->session->userdata('role') == 'admin') : ?>
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Daftar Pengajuan Pindah Jadwal</h4>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table id="example" class="display" style="min-width: 845px">
									<thead>
										<tr>
											<th>No</th>
											<th>Karyawan</th>
											<th>Tanggal Lama</th>
											<th>Jadwal Masuk Lama</th>
											<th>Jadwal Keluar Lama</th>
											<th>Tanggal Baru</th>
											<th>Jadwal Masuk Baru</th>
											<th>Jadwal Keluar Baru</th>
											<th>Tanggal Pengajuan</th>
											<th>Status</th>
											<th>Alasan</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$no = 1;
										foreach ($pjadwals->result_array() as $jadwal) : ?>
											<tr>
												<td><?= $no++ ?></td>
												<td><?= ucfirst($jadwal['nama_panggilan']) ?></td>
												<td><?= date('d M Y', strtotime($jadwal['tanggal_lama'])) ?></td>
												<td><?= date('H:i:s', strtotime($jadwal['jadwal_masuk_lama'])) ?></td>
												<td><?= date('H:i:s', strtotime($jadwal['jadwal_keluar_lama'])) ?></td>
												<td><?= date('d M Y', strtotime($jadwal['tanggal'])) ?></td>
												<td><?= date('H:i:s', strtotime($jadwal['jadwal_masuk'])) ?></td>
												<td><?= date('H:i:s', strtotime($jadwal['jadwal_keluar'])) ?></td>
												<td><?= date('d M Y H:i:s', strtotime($jadwal['pindah_ca'])) ?></td>
												<?php
												if ($jadwal['status_pindah'] == 'approve') : ?>
													<td><span class="badge badge-success"><?= $jadwal['status_pindah'] ?></span></td>
												<?php endif ?>

												<?php
												if ($jadwal['status_pindah'] == 'pending') : ?>
													<td><span class="badge badge-danger"><?= $jadwal['status_pindah'] ?></span></td>
												<?php endif ?>
												<td><?= ucfirst($jadwal['alasan']) ?></td>

												<?php
												if ($jadwal['status_pindah'] == 'pending') : ?>
													<td>
														<div class="dropdown">
															<button id="toa" class="btn btn-sm btn-primary" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
																<span class="bi bi-three-dots-vertical"></span>
															</button>
															<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
																<a href="<?= base_url('pindahjadwal/approve/' . $jadwal['pindah_jadwal_id']) ?>" class="dropdown-item" onclick="return confirm('Yakin approve pindah jadwal?')"><i class="fa fa-check" id="from1"></i> Approve</a>
																<a href="<?= base_url('pindahjadwal/tolak/' . $jadwal['pindah_jadwal_id']) ?>" class="dropdown-item" onclick="return confirm('Yakin tolak pindah jadwal?')"><i class="fa fa-times" id="from1"></i> Tolak</a>
															</div>
														</div>
													</td>
												<?php else : ?>
													<td>-</td>
												<?php endif ?>

											</tr>
										<?php endforeach ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				<?php endif ?>


			</div>
		</div>
	</div>
</div>
<!--**********************************
            Content body end
        ***********************************-->
