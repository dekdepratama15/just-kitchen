<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->


		<div class="row">
			<div class="col-lg-12">
				<?php if ($this->session->flashdata('success')) : ?>
					<div class="alert alert-success solid">
						<?= $this->session->flashdata('success') ?>
					</div>
				<?php endif ?>

				<?php if ($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger solid">
						<?= $this->session->flashdata('error') ?>
					</div>
				<?php endif ?>
			</div>
			<div class="col-12">
				<a href="<?= base_url('pindahjadwal/listpindah') ?>"><button class="btn btn-primary my-3">Lihat Pengajuan Pindah Jadwal</button></a>
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Daftar Jadwal Anda</h4>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="example" class="display" style="min-width: 845px">
								<thead>
									<tr>
										<th>No</th>
										<th>Tanggal</th>
										<th>Jadwal Masuk</th>
										<th>Jadwal Keluar</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$no = 1;
									foreach ($jadwals->result_array() as $jadwal) : ?>
										<tr>
											<td><?= $no++ ?></td>
											<td><?= date('d M Y', strtotime($jadwal['tanggal'])) ?></td>
											<td><?= date('H:i:s', strtotime($jadwal['jadwal_masuk'])) ?></td>
											<td><?= date('H:i:s', strtotime($jadwal['jadwal_keluar'])) ?></td>

											<td>
												<div class="dropdown">
													<button id="toa" class="btn btn-sm btn-primary" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
														<span class="bi bi-three-dots-vertical"></span>
													</button>
													<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
														<a href="<?= base_url('pindahjadwal/pindah/' . $jadwal['jadwal_id']) ?>" class="dropdown-item"><i class="fa fa-edit" id="from1"></i> Pindah Jadwal</a>
													</div>
												</div>
											</td>
										</tr>
									<?php endforeach ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--**********************************
            Content body end
        ***********************************-->
