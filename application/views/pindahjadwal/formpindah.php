<!--**********************************
            Content body start
        ***********************************-->
<div class="content-body">
	<div class="container-fluid">
		<!-- row -->
		<div class="row">
			<div class="col-xl-12 col-lg-12">
				<?php if ($this->session->flashdata('error')) : ?>
					<div class="alert alert-danger solid">
						<?= $this->session->flashdata('error') ?>
					</div>
				<?php endif ?>
				<div class="card card-bx">
					<div class="card-header">
						<h4 class="title">Form Pindah Jadwal</h4>
					</div>
					<form method="POST" action="<?= base_url('pindahjadwal/pindahProcess/') . $jadwal['jadwal_id'] ?>" enctype="multipart/form-data">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-12 mb-3">
									<label class="form-label">Tanggal Lama</label>
									<input type="date" class="form-control" name="tanggal" value="<?= $jadwal['tanggal'] ?>" readonly>
								</div>
								<div class="col-sm-6 mb-3">
									<label class="form-label">Jadwal Masuk Lama</label>
									<input type="time" class="form-control" value="<?= $jadwal['jadwal_masuk'] ?>" name="jadwal_masuk" readonly>
								</div>
								<div class="col-sm-6 mb-3">
									<label class="form-label">Jadwal Keluar Lama</label>
									<input type="time" class="form-control" value="<?= $jadwal['jadwal_keluar'] ?>" name="jadwal_keluar" readonly>
								</div>

								<div class="col-sm-12 mb-3">
									<label class="form-label">Tanggal Jadwal Baru</label>
									<input type="date" class="form-control" name="tanggal_baru" value="" required>
								</div>
								<div class="col-sm-6 mb-3">
									<label class="form-label">Jadwal Masuk Baru</label>
									<input type="time" class="form-control" value="" name="jadwal_masuk_baru" required>
								</div>
								<div class="col-sm-6 mb-3">
									<label class="form-label">Jadwal Keluar Baru</label>
									<input type="time" class="form-control" value="" name="jadwal_keluar_baru" required>
								</div>
								<div class="col-sm-12 mb-3">
									<label class="form-label">Alasan Pindah Jadwal</label>
									<textarea name="alasan" id="" cols="30" rows="10" class="form-control" required></textarea>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<button class="btn btn-primary" type="submit">Ajukan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!--**********************************
            Content body end
        ***********************************-->
