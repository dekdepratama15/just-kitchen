-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 01, 2024 at 09:38 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `just_kitchen`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang_mentahs`
--

CREATE TABLE `barang_mentahs` (
  `id` int(11) NOT NULL,
  `nama_barang` varchar(30) NOT NULL,
  `harga` double NOT NULL,
  `keterangan` text NOT NULL,
  `stock` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang_mentahs`
--

INSERT INTO `barang_mentahs` (`id`, `nama_barang`, `harga`, `keterangan`, `stock`, `created_at`, `updated_at`) VALUES
(1, 'cabai', 2000, 'cabai segar sekali', 50, '2023-12-20 10:13:18', '2023-12-20 14:43:35'),
(3, 'bawang', 5000, 'bawang mentah', 35, '2024-01-01 00:54:45', '2024-01-01 00:54:45');

-- --------------------------------------------------------

--
-- Table structure for table `barang_mentah_logs`
--

CREATE TABLE `barang_mentah_logs` (
  `id` int(11) NOT NULL,
  `barang_mentah_id` int(11) NOT NULL,
  `type` enum('masuk','keluar') NOT NULL,
  `jumlah` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang_mentah_logs`
--

INSERT INTO `barang_mentah_logs` (`id`, `barang_mentah_id`, `type`, `jumlah`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 1, 'masuk', 90, 'cabai segar sekali', '2023-12-20 10:13:18', '2023-12-20 14:35:50'),
(5, 1, 'keluar', 10, 'busuk', '2023-12-20 14:36:53', '2023-12-20 14:36:53'),
(6, 1, 'masuk', 80, 'cabai segar sekali', '2023-12-20 14:43:35', '2023-12-20 14:43:35'),
(7, 1, 'keluar', 5, 'busuk', '2023-12-20 14:44:16', '2023-12-20 14:44:16'),
(8, 3, 'masuk', 100, 'bawang mentah', '2024-01-01 00:54:45', '2024-01-01 00:54:45'),
(17, 3, 'keluar', 5, 'Dipakai untuk Nasi Goreng', '2024-01-01 04:12:17', '2024-01-01 04:12:17'),
(18, 1, 'keluar', 10, 'Dipakai untuk Nasi Goreng', '2024-01-01 04:12:17', '2024-01-01 04:12:17');

-- --------------------------------------------------------

--
-- Table structure for table `barang_stocks`
--

CREATE TABLE `barang_stocks` (
  `id` int(11) NOT NULL,
  `nama_barang` varchar(30) NOT NULL,
  `harga` double NOT NULL,
  `keterangan` text NOT NULL,
  `stock` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang_stocks`
--

INSERT INTO `barang_stocks` (`id`, `nama_barang`, `harga`, `keterangan`, `stock`, `created_at`, `updated_at`) VALUES
(29, 'Nasi Goreng', 20000, 'mantap', 15, '2024-01-01 04:12:17', '2024-01-01 04:12:17');

-- --------------------------------------------------------

--
-- Table structure for table `barang_stock_logs`
--

CREATE TABLE `barang_stock_logs` (
  `id` int(11) NOT NULL,
  `barang_stock_id` int(11) NOT NULL,
  `type` enum('masuk','keluar') NOT NULL,
  `jumlah` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang_stock_logs`
--

INSERT INTO `barang_stock_logs` (`id`, `barang_stock_id`, `type`, `jumlah`, `keterangan`, `created_at`, `updated_at`) VALUES
(27, 29, 'masuk', 20, 'mantap', '2024-01-01 04:12:17', '2024-01-01 04:12:17'),
(28, 29, 'keluar', 5, 'dibeli', '2024-01-01 04:28:41', '2024-01-01 04:28:41');

-- --------------------------------------------------------

--
-- Table structure for table `barang_stock_uses`
--

CREATE TABLE `barang_stock_uses` (
  `id` int(11) NOT NULL,
  `barang_stock_id` int(11) NOT NULL,
  `barang_mentah_id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang_stock_uses`
--

INSERT INTO `barang_stock_uses` (`id`, `barang_stock_id`, `barang_mentah_id`, `jumlah`, `created_at`, `updated_at`) VALUES
(36, 29, 3, 5, '2024-01-01 04:12:17', '2024-01-01 04:12:17'),
(37, 29, 1, 10, '2024-01-01 04:12:17', '2024-01-01 04:12:17');

-- --------------------------------------------------------

--
-- Table structure for table `cuti_jadwals`
--

CREATE TABLE `cuti_jadwals` (
  `id` int(11) NOT NULL,
  `jadwal_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `status` enum('pending','approve') NOT NULL,
  `jenis` varchar(50) NOT NULL,
  `alasan` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `jadwals`
--

CREATE TABLE `jadwals` (
  `id` int(11) NOT NULL,
  `karyawan_id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `status` enum('belum','hadir','sakit','cuti','libur','pindah') NOT NULL,
  `surat_sakit` text DEFAULT NULL,
  `jadwal_masuk` time NOT NULL,
  `jadwal_keluar` time NOT NULL,
  `waktu_masuk` time DEFAULT NULL,
  `waktu_keluar` time DEFAULT NULL,
  `waktu_telat` text DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jadwals`
--

INSERT INTO `jadwals` (`id`, `karyawan_id`, `tanggal`, `status`, `surat_sakit`, `jadwal_masuk`, `jadwal_keluar`, `waktu_masuk`, `waktu_keluar`, `waktu_telat`, `created_at`, `updated_at`) VALUES
(2, 7, '2024-01-01', 'belum', NULL, '07:00:00', '16:00:00', NULL, NULL, NULL, '2023-11-06 15:52:25', '2024-01-01 16:33:25'),
(3, 8, '2023-12-28', 'belum', NULL, '08:00:00', '15:00:00', NULL, NULL, NULL, '2023-11-29 10:55:44', '2023-11-29 10:55:44'),
(6, 7, '2023-12-28', 'sakit', 'suratsakit_7_28122023144841.pdf', '14:50:00', '15:00:00', NULL, NULL, NULL, '2023-12-28 14:46:01', '2023-12-28 14:48:41');

-- --------------------------------------------------------

--
-- Table structure for table `karyawans`
--

CREATE TABLE `karyawans` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `jabatan` enum('Pimpinan','Staff') NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `nama_panggilan` varchar(50) NOT NULL,
  `status` enum('Aktif','Resign') NOT NULL,
  `jenis_kelamin` varchar(15) NOT NULL,
  `golongan_darah` varchar(3) NOT NULL,
  `agama` varchar(15) NOT NULL,
  `status_perkawinan` varchar(20) NOT NULL,
  `status_karyawan` enum('Daily','Training','Kontrak','Tetap') NOT NULL,
  `jenis_identitas` varchar(30) NOT NULL,
  `no_identitas` varchar(25) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `tempat_lahir` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `tgl_bergabung` date NOT NULL,
  `tgl_selesai` date DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `karyawans`
--

INSERT INTO `karyawans` (`id`, `user_id`, `jabatan`, `nama_lengkap`, `nama_panggilan`, `status`, `jenis_kelamin`, `golongan_darah`, `agama`, `status_perkawinan`, `status_karyawan`, `jenis_identitas`, `no_identitas`, `no_telp`, `tgl_lahir`, `tempat_lahir`, `alamat`, `tgl_bergabung`, `tgl_selesai`, `created_at`, `updated_at`) VALUES
(7, 10, 'Pimpinan', 'I Kadek Surya Indrawan', 'Surya', 'Aktif', 'Laki-laki', 'O', 'Hindu', 'Belum Kawin', 'Tetap', 'KTP', '51718323124', '081738471837', '2001-11-29', 'Denpasar', 'Sidakarya', '2023-11-01', NULL, '2023-11-01 10:49:15', '2023-11-01 13:50:34'),
(8, 11, 'Staff', 'Abdul Rais', 'Abdul', 'Aktif', 'Laki-laki', 'AB', 'Buddha', 'Belum Kawin', 'Tetap', 'KTP', '827837821', '081738283', '2016-01-03', 'Bandung', 'Bandung', '2023-11-03', NULL, '2023-11-03 10:44:17', '2023-11-29 10:36:14');

-- --------------------------------------------------------

--
-- Table structure for table `pengumumans`
--

CREATE TABLE `pengumumans` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `judul` text NOT NULL,
  `file` text NOT NULL,
  `keterangan` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pengumumans`
--

INSERT INTO `pengumumans` (`id`, `user_id`, `judul`, `file`, `keterangan`, `created_at`, `updated_at`) VALUES
(10, 1, 'Test Judul Pengumuman', 'pengumuman_1_03112023144545.pdf', 'Tanggal 15 November tolong datang semuanya tepat waktu !', '2023-11-03 14:45:45', '2023-11-03 14:45:45');

-- --------------------------------------------------------

--
-- Table structure for table `peringatans`
--

CREATE TABLE `peringatans` (
  `id` int(11) NOT NULL,
  `karyawan_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `nomor` varchar(30) NOT NULL,
  `jenis` enum('SP1','SP2','SP3') NOT NULL,
  `alasan` text NOT NULL,
  `file` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `peringatans`
--

INSERT INTO `peringatans` (`id`, `karyawan_id`, `user_id`, `nomor`, `jenis`, `alasan`, `file`, `created_at`, `updated_at`) VALUES
(2, 7, 1, '233129348', 'SP2', 'Tidak masuk 1 tahun anak ini memang nakal', 'peringatan_1_03112023141520.pdf', '2023-11-03 14:15:20', '2023-11-03 14:16:52');

-- --------------------------------------------------------

--
-- Table structure for table `pindah_jadwals`
--

CREATE TABLE `pindah_jadwals` (
  `id` int(11) NOT NULL,
  `jadwal_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `status` enum('pending','approve') NOT NULL,
  `alasan` text NOT NULL,
  `jadwal_keluar` time NOT NULL,
  `jadwal_masuk` time NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` enum('admin','user') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `role`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '0192023a7bbd73250516f069df18b500', 'admin', '2023-10-25 12:47:42', '2023-10-25 12:47:42'),
(10, 'surya', 'surya@gmail.com', '8c3a9f08696af482a53e1dd8db0ef40f', 'user', '2023-11-01 10:49:15', '2023-11-01 13:50:34'),
(11, 'indrawan', 'indrawan@gmail.com', 'e62b0d084a62a380e2d329545f578b32', 'user', '2023-11-03 10:44:17', '2023-11-29 10:36:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang_mentahs`
--
ALTER TABLE `barang_mentahs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `barang_mentah_logs`
--
ALTER TABLE `barang_mentah_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_barang_mentah_log_barang_mentah` (`barang_mentah_id`);

--
-- Indexes for table `barang_stocks`
--
ALTER TABLE `barang_stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `barang_stock_logs`
--
ALTER TABLE `barang_stock_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_barang_stock_log_barang_stock` (`barang_stock_id`);

--
-- Indexes for table `barang_stock_uses`
--
ALTER TABLE `barang_stock_uses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_barang_mentah_use` (`barang_mentah_id`),
  ADD KEY `fk_barang_stock_use` (`barang_stock_id`);

--
-- Indexes for table `cuti_jadwals`
--
ALTER TABLE `cuti_jadwals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cuti_jadwal_user` (`user_id`),
  ADD KEY `fk_cuti_jadwal_jadwal` (`jadwal_id`);

--
-- Indexes for table `jadwals`
--
ALTER TABLE `jadwals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_jadwal_karyawan` (`karyawan_id`);

--
-- Indexes for table `karyawans`
--
ALTER TABLE `karyawans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_karyawan_user` (`user_id`);

--
-- Indexes for table `pengumumans`
--
ALTER TABLE `pengumumans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pengumuman_user` (`user_id`);

--
-- Indexes for table `peringatans`
--
ALTER TABLE `peringatans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_peringatan_karyawan` (`karyawan_id`),
  ADD KEY `fk_peringatan_user` (`user_id`);

--
-- Indexes for table `pindah_jadwals`
--
ALTER TABLE `pindah_jadwals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pindah_jadwal_jadwal` (`jadwal_id`),
  ADD KEY `fk_pindah_jadwal_user` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang_mentahs`
--
ALTER TABLE `barang_mentahs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `barang_mentah_logs`
--
ALTER TABLE `barang_mentah_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `barang_stocks`
--
ALTER TABLE `barang_stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `barang_stock_logs`
--
ALTER TABLE `barang_stock_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `barang_stock_uses`
--
ALTER TABLE `barang_stock_uses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `cuti_jadwals`
--
ALTER TABLE `cuti_jadwals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `jadwals`
--
ALTER TABLE `jadwals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `karyawans`
--
ALTER TABLE `karyawans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pengumumans`
--
ALTER TABLE `pengumumans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `peringatans`
--
ALTER TABLE `peringatans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `pindah_jadwals`
--
ALTER TABLE `pindah_jadwals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `barang_mentah_logs`
--
ALTER TABLE `barang_mentah_logs`
  ADD CONSTRAINT `fk_barang_mentah_log_barang_mentah` FOREIGN KEY (`barang_mentah_id`) REFERENCES `barang_mentahs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `barang_stock_logs`
--
ALTER TABLE `barang_stock_logs`
  ADD CONSTRAINT `fk_barang_stock_log_barang_stock` FOREIGN KEY (`barang_stock_id`) REFERENCES `barang_stocks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `barang_stock_uses`
--
ALTER TABLE `barang_stock_uses`
  ADD CONSTRAINT `fk_barang_mentah_use` FOREIGN KEY (`barang_mentah_id`) REFERENCES `barang_mentahs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_barang_stock_use` FOREIGN KEY (`barang_stock_id`) REFERENCES `barang_stocks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cuti_jadwals`
--
ALTER TABLE `cuti_jadwals`
  ADD CONSTRAINT `fk_cuti_jadwal_jadwal` FOREIGN KEY (`jadwal_id`) REFERENCES `jadwals` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cuti_jadwal_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `jadwals`
--
ALTER TABLE `jadwals`
  ADD CONSTRAINT `fk_jadwal_karyawan` FOREIGN KEY (`karyawan_id`) REFERENCES `karyawans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `karyawans`
--
ALTER TABLE `karyawans`
  ADD CONSTRAINT `fk_karyawan_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pengumumans`
--
ALTER TABLE `pengumumans`
  ADD CONSTRAINT `fk_pengumuman_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `peringatans`
--
ALTER TABLE `peringatans`
  ADD CONSTRAINT `fk_peringatan_karyawan` FOREIGN KEY (`karyawan_id`) REFERENCES `karyawans` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_peringatan_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pindah_jadwals`
--
ALTER TABLE `pindah_jadwals`
  ADD CONSTRAINT `fk_pindah_jadwal_jadwal` FOREIGN KEY (`jadwal_id`) REFERENCES `jadwals` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_pindah_jadwal_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
